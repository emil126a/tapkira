<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealEstateImage extends Model
{
      public function real_estate()
   {
   	   return $this->belongsTo('App\RealEstate');
   }
}
