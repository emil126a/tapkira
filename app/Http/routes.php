<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(
    [
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
    ],
    function()
    {

        //guests starts
        Route::get('/facebook/login', 'Auth\FacebookController@login');
// Endpoint that is redirected to after an authentication attempt
        Route::get('/facebook/callback','Auth\FacebookController@callback');
        /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
        Route::get('/admin/login','Adminauth\AuthController@showLoginForm');
        Route::post('/admin/login','Adminauth\AuthController@login');
        Route::get('/admin/password/reset','Adminauth\PasswordController@resetPassword');
    // Registration Routes...
/*        Route::get('admin/register', 'Adminauth\AuthController@showRegistrationForm');
        Route::post('admin/register', 'Adminauth\AuthController@register');*/
        Route::get('/homepage', 'GuestController@index');
        Route::get('/contact', 'GuestController@contact');
        Route::post('/contact', 'GuestController@send_message');
        Route::get('/about', 'GuestController@about');


        Route::get('/related_sub_real_estate_search_form', 'GuestController@related_sub_real_estate_search_form');
        Route::get('/related_sub_real_estate_search_form_homepage', 'GuestController@related_sub_real_estate_search_form_homepage');


        Route::get('/add/new', 'GuestController@add_new');    
        Route::get('/rel_sub/guest', 'GuestController@rel_sub');  
        Route::get('/rel_sub_real_estate/guest', 'GuestController@rel_sub_real_estate');  
        Route::get('/rel_item_sub_category/guest', 'GuestController@rel_item_sub_category');
        Route::get('/rel_service_sub_category/guest', 'GuestController@rel_service_sub_category');

//guests ends
 




//services

        Route::get('/service_list/{cat_id}/{sub_cat_id?}', 'GuestController@service_list');
        Route::get('/service/{id}', 'GuestController@service');
       Route::get('/related_sub_service_category', 'GuestController@related_sub_service_category');

//real estate

        Route::get('/real_estate_list/{cat_id}', 'GuestController@real_estate_list');
        Route::get('/real_estate/{id}', 'GuestController@real_estate');

//items/

      //  Route::get('/item_list/{cat_id}', 'GuestController@item_list');
        Route::get('/item_list/{cat_id}/{sub_cat_id?}', 'GuestController@item_list');
        Route::get('/item/{id}', 'GuestController@item');
        Route::get('/related_sub_item_category', 'GuestController@related_sub_item_category');
        Route::get('/related_sub_item_category_homepage', 'GuestController@related_sub_item_category_homepage');
        Route::get('/related_sub_service_category_homepage', 'GuestController@related_sub_service_category_homepage');


        Route::group(['middleware' => ['admin']], function () {//admins

            Route::get('/admin/logout','Adminauth\AuthController@logout');
            Route::get('/admin', 'Admin\DashboardController@real_estates');


/*real estates*/            
            Route::get('/admin/real_estates/{status}', 'Admin\DashboardController@real_estates');
            Route::get('/admin/edit_real_estate/{id}', 'Admin\DashboardController@real_estate_edit_form');
            Route::get('/admin/rel_sub_real_estate', 'Admin\DashboardController@rel_sub_real_estate');
            Route::post('/admin/store_real_estate/{id}', 'Admin\DashboardController@store_real_estate');
            Route::get('/admin/delete_real_estate_image/{id}', 'Admin\DashboardController@delete_real_estate_image');
            Route::get('/admin/delete_real_estate/{id}', 'Admin\DashboardController@delete_real_estate');
            Route::get('/admin/archive_real_estate/{id}', 'Admin\DashboardController@archive_real_estate');
            Route::get('/admin/vip/{id}', 'Admin\DashboardController@vip_real_estate');
            Route::post('/admin/real_estate_checbox_action', 'Admin\DashboardController@real_estate_checbox_action');
          
/*items*/
            Route::get('/admin/items/{status}', 'Admin\DashboardController@items');          
            Route::get('/admin/edit_item/{id}', 'Admin\DashboardController@item_edit_form');
            Route::get('/admin/rel_item_sub_category', 'Admin\DashboardController@related_sub_item_category');
            Route::post('/admin/store_item/{id}', 'Admin\DashboardController@store_item');
            Route::get('/admin/delete_item_image/{id}', 'Admin\DashboardController@delete_item_image');
            Route::get('/admin/delete_item/{id}', 'Admin\DashboardController@delete_item');
         
            Route::get('/admin/archive_item/{id}', 'Admin\DashboardController@archive_item');
            Route::get('/admin/vip_item/{id}', 'Admin\DashboardController@vip_item');
            Route::post('/admin/item_checbox_action', 'Admin\DashboardController@item_checbox_action');
/*services*/
            Route::get('/admin/services/{status}', 'Admin\DashboardController@services');          
            Route::get('/admin/edit_service/{id}', 'Admin\DashboardController@service_edit_form');
            Route::post('/admin/store_service/{id}', 'Admin\DashboardController@store_service');
            Route::get('/admin/delete_service_image/{id}', 'Admin\DashboardController@delete_service_image');
            Route::get('/admin/rel_service_sub_category', 'Admin\DashboardController@related_sub_service_category');
            Route::get('/admin/delete_service/{id}', 'Admin\DashboardController@delete_service');


            Route::get('/admin/archive_service/{id}', 'Admin\DashboardController@archive_service');
            Route::get('/admin/vip_service/{id}', 'Admin\DashboardController@vip_service');
            Route::post('/admin/service_checbox_action', 'Admin\DashboardController@service_checbox_action');

            Route::get('/admin/users','Admin\DashboardController@users');


            Route::get('/admin/delete_user/{id}','Admin\DashboardController@delete_user');
            Route::get('/admin/edit_user/{id}','Admin\DashboardController@edit_user');
            Route::post('/admin/save_user','Admin\DashboardController@save_user');


            Route::get('/admin/account','Admin\DashboardController@account');
            Route::post('/admin/account','Admin\DashboardController@save_account');


        });

        
        Route::get('/', 'GuestController@index');


        Route::group(['middleware' => 'web'], function () {//users
            Route::auth();
            Route::get('/add', 'Auth\DashboardController@add');
            Route::get('/success', 'Auth\DashboardController@success');
            Route::post('/real_estates_save', 'Auth\DashboardController@storeRealestate');

            Route::post('/upload_real_estate_images','Auth\DashboardController@upload_real_estate_images_ajax');
            Route::post('/delete_real_estate_images','Auth\DashboardController@delete_real_estate_images_ajax');



             Route::post('/upload_item_images','Auth\DashboardController@upload_item_images_ajax');
             Route::post('/delete_item_images','Auth\DashboardController@delete_item_images_ajax');


            Route::post('/upload_service_images','Auth\DashboardController@upload_service_images_ajax');
             Route::post('/delete_service_images','Auth\DashboardController@delete_service_images_ajax');



            Route::post('/item_save', 'Auth\DashboardController@storeItem');
            Route::post('/service_save', 'Auth\DashboardController@storeService');
            Route::get('/dashboard_profile', 'Auth\DashboardController@dashboard_profile');
            Route::get('/myads', 'Auth\DashboardController@myads');

            Route::get('/rel_sub', 'Auth\DashboardController@rel_sub');  
            Route::get('/rel_sub_real_estate', 'Auth\DashboardController@rel_sub_real_estate');  
            Route::get('/rel_item_sub_category', 'Auth\DashboardController@rel_item_sub_category');
            Route::get('/rel_service_sub_category', 'Auth\DashboardController@rel_service_sub_category');

            Route::get('/rel_dashboard', 'Auth\DashboardController@related_dashboard');
            Route::post('/save_user_data', 'Auth\DashboardController@save_user_data');
            Route::post('/save_password_change', 'Auth\DashboardController@save_password_change');
            Route::get('/delete_real_estate/{id}', 'Auth\DashboardController@delete_real_estate');
            Route::get('/delete_item/{id}', 'Auth\DashboardController@delete_item');
            Route::get('/delete_service/{id}', 'Auth\DashboardController@delete_service');
            Route::get('/home', 'HomeController@index');
        });
    });



