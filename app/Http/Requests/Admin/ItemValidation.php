<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ItemValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'item_name'=>'required',
        'item_price'=>'required|numeric',
        'description'=>'required',        
        'item_category_id'=>'required',
        'item_images.*'=>'image',
        
        ];
    }
}
