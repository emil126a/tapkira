<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class RealEstateValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'area'=>'required|numeric',
        'price'=>'required|numeric',
        'description'=>'required',
        //'image_files'=>'required|array',
       // 'images.*'=>'required|image',/*|dimensions:min_width=400,min_height=600*/
        'address'=>'required',
        'real_estate_sub_menu_id'=>'required',
        'estate_images.*'=>'image',// images from admin panel 
        ];
    }
}
