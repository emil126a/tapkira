<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Redirect;
class ServiceValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


public function response(array $errors){
        return Redirect::back()->withErrors($errors)->withInput()->with('type', '3');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'service_price'=>'required',
        'service_name'=>'required',
        'description'=>'required',
        'image_files'=>'required|array',
        'phone'=>'required',
       // 'service_images.*'=>'image',// images from admin panel 
/*        'images.*'=>'required|image',*/
        ];
    }
}
