<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App,Redirect;

class ItemValidation extends Request
{

    // protected $redirect = '/contact'.$request->item_category_id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool


     */


     public function response(array $errors){
      //echo 'FormRequest';
     // return 'FormRequest';
       return Redirect::back()->withErrors($errors)->withInput()->with('type', '2');
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * 
     * @return array
     */
    public function rules()
    {
      return [
        'item_name'=>'required',
        'item_price'=>'required|numeric',
        'description'=>'required',
        'image_files'=>'required|array',
        /*'images.*'=>'required|image', */
        'item_category_id'=>'required',
        /*'item_images.*'=>'image',*/
        'phone'=>'required'// images from admin panel 
        ];
    }
}
