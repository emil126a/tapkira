<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Redirect;
class RealEstateValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


public function response(array $errors){
        return Redirect::back()->withErrors($errors)->withInput()->with('type', '1');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
       // 'soil_area'=>'required|numeric',
        //'number_of_rooms'=>'required',
        'area'=>'required|numeric',
        'price'=>'required|numeric',
        'description'=>'required',
        'image_files'=>'required|array',
       // 'images.*'=>'required|image',/*|dimensions:min_width=400,min_height=600*/
        'address'=>'required',
        'real_estate_sub_menu_id'=>'required',
        'estate_images.*'=>'image',// images from admin panel 
        'phone'=>'required'
        ];
    }
}
