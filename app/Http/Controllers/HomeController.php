<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\ItemCategory;
use App\Item;
use App\ItemSubCategory;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $item_cat_subcat=ItemCategory::with('sub_category')->get()->toArray();

     /*     dd($item_cat_subcat);*/
        $item_image=Item::with('item_images')->with('item_category')->with('item_sub_category')->get()->toArray();

    // dd($item_image);

        return view('home');
    }
}
