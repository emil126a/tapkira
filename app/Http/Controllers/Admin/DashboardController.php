<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App,Validator,Hash,Auth;
use App\Http\Controllers\Controller,App\RealEstateCategory,App\RealEstate,App\Cities, App\Item,App\ItemSubCategory, Form;
use App\Http\Requests\RealEstateValidation,Image,App\RealEstateImage,File, Redirect,App\Http\Requests\ItemValidation;

class DashboardController extends Controller
{
  public function __construct(){
    $this->middleware('admin');
    Carbon::setLocale('az');


    $data['count_active_real_estates']=RealEstate::where('active','1')->count();
    $data['count_passive_real_estates']=RealEstate::where('active','0')->count();
    $data['count_archived_real_estates']=RealEstate::where('active','2')->count();




    $data['count_active_items']=Item::where('active','1')->count();
    $data['count_passive_items']=Item::where('active','0')->count();
    $data['count_archived_items']=Item::where('active','2')->count();




    $data['count_active_services']=\App\Service::where('active','1')->count();
    $data['count_passive_services']=\App\Service::where('active','0')->count();
    $data['count_archived_services']=\App\Service::where('active','2')->count();

      view()->share('data',  $data);
  }


  public function real_estates($status=0){

    $real_estate_id=(isset($_GET['real_estate_id']))?$_GET['real_estate_id']:'`real_estates`.`id`';
    $status2=(isset($_GET['real_estate_id']))?'`real_estates`.`active`':$status;



    $data['real_estates']=RealEstate::with('real_estate_category')
    ->with('city')
    ->with('real_estate_images')
    //->where('active',$status)
    ->whereRaw("`real_estates`.`active` =$status2")  
 //   ->whereRaw("`real_estates`.`active` =$status2")  
    ->whereRaw("`real_estates`.`id` =$real_estate_id")           
    ->orderBy('id','DESC')->paginate(10);




    return view('admin.dashboard.real_estates',$data);
  }

  public function items($status)
  {
    $item_id=(isset($_GET['item_id']))?$_GET['item_id']:'`items`.`id`';
    $status2=(isset($_GET['item_id']))?'`items`.`active`':$status;


    $search_check=(isset($_GET['item_id']))?true:false;


    $data['items']=Item::with('item_category')
    ->with('city')
    ->with('item_images')
  //  ->where('active',$status)
    ->whereRaw("`items`.`active` =$status2")  
    ->whereRaw("`items`.`id` =$item_id")           
    ->orderBy('id','DESC')->paginate(10);


    $data['count_active_items']=Item::where('active','1')->count();
    $data['count_passive_items']=Item::where('active','0')->count();
    $data['count_archived_items']=Item::where('active','2')->count();

    return view('admin.dashboard.items',$data);
  }

  public function real_estate_edit_form($id)
  {

    $title               = 'title_' . App::getLocale();
    $data['real_estate_result']=App\RealEstate::with('user')->with('real_estate_images')->where('id', $id)->first();
      $data['real_estate_categories'] = RealEstateCategory::pluck($title, 'id')->all(); // our default
      $data['cities'] = Cities::pluck($title, 'id')->all(); // our default
      $data['real_estate_id']=$id;
      return view('admin.dashboard.real_estate_edit_form',$data);
    }


    public function rel_sub_real_estate(Request $request)
    {

      $real_estate_sub_menu_id= $request->real_estate_sub_menu_id;

      if($real_estate_sub_menu_id=='1' || $real_estate_sub_menu_id=='2' || $real_estate_sub_menu_id=="3")
      {
       return view('admin.dashboard.real_estate_apartment');
     }



       // return  $request->real_estate_sub_menu_id;
   }


   public function store_real_estate(App\Http\Requests\Admin\RealEstateValidation $request, $id)
   {

    //  die($id);
     $real_estate                          = App\RealEstate::find($id);
     $real_estate->real_estate_category_id = $request->real_estate_sub_menu_id;
     $real_estate->price = $request->price;
     $real_estate->area                    = $request->area;
     $real_estate->address                 = $request->address;

     $real_estate->description             = $request->description;
     $real_estate->lat                     = $request->lat;
     $real_estate->lng                     = $request->long;
     $real_estate->address                 = $request->address;
     $real_estate->real_estate_category_id = $request->real_estate_sub_menu_id;
        // $real_estate->priority_person=$request->priority_person;

     $real_estate->rent_type               = $request->rent_type;
     $real_estate->payment_period_type     = $request->payment_period_type;


     if($request->real_estate_sub_menu_id=='1' || $request->real_estate_sub_menu_id=='2' || $request->real_estate_sub_menu_id=='3')
     {
      $real_estate->room_number             = $request->number_of_rooms;
      $real_estate->client_priority         = $request->priority_person;
      $real_estate->real_estate_status      = $request->real_estate_status;
    }

    else
    {
      $real_estate->room_number             = null;
      $real_estate->client_priority         = null;
      $real_estate->real_estate_status      = null;
    }








        //$real_estate->user_id                 = Auth::user()->id;
    $real_estate->city_id                 = $request->real_estate_city_id;


    $real_estate->save();

    if ($request->hasFile('estate_images')) {


      $files = $request->file('estate_images');

      $upload_folder =public_path('/uploads/real_estate_images');
            //   $names = [];
            //dd($files);
      foreach ($files as $file) {
                // die('asd');
        $extension    = $file->getClientOriginalExtension();
        $renamed_file = uniqid('img_') . '.' . $extension;
        $move_file    = $file->move($upload_folder, $renamed_file);
        $watermark_path=public_path() . '/uploads/watermark.png';
        $img = Image::make($upload_folder.'/'. $renamed_file)->resize(800, null, function ($constraint) {
          $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
        $img->save();
                if ($move_file) //if file has moved
                {
                  $image                 = new RealEstateImage;
                  $image->image          = $renamed_file;
                  $image->real_estate_id = $real_estate->id;
                  $image->save();
                }
                //$names[] = $file->getClientOriginalName();
              }
            }


            return redirect(App::getLocale() . '/admin/real_estates/'.$real_estate->active)->with('message', 'Düzəliş edildi');
          }


          public function delete_real_estate_image($id)
          {

            $image=RealEstateImage::find($id);
            $path= 'uploads/real_estate_images/';
            File::delete($path .$image->image);  
            App\RealEstateImage::destroy($id);  
            return back();
          }


          public function delete_real_estate($id)
          {

            $real_estate_images=RealEstateImage::select('image')->where('real_estate_id','=',$id)->get();

     //    dd($image->pluck('filename')->toArray());
            if(count($real_estate_images)>0)
            {
              $path= 'uploads/real_estate_images/';

              foreach ($real_estate_images as $real_estate_image )
              {
               File::delete($path .$real_estate_image->image);
             }

    //        File::delete($path .$image->filename);  
           }
           RealEstate::destroy($id);  
           return back()->with('message', 'Silindi');
         }


         public function delete_item($id)
         {

          $item_images=\App\ItemImage::select('image')->where('item_id','=',$id)->get();

     //    dd($image->pluck('filename')->toArray());
          if(count($item_images)>0)
          {
            $path= 'uploads/item_images/';

            foreach ($item_images as $item_image )
            {
             File::delete($path .$item_image->image);
           }

    //        File::delete($path .$image->filename);  
         }
         \App\Item::destroy($id);  
         return back()->with('message', 'Silindi');
       }




       public function archive_real_estate($id)// active 2= archived ads
        {
           $real_estate                          = App\RealEstate::find($id);

          if($real_estate->active==1)
          {
           $real_estate->active                 = '2';//archive
          }
           else
           {
             $real_estate->active                 = '1';//active
             $real_estate->activated_at= \Carbon\Carbon::now();
             $real_estate->expired='no';
             $real_estate->notified='no';
           }

         $real_estate->save();
         return back();
       }


       public function archive_item($id)// active 2= archived ads
        {
           $item                          = \App\Item::find($id);

          if($item->active==1)
          {
           $item->active                 = '2';//archive
          }
          elseif($item->active==0)
          {
           $item->active                 = '2';//archive
          }
           else
           {
             $item->active                 = '1';//active
            $item->activated_at= \Carbon\Carbon::now();
            $item->expired='no';
            $item->notified='no';
           }

         $item->save();
         return back();
       }

       public function vip_real_estate($id)
       {
        $real_estate                          = App\RealEstate::find($id);

        if($real_estate->vip==1)//1 vip, 0 non-vip
        {
           $real_estate->vip                 = '0';//non vip
         }
         else
         {
           $real_estate->vip                 = '1';//vip
         }

         $real_estate->save();
         return back();
       }
 public function vip_item($id)
       {
        $item                          = \App\Item::find($id);

        if($item->vip==1)//1 vip, 0 non-vip
        {
           $item->vip                 = '0';//non vip
         }
         else
         {
           $item->vip                 = '1';//vip
         }

         $item->save();
         return back();
       }

       public function real_estate_checbox_action(Request $request)
       {



  //    $input=$request->all();

        if(isset($request->activate))
        {
          foreach ($request->checkbox as $key =>$real_estate_id)
          {
            $real_estate = App\RealEstate::find($real_estate_id);
            $real_estate->active= '1';
            $real_estate->activated_at= \Carbon\Carbon::now();
            $real_estate->expired='no';
            $real_estate->notified='no';
            $real_estate->save();

            
          }
          return back();
        }

        elseif (isset($request->delete)) {

         foreach ($request->checkbox as $key =>$real_estate_id)
         {
          $this->delete_real_estate($real_estate_id);


        }
        return back();

      }

      else
      {
        foreach ($request->checkbox as $key =>$real_estate_id)
        {
          $real_estate = App\RealEstate::find($real_estate_id);
          $real_estate->active= '2';
          $real_estate->save();


        }
        return back();
      }
    }



public function item_checbox_action(Request $request)
       {



  //    $input=$request->all();

        if(isset($request->activate))
        {
          foreach ($request->checkbox as $key =>$item)
          {
            $item = App\Item::find($item);
            $item->active= '1';
            $item->activated_at= \Carbon\Carbon::now();
            $item->expired='no';
            $item->notified='no';
            $item->save();

            
          }
          return back();
        }

        elseif (isset($request->delete)) {

         foreach ($request->checkbox as $key =>$item)
         {
          $this->delete_item($item);


        }
        return back();

      }

      else
      {
        foreach ($request->checkbox as $key =>$real_estate_id)
        {
          $real_estate = \App\Item::find($real_estate_id);
          $real_estate->active= '2';
          $real_estate->save();


        }
        return back();
      }
    }


    public function item_edit_form($id)
    {
      $title               = 'title_' . App::getLocale();
      $data['item_result']=App\Item::with('user')->with('item_images')->where('id', $id)->first();
      $data['item_categories'] = App\ItemCategory::pluck($title, 'id')->all(); // our default
      $data['cities'] = Cities::pluck($title, 'id')->all(); // our default
      $data['item_id']=$id;
      return view('admin.dashboard.item_edit_form',$data);
    }


    public function related_sub_item_category(Request $request)
    {
      $title                     = 'title_' . App::getLocale();
      $related_item_sub_category = ItemSubCategory::where('item_category_id', $request->rel_item_sub_category)->pluck($title, 'id');

      $result = ($related_item_sub_category->count() > 0) ? '<div class="form-group">
      <label for="email" class="col-md-4 control-label">Alt kateqoriya</label>

      <div class="col-md-6">'.

       Form::select('sub_menu_id', $related_item_sub_category, null, array(
        'class' => 'form-control'
        ))

       .'</div>
     </div>'  : '';


     return $result;
   }



   public function store_item(App\Http\Requests\Admin\ItemValidation $request, $id)
   {
    $item                       = App\Item::find($id);
    $item->description          = $request->description;
    $item->city_id              = $request->item_city_id;
    $item->rent_type            = $request->item_ad_type;
    $item->item_category_id     = $request->item_category_id;
    $item->item_sub_category_id = $request->sub_menu_id;
    $item->title                = $request->item_name;
    $item->price                = $request->item_price;
            $item->is_new               = ($request->is_new==null)?0:1;//0-old, 1-new
            $item->payment_period_type     = $request->payment_period_type;
        //    $item->user_id                 = Auth::user()->id;
            $item->save();

            if ($request->hasFile('item_images')) {


              $files = $request->file('item_images');

              $upload_folder =public_path('/uploads/item_images');
            //   $names = [];
            //dd($files);
              foreach ($files as $file) {
                // die('asd');
                $extension    = $file->getClientOriginalExtension();
                $renamed_file = uniqid('img_') . '.' . $extension;
                $move_file    = $file->move($upload_folder, $renamed_file);
                $watermark_path=public_path() . '/uploads/watermark.png';
                $img = Image::make($upload_folder.'/'. $renamed_file)->resize(800, null, function ($constraint) {
                  $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                $img->save();
                if ($move_file) //if file has moved
                {
                  $image                 = new \App\ItemImage;
                  $image->image          = $renamed_file;
                  $image->item_id = $item->id;
                  $image->save();
                }
                //$names[] = $file->getClientOriginalName();
              }
            }
            return redirect(App::getLocale() . '/admin/items/'.$item->active)->with('message', 'Düzəliş edildi');
           // return redirect(App::getLocale() . '/success')->with('message', 'Əşyalar')->with('id', $item->id); 
    }


          public function delete_item_image($id)
          {

            $image=\App\ItemImage::find($id);
            $path= 'uploads/item_images/';
            File::delete($path .$image->image);  
            \App\ItemImage::destroy($id);  
            return back();
          }


          public function services($status)
          {
                $service_id=(isset($_GET['service_id']))?$_GET['service_id']:'`services`.`id`';
                $status2=(isset($_GET['service_id']))?'`services`.`active`':$status;


                //$search_check=(isset($_GET['service_id']))?true:false;


                $data['services']=\App\Service::with('service_category')
                ->with('city')
                ->with('service_images')
              //  ->where('active',$status)
                ->whereRaw("`services`.`active` =$status2")  
                ->whereRaw("`services`.`id` =$service_id")           
                ->orderBy('id','DESC')->paginate(10);


                $data['count_active_services']=\App\Service::where('active','1')->count();
                $data['count_passive_services']=\App\Service::where('active','0')->count();
                $data['count_archived_services']=\App\Service::where('active','2')->count();

                return view('admin.dashboard.services',$data);
          }

          /*public function active_item($id)
          {

           $item = App\Item::find($id);
           $item->active= '1';
           $item->save();
           return back();
         }
*/



    public function service_edit_form($id)
    {
      $title               = 'title_' . App::getLocale();
      $data['service_result']=App\Service::with('user')->with('service_images')->where('id', $id)->first();
      $data['service_categories'] = App\ServiceCategory::pluck($title, 'id')->all(); // our default
      $data['cities'] = Cities::pluck($title, 'id')->all(); // our default
      $data['service_id']=$id;
      return view('admin.dashboard.service_edit_form',$data);
    }


       public function store_service(App\Http\Requests\Admin\ServiceValidation $request, $id)
   {

    $service                          = App\Service::find($id);
    $service->description             = $request->description;
    $service->city_id                 = $request->service_city_id;
    $service->service_ad_type         = $request->service_ad_type;
    $service->service_category_id     = $request->service_category_id;
    $service->service_sub_category_id = $request->sub_menu_id;
    $service->title                   = $request->service_name;
    $service->price                   = $request->service_price;
            
      //      $service->payment_period_type     = $request->payment_period_type;
        //    $service->user_id                 = Auth::user()->id;
            $service->save();

            if ($request->hasFile('service_images')) {


              $files = $request->file('service_images');

              $upload_folder =public_path('/uploads/service_images');
            //   $names = [];
            //dd($files);
              foreach ($files as $file) {
                // die('asd');
                $extension    = $file->getClientOriginalExtension();
                $renamed_file = uniqid('img_') . '.' . $extension;
                $move_file    = $file->move($upload_folder, $renamed_file);
                $watermark_path=public_path() . '/uploads/watermark.png';
                $img = Image::make($upload_folder.'/'. $renamed_file)->resize(800, null, function ($constraint) {
                  $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                $img->save();
                if ($move_file) //if file has moved
                {
                  $image                 = new \App\serviceImage;
                  $image->image          = $renamed_file;
                  $image->service_id = $service->id;
                  $image->save();
                }
                //$names[] = $file->getClientOriginalName();
              }
            }




            return redirect(App::getLocale() . '/admin/services/'.$service->active)->with('message', 'Düzəliş edildi');
           // return redirect(App::getLocale() . '/success')->with('message', 'Əşyalar')->with('id', $item->id); 
    }

    public function related_sub_service_category(Request $request)
    {
      $title                     = 'title_' . App::getLocale();
      $related_service_sub_category = \App\ServiceSubCategory::where('service_category_id', $request->rel_service_sub_category)->pluck($title, 'id');

      $result = ($related_service_sub_category->count() > 0) ? '<div class="form-group">
      <label for="email" class="col-md-4 control-label">Alt kateqoriya</label>

      <div class="col-md-6">'.

       Form::select('sub_menu_id', $related_service_sub_category, null, array(
        'class' => 'form-control'
        ))

       .'</div>
     </div>'  : '';


     return $result;
    }


    public function delete_service_image($id)
    {

            $image=\App\ServiceImage::find($id);
            $path= 'uploads/service_images/';
            File::delete($path .$image->image);  
            \App\ServiceImage::destroy($id);  
            return back();
    }

public function delete_service($id)
         {

          $service_images=\App\ServiceImage::select('image')->where('service_id','=',$id)->get();

     //    dd($image->pluck('filename')->toArray());
          if(count($service_images)>0)
          {
            $path= 'uploads/service_images/';

            foreach ($service_images as $service_image )
            {
             File::delete($path .$service_image->image);
           }

    //        File::delete($path .$image->filename);  
         }
         \App\Service::destroy($id);  
         return back()->with('message', 'Silindi');
       }

         public function archive_service($id)// active 2= archived ads
        {
           $service                          = \App\Service::find($id);

          if($service->active==1)
          {
           $service->active                 = '2';//archive
          }
          elseif($service->active==0)
          {
           $service->active                 = '2';//archive
          }
           else
           {
             $service->active                 = '1';//active
             $service->activated_at= \Carbon\Carbon::now();
             $service->expired='no';
             $service->notified='no';
           }

         $service->save();
         return back();
       }


        public function vip_service($id)
       {
        $service                          = \App\Service::find($id);

        if($service->vip==1)//1 vip, 0 non-vip
        {
           $service->vip                 = '0';//non vip
         }
         else
         {
           $service->vip                 = '1';//vip
         }

         $service->save();
         return back();
       }


       public function service_checbox_action(Request $request)
       {



  //    $input=$request->all();

        if(isset($request->activate))
        {
          foreach ($request->checkbox as $key =>$item)
          {
            $service = App\Service::find($item);
            $service->active= '1';
            $service->activated_at= \Carbon\Carbon::now();
            $service->expired='no';
            $service->notified='no';
            $service->save();

            
          }
          return back();
        }

        elseif (isset($request->delete)) {

         foreach ($request->checkbox as $key =>$item)
         {
          $this->delete_service($item);


        }
        return back();

      }

      else
      {
        foreach ($request->checkbox as $key =>$real_estate_id)
        {
          $service = \App\Service::find($real_estate_id);
          $service->active= '2';
          $service->save();


        }
        return back();
      }
    }

    public function users()
    {
      $data['users']=\App\User::all();
      return view('admin/dashboard/users',$data);
    }


    public function edit_user($id)
    {
      
      $data['user']=\App\User::find($id);
      return view('admin..dashboard.edit_user',$data);
     
    }


    public function delete_user($id)
    {
      $user=\App\User::find($id);
      $user->delete();

      return back();
    }


    public function save_user(Request $request)
    {
           $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',            
            'password'=>'same:password_confirmation',
            'password_confirmation'=>'min:3'


        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();;
        }

       $user=\App\User::find($request->id);

       $user->name=$request->name;
       $user->email=$request->email;
       $user->phone=$request->phone;

       if($request->password!='')
       {
          $user->password=Hash::make($request->password);
       }

       $user->save();

       return redirect(App::getLocale().'/admin/users');

    }


    public function account()
    {
        return view('admin.dashboard.account');
    }


    public function save_account(Request $request)
    {
          $validator = Validator::make($request->all(), [
            'password'=>'required|same:password_confirmation',
            'password_confirmation'=>'required|min:3'


        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();;
        }


         $admin=\App\Admin::find(Auth::guard('admin')->user()->id);
         $admin->password=Hash::make($request->password);
         $admin->save();

        return back()->with('message','Şifrəniz müvəffəqiyyətlə dəyişdirildi');
    }

  }
