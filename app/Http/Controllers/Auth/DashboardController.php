<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RealEstateValidation;
use App\Http\Requests\ItemValidation;
use App\Http\Requests\ServiceValidation;
use App\Http\Requests\UserPrivateDataValidation;
use App\Http\Requests\UserPasswordChangeValidation,Response;
use App\Cities, Auth,Input;
use Image, Carbon, Hash, File;
use App\ItemCategory, Form, App\ServiceCategory, App\RealEstateCategory, App, App\ItemSubCategory, App\ServiceSubCategory, Validator, Redirect, App\RealEstateImage, App\ItemImage, App\RealEstate;



class DashboardController extends Controller
{


 // public $user_id;

  public function __construct()

  {


    $this->middleware('auth');
    Carbon::setLocale(App::getLocale());

   // $this->user_id=Auth::user()->id;
  }


  public function add()
  {
        //ItemCategory::with('sub_category')->get()->toArray()


        //  dd(RealEstate::with('user')->get()->toArray());
    $title               = 'title_' . App::getLocale();
    $data['cities']      = Cities::all()->pluck($title, 'id');
        //$data['cities']=Cities::all()->pluck($title,'id');
        $data['real_estate'] = RealEstateCategory::all()->pluck($title, 'id'); // our default 
        $data['item']        = ItemCategory::all()->pluck($title, 'id');
        $data['service']     = ServiceCategory::all()->pluck($title, 'id');
        
        //   $data['url']='save_real_estate';
        return view('auth.dashboard.add', $data);
      }


      public function rel_sub(Request $request)
      {

        $select_option_value = $request->rel_sub;
        $title               = 'title_' . App::getLocale();
        
        
        if ($select_option_value == '1') {
          $data['real_estate'] = RealEstateCategory::all()->pluck($title, 'id');
          $data['cities']      = Cities::all()->pluck($title, 'id');
            //     $data['url']='save_real_estate';
          return view('auth.dashboard.real_estates', $data);
        } elseif ($select_option_value == '2') {

          $data['item']   = ItemCategory::all()->pluck($title, 'id');
          $data['cities'] = Cities::all()->pluck($title, 'id');
            //   $data['item_sub_category']=ItemSubCategory::all()->pluck($title,'id');
            // $data['url']='save_item';
          return view('auth.dashboard.items', $data);
        }
        
        else {
          $data['service'] = ServiceCategory::all()->pluck($title, 'id');
          $data['cities'] = Cities::all()->pluck($title, 'id');
            //   $data['url']='save_services';
          return view('auth.dashboard.services', $data);
        }
        
        
        //    return $request->rel_sub;
        
        //return '<br><label>Alt kateqoriya</label>'.Form::select('sub_menu_id',$item->toArray(),null,array('class'=>'form-control','onchange'=>'related_sub_sub_category()'));
      }


      public function rel_item_sub_category(Request $request)
      {
        $title                     = 'title_' . App::getLocale();
        $related_item_sub_category = ItemSubCategory::where('item_category_id', $request->rel_item_sub_category)->pluck($title, 'id');
        
        $result = ($related_item_sub_category->count() > 0) ? '<div class="form-group"><label>'.trans('common.item_sub_category').'</label>' . Form::select('sub_menu_id', $related_item_sub_category, null, array(
          'class' => 'form-control'
          )) .'</div>': '';
        
        
        return $result;
      }


      public function rel_service_sub_category(Request $request)
      {
        $title                        = 'title_' . App::getLocale();
        $related_service_sub_category = ServiceSubCategory::where('service_category_id', $request->rel_service_sub_category)->pluck($title, 'id');
        
        $result = ($related_service_sub_category->count() > 0) ? '<div class="form-group"><label>Xidmətin  növü</label>' . Form::select('service_sub_category_id', $related_service_sub_category, null, array(
          'class' => 'form-control'
          )).'</div>': '';
        
        
        return $result;
      }


      public function storeRealestate(RealEstateValidation $request)
      {

        $real_estate                          = new RealEstate();
        $real_estate->real_estate_category_id = $request->sub_menu_id;
        $real_estate->price = $request->price;
        $real_estate->area                    = $request->area;
        $real_estate->address                 = $request->address;
        $real_estate->room_number             = $request->number_of_rooms;
        $real_estate->description             = $request->description;
        $real_estate->lat                     = $request->lat;
        $real_estate->lng                     = $request->long;
        $real_estate->address                 = $request->address;
        $real_estate->real_estate_category_id = $request->real_estate_sub_menu_id;
        // $real_estate->priority_person=$request->priority_person;
        $real_estate->real_estate_status      = $request->real_estate_status;
        $real_estate->rent_type               = $request->real_estate_ad_type;
        $real_estate->payment_period_type     = $request->payment_period_type;
        $real_estate->client_priority         = $request->priority_person;
        $real_estate->user_id                 = Auth::user()->id;
        $real_estate->city_id                 = $request->real_estate_city_id;
          
        $real_estate->save();
        

          foreach ($request->image_files as $request->image_file) {
            $image                 = new RealEstateImage;
                  $image->image          = $request->image_file;
                  $image->real_estate_id = $real_estate->id;
                  $image->save(); 
        }

              $user_update=\App\User::find(Auth::user()->id);
              $user_update->phone=$request->phone;
              $user_update->save();
              return redirect(App::getLocale() . '/success')->with('message', trans('common.real_estate'))->with('id', $real_estate->id);

          }


          public function storeItem(ItemValidation $request)
          {
            $item                       = new App\Item();
            $item->description          = $request->description;
            $item->city_id              = $request->item_city_id;
            $item->rent_type            = $request->item_ad_type;
            $item->item_category_id     = $request->item_category_id;
            $item->item_sub_category_id = $request->sub_menu_id;
            $item->title                = $request->item_name;
            $item->price                = $request->item_price;
            $item->is_new               = ($request->is_new==null)?0:1;//0-old, 1-new
            $item->payment_period_type     = $request->payment_period_type;
            $item->user_id                 = Auth::user()->id;
            $item->save();


 
 foreach ($request->image_files as $request->image_file) {
            $image                 =  new \App\ItemImage;
                  $image->image          = $request->image_file;
                  $image->item_id = $item->id;
                  $image->save(); 
        }

          
  $user_update=\App\User::find(Auth::user()->id);
    $user_update->phone=$request->phone;
    
    $user_update->save();
        
          
        return redirect(App::getLocale() . '/success')->with('message', trans('common.items'))->with('id', $item->id);


          }


          public function storeService(ServiceValidation $request)
          {
            $service                          = new App\Service();
            $service->title                   = $request->service_name;
            $service->description             = $request->description;
            $service->service_category_id     = $request->service_category_id;
            $service->service_sub_category_id = $request->service_sub_category_id;
            $service->service_name            = $request->service_name;
            $service->service_ad_type         = $request->service_ad_type;
            $service->price                   = $request->service_price;
            $service->city_id                 = $request->service_city_id;
            $service->user_id                 =  Auth::user()->id;
            $service->save();

             foreach ($request->image_files as $request->image_file) {
            $image                 = new \App\ServiceImage;
                  $image->image          = $request->image_file;
                  $image->service_id = $service->id;
                  $image->save(); 
        }

          $user_update=\App\User::find(Auth::user()->id);
    $user_update->phone=$request->phone;
    
    $user_update->save();

            return redirect(App::getLocale() . '/success')->with('message', trans('common.services'))->with('id', $service->id);
          }


          public function success()
          {
            return view('auth.dashboard.success');
          }

          public function rel_sub_real_estate(Request $request)
          {
            $real_estate_sub_menu_id= $request->real_estate_sub_menu_id;

            if($real_estate_sub_menu_id=='1' || $real_estate_sub_menu_id=='2' || $real_estate_sub_menu_id=="3")
            {
             return view('auth.dashboard.real_estate_apartment');
           }
           else
           {
             return view('auth.dashboard.real_estate_soil');
           }


         }


         public function dashboard_profile()
         {


          if(Auth::user()->facebook_user_id!=0)//if facebook user then we won't allow him to see profile page
          {
             return redirect(App::getLocale() . '/myads') ;
          }
           return view('auth.dashboard.dashboard_profile');
          
        }

         

        public function myads()
        {



       $data['real_estates']=App\RealEstate::with('real_estate_category')
            ->with('city')
            ->with('real_estate_images')
            ->where('user_id',Auth::user()->id)                                    
            ->orderBy('id','DESC')->get();

            $data['items']=App\Item::with('item_category')
            ->with('city')
            ->with('item_images')
            ->where('user_id',Auth::user()->id)                                    
            ->orderBy('id','DESC')->get(); 


            $data['services']=App\Service::with('service_category')
            ->with('city')
            ->with('service_images')
            ->where('user_id',Auth::user()->id)                               
            ->orderBy('id','DESC')->get(); 

            
          return view('auth.dashboard.dashboard_my_ads',$data);
        }


        public function related_dashboard(Request $request)
        {
          $cat_id=$request->id;




          if($cat_id==0)
          {
            $data['real_estates']=App\RealEstate::with('real_estate_category')
            ->with('city')
            ->with('real_estate_images')
            ->where('user_id',Auth::user()->id)                                    
            ->orderBy('id','DESC')->get();

            $data['items']=App\Item::with('item_category')
            ->with('city')
            ->with('item_images')
            ->where('user_id',Auth::user()->id)                                    
            ->orderBy('id','DESC')->get(); 


            $data['services']=App\Service::with('service_category')
            ->with('city')
            ->with('service_images')
            ->where('user_id',Auth::user()->id)                               
            ->orderBy('id','DESC')->get(); 


            
            return view('auth.dashboard.related_all_ads',$data);
          }
          elseif($cat_id==1)
          {
            $data['data_list']=App\RealEstate::with('real_estate_category')
            ->with('city')
            ->with('real_estate_images')
            ->where('user_id',Auth::user()->id)                                    
            ->orderBy('id','DESC')->get();

            
            return view('auth.dashboard.related_dashboard_real_estates',$data);
          }
          elseif($cat_id==2){


            $data['data_list']=App\Item::with('item_category')
            ->with('city')
            ->with('item_images')
            ->where('user_id',Auth::user()->id)                                    
            ->orderBy('id','DESC')->get(); 
            
            return view('auth.dashboard.related_dashboard_items',$data);
          }
          else
          {
            $data['data_list']=App\Service::with('service_category')
            ->with('city')
            ->with('service_images')
            ->where('user_id',Auth::user()->id)                               
            ->orderBy('id','DESC')->get(); 
            
          //  dd($data['data_list']->toArray());

            return view('auth.dashboard.related_dashboard_services',$data);


          }
        }


        public function save_user_data(UserPrivateDataValidation $request)
        {

            $user                             =  App\User::find(Auth::user()->id);
            $user->email                      = $request->email;    
            $user->name                      = $request->fullname; 
            $user->phone                      = $request->phone;     

            $user->save();

         return  redirect(App::getLocale().'/dashboard_profile/')->with('message',trans('common.success_message_user_info_save'));


        }


        public function save_password_change(UserPasswordChangeValidation $request)
        {

              $user = App\User::find(Auth::user()->id);

          if(!Hash::check($request->old_password, $user->password))
          {

            return  redirect(App::getLocale().'/dashboard_profile/')->with('error',trans('common.error_password_no_match_db'));
         //  return back()->with('error','The specified password does not match the database password');
         }
         else
         {
               $user->password                      = Hash::make($request->new_password);                    
                $user->save();
              return  redirect(App::getLocale().'/dashboard_profile/')->with('password_message',trans('common.success_message_user_info_save'));
         }

       
        }


        public function delete_real_estate($id)
    {

        $count_result=RealEstate::where('id', $id)->where('user_id', Auth::user()->id)->get()->toArray(); 

    
        if(count($count_result)>0)
        {

              $real_estate_images=RealEstateImage::select('image')->where('real_estate_id','=',$id)->get();

     //    dd($image->pluck('filename')->toArray());
              if(count($real_estate_images)>0)
              {
                  $path= 'uploads/real_estate_images/';

                  foreach ($real_estate_images as $real_estate_image )
                  {
                     File::delete($path .$real_estate_image->image);
                  }

    //        File::delete($path .$image->filename);  
             }
              RealEstate::destroy($id);  
              return redirect(App::getLocale() . '/myads')->with('message',trans('common.post_deleted'));
        }

        else
        {
            return redirect(App::getLocale() . '/myads') ;
        }
 
    }


    public function delete_item($id)
    {
        $count_result=App\Item::where('id', $id)->where('user_id', Auth::user()->id)->get()->toArray(); 

    
        if(count($count_result)>0)
        {

              $item_images=App\ItemImage::select('image')->where('item_id','=',$id)->get();

     //    dd($image->pluck('filename')->toArray());
              if(count($item_images)>0)
              {
                  $path= 'uploads/item_images/';

                  foreach ($item_images as $item_image )
                  {
                     File::delete($path .$item_image->image);
                  }

    //        File::delete($path .$image->filename);  
             }
              App\Item::destroy($id);  
              return redirect(App::getLocale() . '/myads')->with('message',trans('common.post_deleted'));
        }

        else
        {
            return redirect(App::getLocale() . '/myads') ;
        }
    }


public function delete_service($id)
    {
        $count_result=App\Service::where('id', $id)->where('user_id', Auth::user()->id)->get()->toArray(); 

    
        if(count($count_result)>0)
        {

              $service_images=App\ServiceImage::select('image')->where('service_id','=',$id)->get();

     //    dd($image->pluck('filename')->toArray());
              if(count($service_images)>0)
              {
                  $path= 'uploads/service_images/';

                  foreach ($service_images as $service_image )
                  {
                     File::delete($path .$service_image->image);
                  }

    //        File::delete($path .$image->filename);  
             }
              App\Service::destroy($id);  
              return redirect(App::getLocale() . '/myads')->with('message',trans('common.post_deleted'));
        }

        else
        {
            return redirect(App::getLocale() . '/myads') ;
        }
    }


            public function upload_real_estate_images_ajax(Request $request)
            {
              if($request->hasFile('myfile')){
                  $watermark_path=public_path() . '/uploads/watermark.png';
                  $destinationPath = public_path('/uploads/real_estate_images');

           
                $files = $request->file('myfile');

               // var_dump($request->file('image'));
               // $file = $request->file('myfile'); // your file upload input field in the form should be named 'file'

                // Declare the rules for the form validation.
                $rules = array('myfile'  => 'mimes:jpg,jpeg,bmp,png');
                $data = array('myfile' => $files);

                // Validate the inputs.
                $validation = Validator::make($data, $rules);

                if ($validation->fails())
                {
                    return Response::json('error', 400);
                }

                if(is_array($files))
                {
                    foreach($files as $file) {
                      $extension    = $file->getClientOriginalExtension();
                      $renamed_file = uniqid('img_') . '.' . $extension;
                     // $uploadSuccess    = $file->move($destinationPath, $renamed_file);
                      $img = Image::make($file->getRealPath())->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                      $img->save($destinationPath.'/'. $renamed_file);


                       // $part->move($destinationPath, $filename);
                    }
                }
                else //single file
                {
                   // $filename = $files->getClientOriginalExtension();
                      $extension    = $files->getClientOriginalExtension();
                      $renamed_file = uniqid('img_') . '.' . $extension;
$img = Image::make($files->getRealPath())->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                      $img->save($destinationPath.'/'. $renamed_file);

                  //  $uploadSuccess = $files->move($destinationPath, $renamed_file);
                }

                if( $img ) {
                    return $renamed_file;
                } else {
                    return Response::json('error', 400);
                }
}
             
 
             
          }


                      public function upload_item_images_ajax(Request $request)
            {
              if($request->hasFile('myfile')){
                  $watermark_path=public_path() . '/uploads/watermark.png';
                  $destinationPath = public_path('/uploads/item_images');

           
                $files = $request->file('myfile');

               // var_dump($request->file('image'));
               // $file = $request->file('myfile'); // your file upload input field in the form should be named 'file'

                // Declare the rules for the form validation.
                $rules = array('myfile'  => 'mimes:jpg,jpeg,bmp,png');
                $data = array('myfile' => $files);

                // Validate the inputs.
                $validation = Validator::make($data, $rules);

                if ($validation->fails())
                {
                    return Response::json('error', 400);
                }

                if(is_array($files))
                {
                    foreach($files as $file) {
                      $extension    = $file->getClientOriginalExtension();
                      $renamed_file = uniqid('img_') . '.' . $extension;
                     // $uploadSuccess    = $file->move($destinationPath, $renamed_file);
                      $img = Image::make($file->getRealPath())->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                      $img->save($destinationPath.'/'. $renamed_file);


                       // $part->move($destinationPath, $filename);
                    }
                }
                else //single file
                {
                   // $filename = $files->getClientOriginalExtension();
                      $extension    = $files->getClientOriginalExtension();
                      $renamed_file = uniqid('img_') . '.' . $extension;
$img = Image::make($files->getRealPath())->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                      $img->save($destinationPath.'/'. $renamed_file);

                  //  $uploadSuccess = $files->move($destinationPath, $renamed_file);
                }

                if( $img ) {
                    return $renamed_file;
                } else {
                    return Response::json('error', 400);
                }
}
             
 
             
          }


            public function upload_service_images_ajax(Request $request)
            {
              if($request->hasFile('myfile')){
                  $watermark_path=public_path() . '/uploads/watermark.png';
                  $destinationPath = public_path('/uploads/service_images');

           
                $files = $request->file('myfile');

               // var_dump($request->file('image'));
               // $file = $request->file('myfile'); // your file upload input field in the form should be named 'file'

                // Declare the rules for the form validation.
                $rules = array('myfile'  => 'mimes:jpg,jpeg,bmp,png');
                $data = array('myfile' => $files);

                // Validate the inputs.
                $validation = Validator::make($data, $rules);

                if ($validation->fails())
                {
                    return Response::json('error', 400);
                }

                if(is_array($files))
                {
                    foreach($files as $file) {
                      $extension    = $file->getClientOriginalExtension();
                      $renamed_file = uniqid('img_') . '.' . $extension;
                     // $uploadSuccess    = $file->move($destinationPath, $renamed_file);
                      $img = Image::make($file->getRealPath())->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                      $img->save($destinationPath.'/'. $renamed_file);


                       // $part->move($destinationPath, $filename);
                    }
                }
                else //single file
                {
                   // $filename = $files->getClientOriginalExtension();
                      $extension    = $files->getClientOriginalExtension();
                      $renamed_file = uniqid('img_') . '.' . $extension;
$img = Image::make($files->getRealPath())->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
})->insert($watermark_path, 'center');// resize only the width of the image
                      $img->save($destinationPath.'/'. $renamed_file);

                  //  $uploadSuccess = $files->move($destinationPath, $renamed_file);
                }

                if( $img ) {
                    return $renamed_file;
                } else {
                    return Response::json('error', 400);
                }
}
             
 
             
          }
          public function delete_real_estate_images_ajax(Request $request)
          {

            $path= 'uploads/real_estate_images/';
            File::delete($path .$request->filename);  


           // $path= 'uploads/real_estate_images';
          //  $file_path=$path .$request->filename;

            
                 //  File::delete($path);
                
                         
          }

           public function delete_item_images_ajax(Request $request)
          {

            $path= 'uploads/item_images/';
            File::delete($path .$request->filename);  


           // $path= 'uploads/real_estate_images';
          //  $file_path=$path .$request->filename;

            
                 //  File::delete($path);
                
                         
          }



  public function delete_service_images_ajax(Request $request)
          {

            $path= 'uploads/service_images/';
            File::delete($path .$request->filename);  


           // $path= 'uploads/real_estate_images';
          //  $file_path=$path .$request->filename;

            
                 //  File::delete($path);
                
                         
          }



      }