<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon;


use App\Http\Requests\RealEstateValidation,File;
use Mail;
use App\Http\Requests\ItemValidation;
use App\Http\Requests\ServiceValidation;
use App\Http\Requests\ContactValidation;
use App\Cities;
use App\ItemCategory,Form,App\ServiceCategory,App\RealEstateCategory,App,App\ItemSubCategory,App\ServiceSubCategory, Validator, Redirect,App\RealEstateImage,App\RealEstate;
use Illuminate\Support\Facades\Input;

class GuestController extends Controller
{

public function __construct()
{
  Carbon::setLocale(App::getLocale());
}

 
	public $timestamps = false;
	public function index()
	{
    
    $title               = 'title_' . App::getLocale();
    $data['cities']      = Cities::pluck($title, 'id')->toArray();
    $data['real_estate_categories'] = RealEstateCategory::pluck($title, 'id')->all(); // our default
    $data['item_categories'] = ItemCategory::pluck($title, 'id')->all(); // our default
    $data['sub_item_categories']=App\ItemSubCategory::where('item_category_id',1)->pluck('title_'.App::getLocale(), 'id')->all();//our default
    $data['service_categories'] = ServiceCategory::pluck($title, 'id')->all(); // our default
    $data['real_estates']=App\RealEstate::with('real_estate_category')->with('city')->with('real_estate_images')->where('active',1)->take(8)->orderBy('vip','DESC')->orderBy('id','DESC')->get();

    $data['items']=App\Item::with('item_category')->with('city')->with('item_images')->where('active',1)->take(8)->orderBy('vip','DESC')->orderBy('id','DESC')->get();
    $data['services']=App\Service::with('service_category')->with('city')->with('service_images')->where('active',1)->take(8)->orderBy('vip','DESC')->orderBy('id','DESC')->get();

    $data['count_real_estates_per_day']=App\RealEstate::whereDay('activated_at', '=', date('d'))->where('active',1)->get();
    $data['count_items_per_day']=App\Item::whereDay('activated_at', '=', date('d'))->where('active',1)->get();
    $data['count_service_per_day']=App\Service::whereDay('activated_at', '=', date('d'))->where('active',1)->get();

 

		return view('guest/homepage',$data);
	}


 public function real_estate_list($cat_id, Request $request)
	{

      $city_id=($request->has('city_id')  || $request->city_id!='')?$request->city_id:'`real_estates`.`city_id`'; 
      $rent_type=($request->has('rent_type')  || $request->rent_type!='')?$request->rent_type:'`real_estates`.`rent_type`';  
    

      $min_price=($request->has('min_price') || $request->min_price!='' )?$request->min_price:'`real_estates`.`price`';
      $max_price=($request->has('max_price') || $request->max_price!='')?$request->max_price:'`real_estates`.`price`';
      $apartment_type=($request->has('apartment_type') || $request->apartment_type!='')?$request->apartment_type:'`real_estates`.`real_estate_status`';
      $city_id=($request->has('city_id') || $request->city_id!='')?$request->city_id:'`real_estates`.`city_id`';
      $room_numbers=($request->has('room_numbers') || $request->room_numbers!='')?$request->room_numbers:'`real_estates`.`room_number`';
      $payment_period_type=($request->has('payment_period_type') || $request->payment_period_type!='')?$request->payment_period_type:'`real_estates`.`payment_period_type`';
      $cat_id=($request->has('cat_id') || $request->cat_id!='')?$request->cat_id:$cat_id;
    
      $priority_person=($request->has('priority_person') || $request->priority_person!='')?$request->priority_person:'`real_estates`.`client_priority`';

      $data['max_price'] =  $request->max_price;      




if(($cat_id==1 || $request->cat_id==1) || ($cat_id==2 || $request->cat_id==2) || ($cat_id==3 || $request->cat_id==3))
{

  $apartment_query=" and `real_estates`.`real_estate_status`=$apartment_type and `real_estates`.`room_number`=$room_numbers and `real_estates`.`client_priority`=$priority_person ";
}
else
{
   $apartment_query='';
}


		  $data['real_estate_list']=

        App\RealEstate::with('real_estate_category')
                      ->with('city')
                      ->with('real_estate_images')
                      ->where('real_estate_category_id',$cat_id)
                      ->where('active',1)
                      ->whereRaw("`real_estates`.`city_id` =$city_id")
                      ->whereRaw("`real_estates`.`rent_type` =$rent_type")
                      ->whereRaw("`real_estates`.`price`>=$min_price")
                      ->whereRaw("`real_estates`.`price`<=$max_price $apartment_query")                                    
                      ->whereRaw("`real_estates`.`payment_period_type`=$payment_period_type")                 
                      ->orderBy('vip','DESC')
                      ->orderBy('id','DESC')
                      ->paginate(6);

 $data['real_estate_list']->appends($request->all())->links();

		$title               = 'title_' . App::getLocale();
		$data['cities']      = Cities::pluck($title, 'id')->toArray();
		$data['cat_id']      =$cat_id;
    $data['request']     =$request;
        //$data['cities']=Cities::all()->pluck($title,'id');
    $data['categories'] = RealEstateCategory::pluck($title, 'id')->all(); // our default
		//dd($data['real_estate_list']->toArray());
        return view('guest/real_estate_list',$data);
    }


    public function real_estate($id){

    	$data['real_estate_result']=App\RealEstate::with('real_estate_category')->with('user')->with('real_estate_images')->where('id', $id)->where('active',1)->get();


	//dd($data['real_estate_result']->toArray());
    	return view('guest/real_estate',$data);
    }


    public function related_sub_real_estate_search_form(Request $req)
    {


    	$title               = 'title_' . App::getLocale();
    	$data['cities']      = Cities::pluck($title, 'id')->toArray(); 	    
        //$data['cities']=Cities::all()->pluck($title,'id');
        $data['categories'] = RealEstateCategory::all()->pluck($title, 'id'); // our default 
        $data['real_estate_sub_id']=$req->real_estate_sub_menu_id;



        $real_estate_sub_menu_id=$req->real_estate_sub_menu_id;

        if($real_estate_sub_menu_id=='1' || $real_estate_sub_menu_id=='2' || $real_estate_sub_menu_id=="3")
        {
        	return view('guest.real_estate_apartment_search_form',$data);
        }
        else
        {
        	return view('guest.real_estate_soil_search_form',$data);
        }

    }


    public function item_list($cat_id,$sub_cat_id=null)
    {
 
//   $request=new Request();

      $cat_id=(Input::get('cat_id')!='')?Input::get('cat_id'):$cat_id;
    // $sub_cat_id=(Input::get('sub_cat_id')!='')?Input::get('sub_cat_id'):$sub_cat_id;


//      $sub_cat_id=isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:$sub_cat_id;

    
    $city_id=(Input::get('city_id')!='')?Input::get('city_id'):'`items`.`city_id`'; 
    $payment_period_type=(Input::get('payment_period_type')!='')?Input::get('payment_period_type'):'`items`.`payment_period_type`'; 
    $min_price=(Input::get('min_price')!='')?Input::get('min_price'):'`items`.`price`';
    $max_price=(Input::get('max_price')!='')?Input::get('max_price'):'`items`.`price`';   
    $rent_type =(Input::get('rent_type')!='')?Input::get('rent_type'):'`items`.`rent_type`';  

 

    if($sub_cat_id!='')
    {
      $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'=`items`.`item_sub_category_id`';//from menu
    }
    else{

      if(Input::get('sub_cat_id')=='all')
      {
        $sub_cat_id1='=`items`.`item_sub_category_id`';
      }
      else{
        $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'IS NULL';
      }

      
       
        

       

      
    }
    


    $data['cities']      = Cities::pluck('title_'.App::getLocale(), 'id')->toArray();
    $data['cat_id']      =$cat_id;
    $data['sub_cat_id']  =$sub_cat_id1;
 
        //$data['cities']=Cities::all()->pluck($title,'id');
    $data['categories']  = App\ItemCategory::pluck('title_'.App::getLocale(), 'id')->all(); // our default
    $data['sub_item_categories']=App\ItemSubCategory::where('item_category_id',$cat_id)->pluck('title_'.App::getLocale(), 'id')->all();

      $data['item_list']=App\Item::with('item_category')
                        ->where('item_category_id',$cat_id)
                         ->where('active',1)
                      /*  ->where('item_sub_category_id',$sub_cat_id)*/
                        ->with('item_sub_category')
                        ->with('item_images')
                        ->with('city')
                        ->whereRaw("`items`.`city_id` =$city_id")
                        ->whereRaw("`items`.`payment_period_type` =$payment_period_type")
                        ->whereRaw("`items`.`price`<=$max_price")                                    
                        ->whereRaw("`items`.`price`>=$min_price")                            
                        ->whereRaw("`items`.`rent_type`=$rent_type")
                        ->whereRaw("`items`.`item_sub_category_id` $sub_cat_id1")
                        ->orderBy('vip','DESC')
                        ->orderBy('id','DESC')      
                        ->paginate(6);





        return view('guest.item_list',$data);

    }


    public function related_sub_item_category(Request $req)
    {

      $sub_item_categories=App\ItemSubCategory::where('item_category_id',$req->main_item_cat_id)->pluck('title_'.App::getLocale(), 'id')->all();
     


      $result=(count($sub_item_categories)>0)?'<div class="col-md-3 col-sm-6 col-xs-6">
        <div class="form-gp">
          <label>'.trans('common.item_sub_category').'</label>
           '.Form::select('sub_cat_id', ['all' => trans('common.view_all')]+$sub_item_categories,null,array('class'=>'selectpicker')).'
        </div>
      </div>
<script src="'.asset('assets/js/js.js').'"></script> 
      ':'';


        return $result;
 

    }


     public function related_sub_item_category_homepage(Request $req)
    {

      $sub_item_categories=App\ItemSubCategory::where('item_category_id',$req->main_item_cat_id)->pluck('title_'.App::getLocale(), 'id')->all();
     


      $result=(count($sub_item_categories)>0)?' <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>'.trans('common.sub_category').'</label>'.

                                                    Form::select('sub_cat_id', ['all' => trans('common.view_all')]+$sub_item_categories,null,array('class'=>'selectpicker'))
                                    
                                  .'</div><div class="clearfix"></div>
<script src="'.asset('assets/js/js.js').'"></script> 
      ':'';


        return $result;
 

    }

    public function item($id)
    {
            $data['item_result']=App\Item::with('item_category')->with('user')->with('item_images')->where('id', $id)->where('active',1)->get();


  //dd($data['real_estate_result']->toArray());
      return view('guest.item',$data);
    }






        public function service_list($cat_id,$sub_cat_id=null)
    {
 
//   $request=new Request();

      $cat_id=(Input::get('cat_id')!='')?Input::get('cat_id'):$cat_id;
    // $sub_cat_id=(Input::get('sub_cat_id')!='')?Input::get('sub_cat_id'):$sub_cat_id;

/*if($sub_cat_id!='')
    {
      $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'=`services`.`service_sub_category_id`';
    }
    else{

      if(Input::get('service_search')!=NULL)//if button clicked
      {
        $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'=`services`.`service_sub_category_id`';
      }else{
        $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'IS NULL';

      }

      
    }*/



      if($sub_cat_id!='')
    {
      $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'=`services`.`service_sub_category_id`';//from menu
    }
    else{

      if(Input::get('sub_cat_id')=='all')
      {
        $sub_cat_id1='=`services`.`service_sub_category_id`';
      }
      else{
        $sub_cat_id1=(Input::get('sub_cat_id')!='')?'='.Input::get('sub_cat_id'):'IS NULL';
      }


    }










//      $sub_cat_id=isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:$sub_cat_id;

    
    $city_id=(Input::get('city_id')!='')?Input::get('city_id'):'`services`.`city_id`'; 
   
    $min_price=(Input::get('min_price')!='')?Input::get('min_price'):'`services`.`price`';
    $max_price=(Input::get('max_price')!='')?Input::get('max_price'):'`services`.`price`';   
    $service_ad_type =(Input::get('service_ad_type')!='')?Input::get('service_ad_type'):'`services`.`service_ad_type`';   
   


    $data['cities']      = Cities::pluck('title_'.App::getLocale(), 'id')->toArray();
    $data['cat_id']      =$cat_id;
    $data['sub_cat_id']  =$sub_cat_id1;
 
        //$data['cities']=Cities::all()->pluck($title,'id');
    $data['categories']  = App\ServiceCategory::pluck('title_'.App::getLocale(), 'id')->all(); // our default
    $data['sub_item_categories']=App\ServiceSubCategory::where('service_category_id',$cat_id)->pluck('title_'.App::getLocale(), 'id')->all();

      $data['service_list']=App\Service::with('service_category')
                        ->where('service_category_id',$cat_id)
                         ->where('active',1)
                        /*->where('service_sub_category_id',$sub_cat_id)*/
                        ->with('service_sub_category')
                        ->with('service_images')
                        ->with('city')
                        ->whereRaw("`services`.`city_id` =$city_id")
                        ->whereRaw("`services`.`service_sub_category_id` $sub_cat_id1")
                        ->whereRaw("`services`.`price`<=$max_price")                                    
                        ->whereRaw("`services`.`price`>=$min_price")                            
                        ->whereRaw("`services`.`service_ad_type`=$service_ad_type") 
                        ->orderBy('vip','DESC')
                        ->orderBy('id','DESC')        
                        ->paginate(6);





        return view('guest.service_list',$data);

    }



        public function related_sub_service_category(Request $req)
    {

      $sub_service_categories=App\ServiceSubCategory::where('service_category_id',$req->main_service_cat_id)->pluck('title_'.App::getLocale(), 'id')->all();
     


      $result=(count($sub_service_categories)>0)?'<div class="col-md-3 col-sm-6 col-xs-6">
        <div class="form-gp">
          <label>'.trans('common.service_sub_category').'</label>
           '.Form::select('sub_cat_id', ['all' => trans('common.view_all')]+ $sub_service_categories,null,array('class'=>'selectpicker')).'
        </div>
      </div>
<script src="'.asset('assets/js/js.js').'"></script> 
      ':'';


        return $result;
 

    }

 public function related_sub_service_category_homepage(Request $req)
    {

     $sub_service_categories=App\ServiceSubCategory::where('service_category_id',$req->main_service_cat_id)->pluck('title_'.App::getLocale(), 'id')->all();
     


      $result=(count($sub_service_categories)>0)?' <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>'.trans('common.sub_category').'</label>'.

                                                    Form::select('sub_cat_id', ['all' => trans('common.view_all')]+$sub_service_categories,null,array('class'=>'selectpicker'))
                                    
                                  .'</div><div class="clearfix"></div>
<script src="'.asset('assets/js/js.js').'"></script> 
      ':'';


        return $result;
 

    }
        public function service($id)
    {
            $data['service_result']=App\Service::with('service_category')->with('user')->with('service_images')->where('id', $id)->where('active',1)->get();


  //dd($data['real_estate_result']->toArray());
      return view('guest.service',$data);
    }


    public function contact()
    {
        return view('guest.contact');
    }

    public function about()
    {
       return view('guest.about'); 
    }

    public function related_sub_real_estate_search_form_homepage(Request $req){

        if($req->real_estate_sub_menu_id=='1' || $req->real_estate_sub_menu_id=='2' || $req->real_estate_sub_menu_id=="3")
        {
          return view('guest.real_estate_homepage_related');
        }
    }


    public function send_message(ContactValidation $request)
    {
        $data =  [
            'fullname'=>$request->fullname,
            'message'=>$request->message,

        ];


  Mail::send('emails.contact', ['data' => $data], function ($m) use ($request) {
                            $m->from('info@tapkira.az', $request->fullname);
                            $m->replyTo($request->email, $request->fullname);
                            $m->to('info@tapkira.az', 'From Tapkira')->subject('Tapkira.az - '. $request->subject);
     						
                        });

  return Redirect::back()->with('message',trans('common.send_success_message'));
    }


    public function add_new()
    {
          $title               = 'title_' . App::getLocale();
    $data['cities']      = Cities::all()->pluck($title, 'id');
        //$data['cities']=Cities::all()->pluck($title,'id');
        $data['real_estate'] = RealEstateCategory::all()->pluck($title, 'id'); // our default 
        $data['item']        = ItemCategory::all()->pluck($title, 'id');
        $data['service']     = ServiceCategory::all()->pluck($title, 'id');
      return view('guest.add_new',$data);
    }



          public function rel_sub(Request $request)
      {

        $select_option_value = $request->rel_sub;
        $title               = 'title_' . App::getLocale();
        
        
        if ($select_option_value == '1') {
          $data['real_estate'] = RealEstateCategory::all()->pluck($title, 'id');
          $data['cities']      = Cities::all()->pluck($title, 'id');
            //     $data['url']='save_real_estate';
          return view('guest.real_estates', $data);
        } elseif ($select_option_value == '2') {

          $data['item']   = ItemCategory::all()->pluck($title, 'id');
          $data['cities'] = Cities::all()->pluck($title, 'id');
            //   $data['item_sub_category']=ItemSubCategory::all()->pluck($title,'id');
            // $data['url']='save_item';
          return view('guest.items', $data);
        }
        
        else {
          $data['service'] = ServiceCategory::all()->pluck($title, 'id');
          $data['cities'] = Cities::all()->pluck($title, 'id');
            //   $data['url']='save_services';
          return view('guest.services', $data);
        }
        
        
        //    return $request->rel_sub;
        
        //return '<br><label>Alt kateqoriya</label>'.Form::select('sub_menu_id',$item->toArray(),null,array('class'=>'form-control','onchange'=>'related_sub_sub_category()'));
      }

         public function rel_sub_real_estate(Request $request)
          {
            $real_estate_sub_menu_id= $request->real_estate_sub_menu_id;

            if($real_estate_sub_menu_id=='1' || $real_estate_sub_menu_id=='2' || $real_estate_sub_menu_id=="3")
            {
             return view('guest.real_estate_apartment');
           }
           else
           {
             return view('guest.real_estate_soil');
           }


         }


           public function rel_item_sub_category(Request $request)
      {
        $title                     = 'title_' . App::getLocale();
        $related_item_sub_category = ItemSubCategory::where('item_category_id', $request->rel_item_sub_category)->pluck($title, 'id');
        
        $result = ($related_item_sub_category->count() > 0) ? '<div class="form-group"><label>'.trans('common.item_sub_category').'</label>' . Form::select('sub_menu_id', $related_item_sub_category, null, array(
          'class' => 'form-control'
          )) .'</div>': '';
        
        
        return $result;
      }


      public function rel_service_sub_category(Request $request)
      {
        $title                        = 'title_' . App::getLocale();
        $related_service_sub_category = ServiceSubCategory::where('service_category_id', $request->rel_service_sub_category)->pluck($title, 'id');
        
        $result = ($related_service_sub_category->count() > 0) ? '<div class="form-group"><label>Xidmətin  növü</label>' . Form::select('service_sub_category_id', $related_service_sub_category, null, array(
          'class' => 'form-control'
          )).'</div>': '';
        
        
        return $result;
      }
}
