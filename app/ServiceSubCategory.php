<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceSubCategory extends Model
{
   public function service_category()
   {
   	   return $this->belongsTo('App\ServiceCategory');
   }

   public function service()
   {
   	   return $this->belongsTo('App\Service');
   }
}
