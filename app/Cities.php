<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
     public function real_estates()
    {
        return $this->hasMany('App\RealEstate');
    }

     public function item()
    {
        return $this->hasMany('App\Item');
    }

    public function service()
    {
        return $this->hasMany('App\Service');
    }
}
