<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule,File;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
            protected function schedule(Schedule $schedule)
            {

                 $schedule->call(function () {//actually it does not remove images from database. It just checks if filenmame exist in database then does nothing. Otherwise it removes files from folder. (For temporary image files)

                   $this->check_and_remove_images_from_database('item_images');
                   $this->check_and_remove_images_from_database('real_estate_images');
                   $this->check_and_remove_images_from_database('service_images');
                })->everyMinute();

                $schedule->call(function () {

                    $this->archive_expired_items();
                    $this->archive_expired_services();
                    $this->archive_expired_real_estates();

                })->everyMinute();


             }

            
                 public function convert_files_to_array($file_list)
                 {

                  $current_date_time=date("Y-m-d H:i:s");

              //    die($current_date_time);
                    $result_array = [];
                    foreach($file_list as $path)
                    {

                        $datetime1 = strtotime(date ("Y-m-d H:i:s", filemtime($path)));
                        $datetime2 = strtotime($current_date_time);
                        $interval  = abs($datetime2 - $datetime1);
                        $minutes   = round($interval / 60);


                        $result_array[] = array('filename'=>pathinfo($path)['basename'],'filecreatedtime'=>$minutes);
                    }

                    return $result_array;
                }


            public function check_and_remove_images_from_database($foldername)
            {

                           $image_path='public_html/uploads/'.$foldername;
                           $files = File::allFiles($image_path);

                        //   dd($files);
                           $result_array=$this->convert_files_to_array($files);
                           //dd($result_array);

                           foreach ($result_array as $key=>$value) {


                            if($foldername=='item_images')
                            {
                                $database_filename=\App\ItemImage::where('image',$value['filename'])->first();
                            }
                            elseif($foldername=='real_estate_images'){
                                $database_filename=\App\RealEstateImage::where('image',$value['filename'])->first();
                            }
                            else
                            {
                                $database_filename=\App\ServiceImage::where('image',$value['filename'])->first();
                            }
 

                            if(count( $database_filename)==0 && $value['filecreatedtime']>20){ //if file does not exist in database and filecreation time length is more 20 minutes

                              File::delete($image_path.'/'.$value['filename']);  
                            //    echo $image_path.'/'.$value['filename']." (".$value['filecreatedtime'].")".' <br>';
                          }

                        }
            }


            public function archive_expired_items() //and send mail notification to users
            {
                $expired_posts=\App\Item::with('user')
                                ->whereRaw('activated_at <= (now() - interval 3 month)')
                                ->whereRaw('notified = "no"');

               
                print_r($expired_posts->get()->toArray());
                
                $data_expired_posts=$expired_posts->get();


                foreach ($data_expired_posts as  $data_expired_post) {
                       Mail::send('emails.expired_notification', ['data' => $data_expired_post,'title'=>$data_expired_post->title,'category'=>'Əşyalar'], function ($m) use ($data_expired_post) {
                            $m->from('info@tapkira.az', 'Tapkira.az');

                            $m->to($data_expired_post->user->email, 'Emil Azizov')->subject('Elanınızın vaxtı bitmişdir');
                        });
                }

             




                 $expired_posts->update(['expired' => 'yes','active'=>'2','notified'=>'yes']);
                
            }



              public function archive_expired_services() //and send mail notification to users
            {
                $expired_posts=\App\Service::with('user')
                                ->whereRaw('activated_at <= (now() - interval 3 month)')
                                ->whereRaw('notified = "no"');

               
                print_r($expired_posts->get()->toArray());
                
                $data_expired_posts=$expired_posts->get();


                foreach ($data_expired_posts as  $data_expired_post) {
                       Mail::send('emails.expired_notification', ['data' => $data_expired_post,'title'=>$data_expired_post->title,'category'=>'Xidmətlər'], function ($m) use ($data_expired_post) {
                            $m->from('info@tapkira.az', 'Tapkira.az');

                            $m->to('azizov.emil@gmail.com', 'Emil Azizov')->subject('Elanınızın vaxtı bitmişdir');
                        });
                }

             




                $expired_posts->update(['expired' => 'yes','active'=>'2','notified'=>'yes']);
                
            }


              public function archive_expired_real_estates() //and send mail notification to users
            {
                $expired_posts=\App\RealEstate::with('user')
                                ->with('real_estate_category')
                                ->whereRaw('activated_at <= (now() - interval 3 month)')
                                ->whereRaw('notified = "no"');

               
                print_r($expired_posts->get()->toArray());
                
                $data_expired_posts=$expired_posts->get();


                foreach ($data_expired_posts as  $data_expired_post) {

                    $title=$data_expired_post->city->title_az.' şəhərində '.$data_expired_post->real_estate_category->title_az;

                       Mail::send('emails.expired_notification', ['data' => $data_expired_post,'title'=>$title,'category'=>'Daşınmaz əmlak'], function ($m) use ($data_expired_post) {
                            $m->from('info@tapkira.az', 'Tapkira.az');

                            $m->to('azizov.emil@gmail.com', 'Emil Azizov')->subject('Elanınızın vaxtı bitmişdir');
                        });
                }

             




                $expired_posts->update(['expired' => 'yes','active'=>'2','notified'=>'yes']);
                
            }
}
