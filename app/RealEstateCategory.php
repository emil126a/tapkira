<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealEstateCategory extends Model
{
   public function item()
   {
   	   return $this->belongsTo('App\RealEstate');
   }
}
