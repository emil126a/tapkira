<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSubCategory extends Model
{
  public function item_category()
   {
   	   return $this->belongsTo('App\ItemCategory');
   }

   public function item()
   {
   	   return $this->belongsTo('App\Item');
   }
}
