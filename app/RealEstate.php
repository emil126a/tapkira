<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealEstate extends Model
{
  public function real_estate_images()
    {
    	return $this->hasMany('App\RealEstateImage');
    }

public function real_estate_category()
    {
    	return $this->hasOne('App\RealEstateCategory','id','real_estate_category_id');
    }


public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo('App\Cities');
    }

    


}
