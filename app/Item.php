<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
   //  public $timestamps = false;
  

    public function item_images()
    {
    	return $this->hasMany('App\ItemImage');
    }

    public function item_category()
    {
    	return $this->hasOne('App\ItemCategory','id','item_category_id');
    }


      public function item_sub_category()
    {
    	return $this->hasOne('App\ItemSubCategory','id','item_sub_category_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo('App\Cities');
    }
}
