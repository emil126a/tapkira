<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;

class User extends Authenticatable
{

     //use Notifiable;
    use SyncableGraphNodeTrait;


protected static $graph_node_field_aliases = [
        'id' => 'facebook_user_id',
        'name' => 'name',
        'picture.url'=>'profile_picture',
        'picture.is_silhouette'=>'is_default_facebook_avatar',
       // 'asdada'=>'password',
        'graph_node_field_name' => 'database_column_name',
    ];

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

 public function real_estates()
    {
        return $this->hasMany('App\RealEstate');
    }

     public function items()
    {
        return $this->hasMany('App\Item');
    }
}
