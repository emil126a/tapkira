<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ItemCategory,App\Cities,App\RealEstate,App\RealEstateCategory,App\ServiceCategory;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    { 

  //   dd(ServiceCategory::with('sub_category')->get()->toArray());
         $data['item_cat_subcat']=ItemCategory::with(array('sub_category' => function($query) {
        $query->orderBy('id', 'DESC');
    }))->get();

       //  $data['item_cat_subcat']=ItemCategory::get();


/*
         $data['cities_col1']=Cities::take(25)->get();
         $data['cities_col2']=Cities::take(25)->offset(25)->get();*/
         $data['real_estate_category']=RealEstateCategory::get();
         $data['service_cat_subcat']=ServiceCategory::with('sub_category')->get();
 
 view()->share('data',  $data);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
