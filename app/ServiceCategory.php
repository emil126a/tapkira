<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
   public function service()
   {
   	   return $this->belongsTo('App\Service');
   }
    
    public function sub_category()
    {
    	return $this->hasMany('App\ServiceSubCategory');
    }
}
