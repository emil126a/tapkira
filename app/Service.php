<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function service_images()
    {
    	return $this->hasMany('App\ServiceImage');
    }

    public function service_category()
    {
    	return $this->hasOne('App\ServiceCategory','id','service_category_id');
    }

      public function service_sub_category()
    {
    	return $this->hasOne('App\ServiceSubCategory','id','service_sub_category_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Cities');
    }

      public function user()
    {
        return $this->belongsTo('App\User');
    }
}

 