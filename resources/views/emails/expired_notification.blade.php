Salam <b>{{$data->user->name}}</b>,<br>
Sizin <i>&quot;{{$title}}&quot;</i> başlıqlı elanınızın vaxtı bitmişdir. Elanınızın vaxtını artırmaq üçün tapkira.az rəhbərliyi ilə əlaqə saxlamağanız tövsiyyə olunur.
<br><br>
Əlaqə saxlayarkən aşağıdakı məlumatları təqdim etməyiniz vacibdir:

<br>
<b>Elan nömrəsi:</b> <u>{{$data->id}}</u><br>


 <b>Elan kateqoriyası:</b>  <u>{{$category}}</u><br><br>

Hörmətlə,<br>
Tapkira.az rəhbərliyi 

-------------------
Здравствуйте <b>{{$data->user->name}}</b>,<br>
Ваша объявление под названием <i>&quot;{{$title}}&quot;</i>  истек срок действии.  Чтобы продлить время объявления рекомендуется связаться с руководством сайта Tapkira.az . <br><br>
В этом случае необходимо предоставить следующие сведения:

<br>
<b>Номер объявления: </b> <u>{{$data->id}}</u><br>
 <b>Категория:</b>  <u>{{$category}}</u><br><br>
С уважением,<br>
Руководство Tapkira.az."
