@extends('admin.layouts.app')

@section('content')

<style type="text/css">
	
	.tdloadinfoboard
	{

		border-top: medium hidden ! important;
	}

	.cursor
	{
		cursor: pointer;
	}

	.cursor:hover {
    background-color: #babdc1;
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-md-11" style="margin: 0 auto; margin-left: 4%;">
      <div class="panel panel-default">
        <div class="panel-heading">Əşyalar</div>

        <div class="panel-body">
          <center>
            <h5><a href="{{url('admin/items/0')}}">Təsdiq gözləyən alanlar <span class="label" style="background-color: red;">{{($data['count_passive_items']>0)?$data['count_passive_items']:''}}</span></a> | <a href="{{url('admin/items/1')}}">Aktiv elanlar <span class="label" style="background-color: green;">{{$data['count_active_items']}}</span></a> | <a href="{{url('admin/items/2')}}">Arxiv edilmiş elanlar <span class="label" style="background-color: orange;">{{$data['count_archived_items']}}</span></a></h5>
          </center>
          <br><br>
                                {!! Form::open(array('url'=>App::getLocale().'/admin/items/1','method'=>'get')) !!}
          <div id="custom-search-input" >
            <div class="input-group col-md-5" style="float: right;">

              <input type="text" class="form-control input-lg" name="item_id" placeholder="Əşya elanının nömrəsi" />
              <span class="input-group-btn">
                <button class="btn btn-info btn-lg" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </span>

          
            </div>
          </div>
              {!!Form::close()!!}
          <div class="clearfix"></div>
          <br><br style="clear: both;">

          
          @if(Session::has('message'))


          <div class="alert alert-success">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <strong>Diqqet!</strong> {{Session::get('message')}}.

          </div>
          @endif
          
          
 
          {!! Form::open(array('url'=>App::getLocale().'/admin/item_checbox_action','method'=>'POST')) !!}
          <table class="table">
            <tr>
              <th><center><input type="checkbox" id="selectAll" /></center></th>
              <th>Şəkil</th>
              
              <th>Əşyanın növü</th>
     
              <th>Şəhər</th>
              <th>Elan növü</th>
              <th>Tarix</th>
              <th>Qiyməti</th>
              
              <th></th>
            </tr>

            

            
            @foreach($items as  $item)

             
            <tr id="slide-content-{{$item->id}}" class="cursor" onclick="LoadInfoBoard({{$item->id}}, 'vacancy');" >
              <td>        <input type="checkbox" name="checkbox[]" value="{{$item->id}}" id="1" /></td>
              <td>




                <a class="example-image-link" href="{{asset('uploads/item_images/'.$item->item_images[0]->image)}}" data-lightbox="{{$item->id}}" data-title="Click the right half of the image to move forward."><img class="example-image" src="{{asset('uploads/item_images/'.$item->item_images[0]->image)}}" alt="" width="100"/></a>



                  <?php
$b = false;
?>


                @foreach($item->item_images as $item_image)
@if(!$b)


                        <?php

                          $b = true;
                          continue;
                        ?>
                      @endif
                <a class="example-image-link" style='display:none;' href="{{asset('uploads/item_images/'.$item_image->image)}}" data-lightbox="{{$item->id}}" data-title="Or press the right arrow on your keyboard."></a>
                @endforeach
                
              </td>
              
              <td>{{$item->item_category->title_az}} @if($item->item_sub_category!=null)
							 / {{$item->item_sub_category->title_az}}
							@endif</td>
                             <td>{{$item->city->title_az}}</td>
              <td>{{trans("common.item_rent_type")[$item->rent_type]}}</td>
              <td>{{$item->created_at->diffForHumans()}}</td>
              <td>{{$item->price}} AZN/{{trans("common.payment_period_type")[$item->payment_period_type]}}</td>
              <td align="right"><!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
               

                <a href=""  title="Sil" style="float:right" class="delete_press btn btn-danger" data-href="{{url('admin/delete_item/'.$item->id)}}" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></a>



                





                <a href="{{url('admin/edit_item/'.$item->id)}}" style="float:right; margin-right: 5px;" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{url('admin/vip_item/'.$item->id)}}" style="float:right; margin-right: 5px;" class="btn {{($item->vip==1)?'btn-default':'btn-success'}}"><i class="fa fa-diamond"></i></a>
				 

                @if($item->active==2)
<a href="{{url('admin/archive_item/'.$item->id)}}" style="float:right;margin-right: 5px;background-color: red;" class="btn btn-danger"><i class="fa fa-check"></i></a>

@else

<a href="{{url('admin/archive_item/'.$item->id)}}" style="float:right;margin-right: 5px;" class="btn {{($item->active==2)?'btn-default':'btn-warning'}}"><i class="fa fa-download"></i></a>
                @endif
 

         
                <!-- Secondary, outline button -->








              </td>
            </tr>
            <tr class="tdloadinfoboard" style="display: none;" id="vacancy_{{$item->id}}">
              <td colspan="7" style="width: 50%;text-align: justify;">{!! nl2br(e($item->description)) !!}</td>
              <td colspan="4">      
                <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;{{$item->title}}  <br><br>
                <i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;{{$item->user->name}}<br><br>
                <i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;{{$item->user->phone}}<br><br>
                <i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;  {{trans("common.item_condition")[$item->is_new]}}
                <br><br>
               
                <br><br>

               
                <br><br>

                 
              </td>
            </tr>
            @endforeach


            
            
            
          </table>



          
          
          
          

          <hr />


          

          <center>        
           @if(Request::segment(4)==1)
           
           
           <button type="submit" class="btn btn-warning" id="archive" name="archive"> <i class="glyphicon glyphicon-ok"></i> &nbsp;Seçilmişləri Arxivləşdir</button>
           

           @else
           <button type="submit" class="btn btn-success" id="activate" name="activate"> <i class="glyphicon glyphicon-ok"></i> &nbsp;Seçilmişləri Aktivləşdir</button>


           @endif

 

           <button type="submit" class="delete_selected btn btn-danger" name="delete"><i class="glyphicon glyphicon-remove"></i> Seçilmişləri sil</button>
         </center>



         {!! Form::close() !!}
         



<br><br>

<center>{{ $items->links() }}</center>
       </div>
     </div>
   </div>
 </div>
</div>



















@endsection
