@extends('admin.layouts.app')

@section('content')

<style type="text/css">
	
	.tdloadinfoboard
	{

		border-top: medium hidden ! important;
	}

	.cursor
	{
		cursor: pointer;
	}

	.cursor:hover {
    background-color: #babdc1;
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-md-11" style="margin: 0 auto; margin-left: 4%;">
      <div class="panel panel-default">
        <div class="panel-heading">İstifadəçilər</div>

        <div class="panel-body">
        	
<table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Tam adı</th>
                <th>Email</th>
                <th>Əlaqə Telefonu</th>
                <th>Qeydiyyatdan keçmə tarixi</th>
                <th>Son giriş tarixi</th>
                <th></th>
                 
            </tr>
        </thead>
         
        <tbody>


        @foreach($users as $user)


 <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->phone}}</td>
                <td data-order="{{Carbon\Carbon::parse($user->created_at)->format('Ymd His') }}">{{Carbon\Carbon::parse($user->created_at)->diffForHumans() }}</td>
                <td data-order="{{Carbon\Carbon::parse($user->last_login)->format('Ymd His') }}">{{Carbon\Carbon::parse($user->last_login)->diffForHumans() }}  </td>
               <td><a href="" title="Sil" style="float:right" class="delete_press btn btn-danger" data-href="{{url('admin/delete_user/'.$user->id)}}" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i> Sil</a>

                 <a href="{{url('admin/edit_user/'.$user->id)}}" style="float:right; margin-right: 5px;" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Düzəliş et</a></td>
            </tr>

        @endforeach
           
             
         
        </tbody>
    </table>
		</div>
     </div>
   </div>
 </div>
</div>










@section('bottom')


<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/dataTables.bootstrap.css')}}">

<script type="text/javascript" src="{{asset('assets/admin/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/dataTables.bootstrap.js')}}"></script>

@endsection






<script type="text/javascript">

$(document).ready(function() {
    $('#example').DataTable();
} );

</script>





@endsection
