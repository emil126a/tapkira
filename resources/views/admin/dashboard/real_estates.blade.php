@extends('admin.layouts.app')

@section('content')

<style type="text/css">
	
	.tdloadinfoboard
	{

		border-top: medium hidden ! important;
	}

	.cursor
	{
		cursor: pointer;
	}

	.cursor:hover {
    background-color: #babdc1;
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-md-11" style="margin: 0 auto; margin-left: 4%;">
      <div class="panel panel-default">
        <div class="panel-heading">Daşınmaz əmlak</div>

        <div class="panel-body">
          <center>
            <h5><a href="{{url('admin/real_estates/0')}}">Təsdiq gözləyən alanlar  <span class="label" style="background-color: red;">{{($data['count_passive_real_estates']>0)?$data['count_passive_real_estates']:''}}</span></a> | <a href="{{url('admin/real_estates/1')}}">Aktiv elanlar <span class="label" style="background-color: green;">{{$data['count_active_real_estates']}}</span></a> | <a href="{{url('admin/real_estates/2')}}">Arxiv edilmiş elanlar <span class="label" style="background-color: orange;">{{$data['count_archived_real_estates']}}</span></a></h5>
          </center>
          <br><br>
                                {!! Form::open(array('url'=>App::getLocale().'/admin/real_estates/1','method'=>'get')) !!}
          <div id="custom-search-input" >
            <div class="input-group col-md-5" style="float: right;">

              <input type="text" class="form-control input-lg" name="real_estate_id" placeholder="Əmlak elanının nömrəsi" />
              <span class="input-group-btn">
                <button class="btn btn-info btn-lg" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </span>

          
            </div>
          </div>
              {!!Form::close()!!}
          <div class="clearfix"></div>
          <br><br style="clear: both;">

          
          @if(Session::has('message'))


          <div class="alert alert-success">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <strong>Diqqet!</strong> {{Session::get('message')}}.

          </div>
          @endif
          
          

          {!! Form::open(array('url'=>App::getLocale().'/admin/real_estate_checbox_action','method'=>'POST')) !!}
          <table class="table">
            <tr>
              <th><center><input type="checkbox" id="selectAll" /></center></th>
              <th>Şəkil</th>
              
              <th>Əmlakın növü</th>
              <th>Sahəsi</th>
              <th>Şəhər</th>
              <th>Elan növü</th>
              <th>Tarix</th>
              <th>Qiyməti</th>
              
              <th></th>
            </tr>

            

            
            @foreach($real_estates as  $real_estate)

            <?php

            $area_measurement=($real_estate->real_estate_category->id==8)?'SOT':'m²';
            
            ?>
            <tr id="slide-content-{{$real_estate->id}}" class="cursor" onclick="LoadInfoBoard({{$real_estate->id}}, 'vacancy');" >
              <td>        <input type="checkbox" name="checkbox[]" value="{{$real_estate->id}}" id="1" /></td>
              <td>




                <a class="example-image-link" href="{{asset('uploads/real_estate_images/'.$real_estate->real_estate_images[0]->image)}}" data-lightbox="{{$real_estate->id}}" data-title="Click the right half of the image to move forward."><img class="example-image" src="{{asset('uploads/real_estate_images/'.$real_estate->real_estate_images[0]->image)}}" alt="" width="100"/></a>
<?php
$b = false;
?>
                @foreach($real_estate->real_estate_images as $real_estate_image)


                      @if(!$b)


                        <?php

                          $b = true;
                          continue;
                        ?>
                      @endif
                <a class="example-image-link" style='display:none;' href="{{asset('uploads/real_estate_images/'.$real_estate_image->image)}}" data-lightbox="{{$real_estate->id}}" data-title="Or press the right arrow on your keyboard."></a>
                @endforeach
                
              </td>
              
              <td>{{$real_estate->real_estate_category->title_az}}</td>
              <td>{{$real_estate->area}} {{$area_measurement}}</td>
              <td>{{$real_estate->city->title_az}}</td>
              <td>{{trans("common.rent_type")[$real_estate->rent_type]}}</td>
              <td>{{$real_estate->created_at->diffForHumans()}}</td>
              <td>{{$real_estate->price}} AZN/{{trans("common.payment_period_type_real_estate")[$real_estate->payment_period_type]}}</td>
              <td align="right"><!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
               

                <a href=""  title="Sil" style="float:right" class="delete_press btn btn-danger" data-href="{{url('admin/delete_real_estate/'.$real_estate->id)}}" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></a>


                <a href="{{url('admin/edit_real_estate/'.$real_estate->id)}}" style="float:right; margin-right: 5px;" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{url('admin/vip/'.$real_estate->id)}}" style="float:right; margin-right: 5px;" class="btn {{($real_estate->vip==1)?'btn-default':'btn-success'}}"><i class="fa fa-diamond"></i></a>

                @if($real_estate->active==2)
                <a href="{{url('admin/archive_real_estate/'.$real_estate->id)}}" style="float:right;margin-right: 5px;background-color: red;" class="btn btn-danger"><i class="fa fa-check"></i></a>

                @else

                <a href="{{url('admin/archive_real_estate/'.$real_estate->id)}}" style="float:right;margin-right: 5px;" class="btn {{($real_estate->active==2)?'btn-default':'btn-warning'}}"><i class="fa fa-download"></i></a>
                @endif

                








              </td>
            </tr>
            <tr class="tdloadinfoboard" style="display: none;" id="vacancy_{{$real_estate->id}}">
              <td colspan="7" style="width: 50%;text-align: justify;">{!! nl2br(e($real_estate->description)) !!}</td>
              <td colspan="4">      
                <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;{{$real_estate->address}}  <br><br>
                <i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;{{$real_estate->user->name}}<br><br>
                <i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;{{$real_estate->user->phone}}
                <br><br>
                @if($real_estate->client_priority!="")
                <i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;{{trans("common.priority_person")[$real_estate->client_priority]}}
                @endif
                <br><br>

                @if($real_estate->real_estate_status!="")
                <i class="fa fa-building" aria-hidden="true"></i>&nbsp;&nbsp;{{trans("common.array_real")[$real_estate->real_estate_status]}}
                @endif
                <br><br>

                @if($real_estate->room_number!="")

                <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;{{$real_estate->room_number}} otaq
                @endif
              </td>
            </tr>
            @endforeach


            
            
            
          </table>



          
          
          
          

          <hr />


          

          <center>        
           @if(Request::segment(4)==1)
           
           
           <button type="submit" class="btn btn-warning" id="archive" name="archive"> <i class="glyphicon glyphicon-ok"></i> &nbsp;Seçilmişləri Arxivləşdir</button>
           

           @else
           <button type="submit" class="btn btn-success" id="activate" name="activate"> <i class="glyphicon glyphicon-ok"></i> &nbsp;Seçilmişləri Aktivləşdir</button>


           @endif

 

           <button type="submit" class="delete_selected btn btn-danger" name="delete"><i class="glyphicon glyphicon-remove"></i> Seçilmişləri sil</button>
         </center>



         {!! Form::close() !!}
         



<br><br>

<center>{{ $real_estates->links() }}</center>
       </div>
     </div>
   </div>
 </div>
</div>



















@endsection











