@extends('admin.layouts.app')

@section('content')

<style type="text/css">
	
	.tdloadinfoboard
	{

		border-top: medium hidden ! important;
	}

	.cursor
	{
		cursor: pointer;
	}

	.cursor:hover {
    background-color: #babdc1;
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-md-11" style="margin: 0 auto; margin-left: 4%;">
      <div class="panel panel-default">
        <div class="panel-heading">Xidmətlər</div>

        <div class="panel-body">
          <center>
            <h5><a href="{{url('admin/services/0')}}">Təsdiq gözləyən alanlar <span class="label" style="background-color: red;">{{($data['count_passive_services']>0)?$data['count_passive_services']:''}}</span></a> | <a href="{{url('admin/services/1')}}">Aktiv elanlar <span class="label" style="background-color: green;">{{$data['count_active_services']}}</span></a> | <a href="{{url('admin/services/2')}}">Arxiv edilmiş elanlar <span class="label" style="background-color: orange;">{{$data['count_archived_services']}}</span></a></h5>
          </center>
          <br><br>
                                {!! Form::open(array('url'=>App::getLocale().'/admin/services/1','method'=>'get')) !!}
          <div id="custom-search-input" >
            <div class="input-group col-md-5" style="float: right;">

              <input type="text" class="form-control input-lg" name="service_id" placeholder="Xidmət elanının nömrəsi" />
              <span class="input-group-btn">
                <button class="btn btn-info btn-lg" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </span>

          
            </div>
          </div>
              {!!Form::close()!!}
          <div class="clearfix"></div>
          <br><br style="clear: both;">

          
          @if(Session::has('message'))


          <div class="alert alert-success">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <strong>Diqqet!</strong> {{Session::get('message')}}.

          </div>
          @endif
          
          
 
          {!! Form::open(array('url'=>App::getLocale().'/admin/service_checbox_action','method'=>'POST')) !!}
          <table class="table">
            <tr>
              <th><center><input type="checkbox" id="selectAll" /></center></th>
              <th>Şəkil</th>          
              <th>Xidmətin növü</th>
              <th>Şəhər</th>
              <th>Elan növü</th>
              <th>Tarix</th>
              <th>Qiyməti</th>
              
              <th></th>
            </tr>

            

            
            @foreach($services as  $service)

             
            <tr id="slide-content-{{$service->id}}" class="cursor" onclick="LoadInfoBoard({{$service->id}}, 'vacancy');" >
              <td>        <input type="checkbox" name="checkbox[]" value="{{$service->id}}" id="1" /></td>
              <td>




                <a class="example-image-link" href="{{asset('uploads/service_images/'.$service->service_images[0]->image)}}" data-lightbox="{{$service->id}}" data-title="Click the right half of the image to move forward."><img class="example-image" src="{{asset('uploads/service_images/'.$service->service_images[0]->image)}}" alt="" width="100"/></a>



                  <?php
$b = false;
?>
                @foreach($service->service_images as $service_image)
@if(!$b)


                        <?php

                          $b = true;
                          continue;
                        ?>
                      @endif
                <a class="example-image-link" style='display:none;' href="{{asset('uploads/service_images/'.$service_image->image)}}" data-lightbox="{{$service->id}}" data-title="Or press the right arrow on your keyboard."></a>
                @endforeach
                
              </td>
              
              <td>{{$service->service_category->title_az}} @if($service->service_sub_category!=null)
							 / {{$service->service_sub_category->title_az}}
							@endif</td>
    
              <td>{{$service->city->title_az}}</td>
              <td>{{trans("common.service_type")[$service->service_ad_type]}}</td>
              <td>{{$service->created_at->diffForHumans()}}</td>
              <td>{{$service->price}} AZN</td>
              <td align="right"><!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
               

                <a href=""  title="Sil" style="float:right" class="delete_press btn btn-danger" data-href="{{url('admin/delete_service/'.$service->id)}}" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></a>



                





                <a href="{{url('admin/edit_service/'.$service->id)}}" style="float:right; margin-right: 5px;" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{url('admin/vip_service/'.$service->id)}}" style="float:right; margin-right: 5px;" class="btn {{($service->vip==1)?'btn-default':'btn-success'}}"><i class="fa fa-diamond"></i></a>
				 

                @if($service->active==2)
<a href="{{url('admin/archive_service/'.$service->id)}}" style="float:right;margin-right: 5px;background-color: red;" class="btn btn-danger"><i class="fa fa-check"></i></a>

@else

<a href="{{url('admin/archive_service/'.$service->id)}}" style="float:right;margin-right: 5px;" class="btn {{($service->active==2)?'btn-default':'btn-warning'}}"><i class="fa fa-download"></i></a>
                @endif
 

         
                <!-- Secondary, outline button -->








              </td>
            </tr>
            <tr class="tdloadinfoboard" style="display: none;" id="vacancy_{{$service->id}}">
              <td colspan="7" style="width: 50%;text-align: justify;">{!! nl2br(e($service->description)) !!}</td>
              <td colspan="4">      
                <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;{{$service->title}}  <br><br>
                <i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;{{$service->user->name}}<br><br>
                <i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;{{$service->user->phone}}<br><br>
                
                <br><br>
               
                <br><br>

               
                <br><br>

                 
              </td>
            </tr>
            @endforeach


            
            
            
          </table>



          
          
          
          

          <hr />


          

          <center>        
           @if(Request::segment(4)==1)
           
           
           <button type="submit" class="btn btn-warning" id="archive" name="archive"> <i class="glyphicon glyphicon-ok"></i> &nbsp;Seçilmişləri Arxivləşdir</button>
           

           @else
           <button type="submit" class="btn btn-success" id="activate" name="activate"> <i class="glyphicon glyphicon-ok"></i> &nbsp;Seçilmişləri Aktivləşdir</button>


           @endif

 

           <button type="submit" class="delete_selected btn btn-danger" name="delete"><i class="glyphicon glyphicon-remove"></i> Seçilmişləri sil</button>
         </center>



         {!! Form::close() !!}
         



<br><br>

<center>{{ $services->links() }}</center>
       </div>
     </div>
   </div>
 </div>
</div>



















@endsection
