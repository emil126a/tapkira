 <div class="form-group">
 	<label for="email" class="col-md-4 control-label">Kimlər üçün?</label>

 	<div class="col-md-6">
 

 		{{Form::select('priority_person',trans('common.priority_person'),isset($client_priority)?$client_priority:old('priority_person'),array('class'=>'form-control'))}}

 	</div>
 </div>

 
 <div class="form-group">
 	<label for="email" class="col-md-4 control-label">Əmlakın vəziyyəti</label>

 	<div class="col-md-6">


 		{{Form::select('real_estate_status',trans('common.array_real'),isset($real_estate_status)?$real_estate_status:old('real_estate_status'),array('class'=>'form-control'))}}

 	</div>
 </div>

 



 <div class="form-group">
 	<label for="email" class="col-md-4 control-label">Otaq sayı</label>

 	<div class="col-md-6">


 		{{Form::select('number_of_rooms',trans('common.real_room_numbers'),isset($room_number)?$room_number:old('number_of_rooms'),array('class'=>'form-control'))}}

 	</div>
 </div>