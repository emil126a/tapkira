@extends('admin.layouts.app')
@section('head')

<?php

use App\serviceSubCategory;

$title='title_'.App::getLocale();
?> 


<style type="text/css">

	.thumbnail{

		height: 100px;
		margin: 10px; 
		float: left;
	}



	.thumbnail_existing
	{
		background-color: #fff;
		border: 1px solid #ddd;
		border-radius: 4px;
		display: block;
		line-height: 1.42857;
		margin-bottom: 20px;
		padding: 4px;
		transition: border 0.2s ease-in-out 0;
		float: left;
		height: 100px;
		margin: 10px;
	}




	#clear{
		display:none;
	}
	#result {
		border: 4px dotted #cccccc;
		display: none;
		float: right;
		margin:0 auto;
		width:100%;
	}

	.img-wrap {
		position: relative;
		display: inline-block;
		/* border: 1px red solid;*/
		font-size: 0;
	}
	.img-wrap .close {
		position: absolute;
		top: 15px;
		right: 15px;
		z-index: 100;
		/*  background-color: #FFF;*/
		padding: 5px 2px 2px;
		color: white;
		font-weight: bold;
		cursor: pointer;
		opacity: .2;
		text-align: center;
		font-size: 15px;
		/*   line-height: 10px;*/
		/* border-radius: 50%;*/
	}
	.img-wrap:hover .close {
		opacity: 1;
	}

</style>

<!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
@endsection


@section('content')


{{print_r($errors)}}
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
			<div class="panel-heading">Xidmətya düzəliş et</div>
				<div class="panel-body">
					{!! Form::open(array('url'=>App::getLocale().'/admin/store_service/'.$service_id,'method'=>'POST', 'files'=>true,'class'=>"form-horizontal",'role'=>"form",'id'=>'myform')) !!} 

<div class="form-group {{ $errors->has('service_name') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">Başlıq</label>

						<div class="col-md-6">


							<input name="service_name" value="{{(old('service_name'))?old('service_name'):$service_result->title}}" class="form-control" placeholder="Xidmətnın adını daxil edin">

							@if ($errors->has('service_name'))
							<span class="help-block">
								<strong>{{ $errors->first('service_name') }}</strong>
							</span>
							@endif
						</div>
					</div>


					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Xidmətnın növü</label>

						<div class="col-md-6">
							{{Form::select('service_category_id',$service_categories,(old("service_category_id"))?old("service_category_id"):$service_result->service_category_id,array('id'=>'service_category_id','class'=>'form-control','onchange'=>'related_sub_service()'))}}
						</div>
 				</div>
<div id="service_sub_category">
	


	@if(!is_null($service_result->service_sub_category_id) || (old('sub_menu_id')!='')  )




<div class="form-group">
            <label for="email" class="col-md-4 control-label">Alt kateqoriya</label>

            <div class="col-md-6">

             {{Form::select('sub_menu_id', serviceSubCategory::where('service_category_id', (old("service_category_id"))?old("service_category_id"):$service_result->service_category_id)->pluck($title,'id'),null,array('class'=>'form-control'))}} </div>
          </div>
@endif
</div>

					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Elanin növü</label>

						<div class="col-md-6">

							{{Form::select('service_ad_type',trans('common.service_type'),(old("rent_type"))?old("rent_type"):$service_result->rent_type,array('class'=>'form-control'))}}

						</div>
					</div>


					<div class="form-group {{ $errors->has('service_price') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">Qiyməti (AZN)</label>

						<div class="col-md-6">


							<input name="service_price" value="{{(old('service_price'))?old('service_price'):$service_result->price}}" class="form-control" placeholder="Xidmətın qiymətini daxil edin">

							@if ($errors->has('service_price'))
							<span class="help-block">
								<strong>{{ $errors->first('service_price') }}</strong>
							</span>
							@endif
						</div>
					</div>
 





					<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">Məzmun</label>

						<div class="col-md-6">
							<textarea rows="5" name="description" id="comment" style="height:185px" placeholder="Xidmətnızı ətraflı şəkildə qeyd edin" class="form-control ">{{(old('description'))?old('description'):$service_result->description}}</textarea>
							@if ($errors->has('description'))
							<span class="help-block">
								<strong>{{ $errors->first('description') }}</strong>
							</span>
							@endif

						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Şəhər</label>

						<div class="col-md-6">
							{{Form::select('service_city_id',$cities,(old('city'))?old('city'):$service_result->city_id,array('class'=>'form-control'))}}

						</div>
					</div>




					<div class="form-group {{ $errors->has('service_images.0') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Şəkil (lər):</label>

						<div class="col-md-6">
							<input id="images" multiple="multiple" name="service_images[]" type="file" accept="image/*">


							@if ($errors->has('service_images.0'))
							<span class="help-block">
								<strong>{{ $errors->first('service_images.0') }}</strong>
							</span>
							@endif
							<output id="result"><button id="clear" type="button" style="float:right; margin-top:-2px;background-color:red;">Sil</button><br style="clear:both;"></output>

						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 " style="float: none; margin: 0 auto auto 35%; ">


							<hr>

							

							@foreach($service_result->service_images as $image)

							<div class="img-wrap">
								<a href=""  title="Sil" class="delete_press close btn btn-danger btn-sm btn-grad" data-href="{{url('admin/delete_service_image/'.$image->id)}}" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-trash"></i></a> 
								<img class="thumbnail_existing" src="{{asset('uploads/service_images/'.$image->image)}}">
							</div>

							@endforeach


						</div>

					</div>







					<div class="form-group">



						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-sign-in"></i> Düzəliş et
							</button>

						</div>
					</div>


					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>










<script>

     function related_sub_service() {

        $.ajax({

           type: "GET",
           url: "{{url('admin/rel_service_sub_category')}}", 
           data: {rel_service_sub_category: $('#service_category_id').val()},       
           cache:false,
           success: 
           function(data){
                    						//alert(data);
                    						$("#service_sub_category").html(data);
                    					}
                    				});
        return false;
    }
</script>

  <script type="text/javascript">





//<![CDATA[

window.onload = function(){   
    //Check File API support


    if(window.File && window.FileList && window.FileReader)
    {
        $('#images').on("change", function(event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                //Only pics
                // if(!file.type.match('image'))
                if(file.type.match('image.*')){
                    if(this.files[0].size < 2097152){    
                  // continue;
                  var picReader = new FileReader();
                  picReader.addEventListener("load",function(event){
                    var picFile = event.target;
                    var div = document.createElement("div");
                    div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +
                    "title='preview image'/>";
                    output.insertBefore(div,null);            
                });
                    //Read the image
                    $('#clear, #result').show();
                    picReader.readAsDataURL(file);
                }else{
                    alert("Image Size is too big. Minimum size is 2MB.");
                    $(this).val("");
                }
            }else{
                alert("You can only upload image file.");
                $(this).val("");
            }
        }                               

    });
    }
    else
    {
        console.log("Your browser does not support File API");
    }
}

$('#files').on("click", function() {
    $('.thumbnail').parent().remove();
    $('result').hide();
    $(this).val("");
});

$('#clear').on("click", function() {
    $('.thumbnail').parent().remove();
    $('#result').hide();
    $('#files').val("");
    $(this).hide();
});

//]]> 




</script>


@endsection