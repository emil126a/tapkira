<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tapkira.az - Admin İdarəetmə Paneli</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    


  <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/responsive.bootstrap.min.css')}}"> 
  <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/dataTables.bootstrap.min.css')}}">  -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.css"> 
    <style>
        body {
            font-family: 'Tahoma';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <style type="text/css">

    .thumbnail{

        height: 100px;
        margin: 10px; 
        float: left;
    }



    .thumbnail_existing
    {
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        display: block;
        line-height: 1.42857;
        margin-bottom: 20px;
        padding: 4px;
        transition: border 0.2s ease-in-out 0;
        float: left;
        height: 100px;
        margin: 10px;
    }




    #clear{
       display:none;
   }
   #result {
    border: 4px dotted #cccccc;
    display: none;
    float: right;
    margin:0 auto;
    width:100%;
}

.img-wrap {
    position: relative;
    display: inline-block;
   /* border: 1px red solid;*/
    font-size: 0;
}
.img-wrap .close {
    position: absolute;
    top: 15px;
    right: 15px;
    z-index: 100;
  /*  background-color: #FFF;*/
    padding: 5px 2px 2px;
    color: white;
    font-weight: bold;
    cursor: pointer;
    opacity: .2;
    text-align: center;
    font-size: 15px;
 /*   line-height: 10px;*/
   /* border-radius: 50%;*/
}
.img-wrap:hover .close {
    opacity: 1;
}

</style>

<!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<!-- Location picker -->
<script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCk4iM6XrgBpfDCiHX5u1I2vX9xH6T1k8o&libraries=places&language={{App::getLocale()}}'></script>
<script src="{{asset('assets/js/locationpicker.jquery.js')}}"></script>
     @yield('head')
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/admin') }}">
                    Admin İdarəetmə Paneli
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
@if (Auth::guard('admin')->user())
             
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('admin/account') }}">Şifrəmi yenilə</a> </li>
                    <li><a href="{{ url('admin/real_estates/0') }}">Daşınmaz əmlak <span class="label" style="background-color: red;">{{($data['count_passive_real_estates']>0)?$data['count_passive_real_estates']:''}}</span></a></li>
                    <li><a href="{{ url('admin/items/0') }}">Əşyalar <span class="label" style="background-color: red;">{{($data['count_passive_items']>0)?$data['count_passive_items']:''}}</span></a></li>
                    <li><a href="{{ url('admin/services/0') }}">Xidmətlər <span class="label" style="background-color: red;">{{($data['count_passive_services']>0)?$data['count_passive_services']:''}}</span></a></li>
                    <li><a href="{{ url('admin/users') }}">İstifadəçilər</a></li>
                    <li style="float:right;"><a href="{{ url('/') }}" target="_blank"><span class="label" style="background-color: green;">Əsas səhifə</span></a></li>
                </ul>
@endif
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (!Auth::guard('admin')->user())
                        <li><a href="{{ url('admin/login') }}">Login</a></li>
                     <!--    <li><a href="{{ url('admin/register') }}">Register</a></li> -->
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{Auth::guard('admin')->user()->name}}  <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('admin/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


    <script src="{{asset('assets/admin/js/jquery-1.12.4.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox-plus-jquery.min.js"></script>


   <!--  <script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/dataTables.responsive.min.js')}} "></script> -->
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js'></script>
<!-- <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script> -->
    

    <script type="text/javascript">

$(document).ready(function() {

    
$(".fancybox").fancybox({
    // API options 
    autoScale: false,
    type: 'iframe',
    padding: 0,
    closeClick: false
});
} );


</script>

<script type="text/javascript">



function LoadInfoBoard (id, type)
{
    var show;
    if(navigator.appName == 'Microsoft Internet Explorer') show = 'block';
    else show = 'table-row';
    
    tr_id = type + '_' + id;
   // img_id = 'img' + type + '_' + id;
    if (document.getElementById(tr_id).style.display == 'none')
    {
        document.getElementById(tr_id).style.display = show;
       // document.getElementById(img_id).src = 'http://cv-az.com/theme/img/yxri.gif';
    }
    else
    {
        document.getElementById(tr_id).style.display = 'none';
       // document.getElementById(img_id).src = 'http://cv-az.com/theme/img/ashgi.gif';
    }
}


$('document').ready(function(){

$('#selectAll').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});


 $('.delete_selected').click(function()//If delete button is pressed
       {
           if (confirm('Seçilmiş elanları silmək istədinizə əminsinizmi?')) {
                return true;
           }
           else
           {
               return false;
           }
       });
   // alert('a');
})
</script>

 <script>

   $(document).on("click", ".delete_press", function () {
     var url = $(this).data('href');

      $('.btn-ok').attr('href',url);
    //alert(myBookId);
});
    </script>


 <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Diqqətlə oxu</h4>
                </div>
            
                <div class="modal-body">
                    <p>Siz bu məlumatı tamamilə silmək üzrəsiniz. Silinmiş məlumatlar geri qaytarıla bilməz.</p>
                    <p>Silmək istədiyinizə əminsinizmi?</p>
                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                    <a class="btn btn-danger btn-ok">Bəli</a>
                </div>
            </div>
        </div>
    </div>
   @yield('bottom')

</body>
</html>

