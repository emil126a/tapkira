@extends('layouts.app')


@section('head')

<style type="text/css">
  
  .ajax-upload-dragdrop
  {
    width: 100% !important;
  }
</style>
@endsection



<?php
$url=App::getLocale().'/real_estates_save';

?>

@if(Session::get('type')=='1')

<?php 

$url=App::getLocale().'/real_estates_save';

?>
@endif

@if(Session::get('type')=='2')

<?php

$url=App::getLocale().'/item_save';

?>
@endif

@if(Session::get('type')=='3')

<?php 

$url=App::getLocale().'/service_save';

?>
@endif





@section('content')
<!-- START: LOGIN/REGISTER -->
<div class="row login-row">
	<div class="container clear-padding">
		<div class="col-sm-2 useful-links">

		</div>
		<div class="col-sm-5 login-form">

  
			<h4>{{trans('common.new_post')}}</h4>
                       <div class="error_box"></div>


            {!! Form::open(array('url'=> $url,'method'=>'POST', 'files'=>true,'class'=>"form-horizontal",'role'=>"form",'name'=>'real_estate_form','id'=>'myform')) !!}
        

        <div class="form-group">
             <label for="rel_sub">{{mb_convert_case(mb_strtolower(trans('common.category'),'utf-8'), MB_CASE_TITLE, "UTF-8")}}</label>

          
         {{Form::select('main_category', trans('common.main_categories'),null,array('class'=>'form-control','id'=>'rel_sub','onchange'=>'related_sub_category()'))}}

        </div>
        


         <div id='sub_cat'>
            @if(Session::get('type')=='1')

            @include('auth.dashboard.real_estates')

            @elseif(Session::get('type')=='2')

            @include('auth.dashboard.items')

            @elseif(Session::get('type')=='3')

            @include('auth.dashboard.services')

            @else

            @include('auth.dashboard.real_estates')

            @endif


        </div> 
      
         



        


   


  <!-- <div class="form-group">
    <button type="submit" class="btn btn-primary" id="submit">Submit</button>
  </div> -->

<div class="form-group">
  
  <button class="button gray"  id="submit" type="submit" name="submit">{{trans('common.save_post')}}</button>
</div>
 
                                        {!! Form::close() !!}

 <script type="text/javascript">

new FormValidator('real_estate_form', [{
    name: 'area',
    display: "{{trans('common.area_label')}}",
    rules: 'required|numeric'
}, {
    name: 'price',
    display: "{{trans('common.price')}}",
    rules: 'required|numeric'
}, {
    name: 'real_estate_sub_menu_id',
    display: "{{trans('common.real_estate_category')}}",
    rules: 'required'
}, {
    name: 'description',
    display: "{{trans('common.item_description')}}",
    rules: 'required'
}, {
    name: 'address',
    display: "{{trans('common.address')}}",
    rules: 'required'
},
 {
    name: 'item_name',
    rules: 'required',
    display: "{{trans('common.post_name')}}"
},
{
    name: 'item_category_id',
    rules: 'required',
    display: "{{trans('common.item_category')}}"
},
{
    name: 'item_price',
    display: "{{trans('common.price')}}",
    rules: 'required|numeric'
},
{
    name: 'service_price',
    rules: 'required|numeric'
},
{
    name: 'service_name',
    display: 'service_name',
    rules: 'required'
}
,
 {
    name: 'phone',
    display: "{{trans('common.contact_phone')}}",
    rules: 'required'
} 


], function(errors, evt) {

    /*
     * DO NOT COPY AND PASTE THIS CALLBACK. THIS IS CONFIGURED TO WORK ON THE DOCUMENTATION PAGE ONLY.
     * YOU MUST CUSTOMIZE YOUR CALLBACK TO WORK UNDER YOUR ENVIRONMENT.
     */

    var SELECTOR_ERRORS = $('.error_box'),
        SELECTOR_SUCCESS = $('.success_box');

    if (errors.length > 0) {
        SELECTOR_ERRORS.empty();

        var counter=0;

        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {

          counter++;
            SELECTOR_ERRORS.append('<b>'+counter+')</b> '+errors[i].message + '<br />');
        }


        $('html,body').scrollTop(0);

        $('.error_box').addClass('alert alert-danger alert-dismissable');
        SELECTOR_SUCCESS.css({ display: 'none' });
        SELECTOR_ERRORS.fadeIn(200);
    } else {
        SELECTOR_ERRORS.css({ display: 'none' });
        SELECTOR_SUCCESS.fadeIn(200);

 document.example_form.submit();
        /*alert('OK');*/
    }

    if (evt && evt.preventDefault) {
        evt.preventDefault();
    } else if (event) {
      /* alert('asd');*/
    }
});

</script>






 
 
 

                 
                </div>
                <div class="col-sm-5 sign-up-form">
                	<div class="ad_limits_info" style="text-align: justify;"> {!! trans('common.rules') !!}
                				 </div>
                				</div>
                			</div>
                		</div>
                		<!-- END: LOGIN/REGISTER -->

                		<script>
                     

function related_sub_category() {

$('.error_box').text('');
$('.error_box').removeClass('alert-danger');


    if($('#rel_sub').val()==1)
    {
        var formAction ="{{url('real_estates_save')}}";
        
    }

    else if($('#rel_sub').val()==2)

    {
        var formAction ="{{url('item_save')}}";
         
    }
    else
    {
        var formAction ="{{url('service_save')}}";
        

    }


    $("#myform").attr("action", formAction);
    



    $.ajax({

     type: "GET",
     url: "{{url('rel_sub')}}", 
     data: {rel_sub: $('#rel_sub').val()}, 
     async: true,  
     cache:true,
     success: 
     function(data){
      $("#sub_cat").html(data).slideDown('slow');


  }
});

                               // alert('asdsad');
                               return false;
                           }

$( document ).ready(function() {
  $('#submit').click(function(e){
     


    if($("input[name='image_files[]']").val()===undefined)
    {
             alert("{!! trans('common.image_empty')!!}");
             e.preventDefault(e);
    }

     
      
     
   

 
  });









});



                       </script>




                       @endsection

