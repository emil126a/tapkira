@extends('layouts.app')

@section('content')

<?php

$dt = new DateTime();
$date= $dt->format('Y-m-d');
?>

@if(Session::has('id'))




	<!-- START: PAGE TITLE -->
	<div class="row page-title" style="background: rgba(0, 0, 0, 0) url('{{asset('assets/images/bg-image10.jpg')}}') repeat fixed 0 0 / contain ;">
		<div class="container clear-padding text-center flight-title transparent">

		{!!trans('common.thank_you_message',['user'=>Auth::user()->name])!!}
		
			<h4 class="thank"><i class="fa fa-thumbs-o-up"></i> {{trans('common.your_post_accepted')}}</h4>
			<span><i class="fa fa-home"></i>  <i class="fa fa-long-arrow-right"></i> {{Session::get('message')}} <i class="fa fa-calendar"></i> {{$date}}</span><br><br>
		</div>
	</div>
	<!-- END: PAGE TITLE -->
	<!-- START: BOOKING DETAILS -->
	<div class="row">
		<div class="container clear-padding">
			<div>
				<div class="col-md-8 col-sm-8">
					<div class=" confirmation-detail">				 
						<p><br>{{trans('common.note_info',['category'=>Session::get('message'),'post_id'=>Session::get('id')])}}</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 booking-sidebar">
					<div class="sidebar-item contact-box">
						<h4><i class="fa fa-phone"></i>{{trans('common.need_help')}}</h4>
						<div class="sidebar-body text-center">
							<p>{{trans('common.wanna_apply')}}</p>
							<h2>{{Config::get('custom.contact_phone')}}</h2>
						</div>
					</div>
					 
				</div>
			</div>
		</div>
	</div>
	<!-- END: BOOKING DETAILS -->

	@endif
@endsection