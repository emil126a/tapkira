@extends('layouts.app')

@section('content')

<?php
$title='title_'.App::getLocale();
?>

<div class="row user-profile">
		<div class="container">
			 @include('auth.dashboard.dashboard_left_menu')
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
 
					<div id="booking">
						<div class="col-md-3 col-sm-3 col-xs-6">
							<form>

                            {{Form::select('',['0' => trans('common.all_my_ads')]+trans('common.main_categories'),0,array('id'=>'category','onchange'=>'related_sub_category()','class'=>'form-control'))}}
							</form>
						</div>
						<div class="clearfix"></div>
						<div id="related_category"> 
					  @include('auth.dashboard.related_all_ads')
						</div>
					</div>			 
				</div>
			</div>
		</div>
	</div>


<script>

                            /*$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        $('.image_text').text('Şəkil №1');
        if(x < max_fields){ //max input box allowed
            //text box increment
        
            $(wrapper).append('<div><br><label>Şəkil №'+x+'</label><div class="input-group"> <input name="email" type="file" style="padding:0px 0px;border: 0 solid #ccc;" class="form-control" placeholder="Mənzilin sahəsi"> <span style="padding: 0px 0px;" class="input-group-addon"><button class="remove_field" style="margin-top: 0px;height: 40px !important;padding: 0px 10px 0px 10px !important;"><i class="fa fa-minus" style="font-size:30px;"></i></button></span> </div></div>'); //add input box
                 x++;
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ 

       // alert('asddas');

    //user click on remove text
        e.preventDefault(); $(this).parent().parent().parent('div').remove(); 
        x--;
        x=2;
    })
});*/

function related_sub_category() {
 
   $.ajax({

     type: "GET",
     url: "{{url('rel_dashboard')}}", 
     data: {id: $('#category').val()},   
     cache:true,
     success: 
     function(data){
     $('#related_category').html(data);


  }
});

                               return false;
                           }



                       </script>
	@endsection
