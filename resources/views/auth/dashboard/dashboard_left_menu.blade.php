<div class="col-md-12 user-name">
				<h3>{{trans('common.welcome_user',['username'=>Auth::user()->name])}}</h3>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
				 
						<li><a href="{{url('myads')}}" class="text-center"><i class="fa fa-history"></i> <span>{{trans('common.my_ads')}}</span></a></li>


						@if(Auth::user()->facebook_user_id==0)

						<li><a  href="{{url('dashboard_profile')}}" class="text-center"><i class="fa fa-user"></i> <span>{{trans('common.profile')}}</span></a></li>	

						@endif


					</ul>
				</div>
			</div>