<div class="image_files"></div>
<div id="status"></div>
<script>

  $(document).ready(function()
  {
   var count = 1;


   var settings = {
    url: "{{url('upload_service_images')}}",
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    },
    method: "POST",
    allowedTypes:"jpg,png,gif,jpeg",
    fileName: "myfile",
    multiple: true,
    showPreview:true,
    maxFileCount:12,
    statusBarWidth:'100%',
    /*dragdropWidth:"76%",*/
    previewHeight: "100px",
    previewWidth: "100px",
    maxFileSize: 1000000,

    onSuccess:function(files,data,xhr)
    {

/*      alert(data);*/

      $(".image_files").append('<input type="hidden" value="'+data+'" name="image_files[]">');

   },
    deleteCallback: function(data, pd) // Delete function must be present when showDelete is set to true
        {
                 
 
$(':input[value="'+data+'"]').remove();
          


$.ajax({

     type: "POST",
     url: "{{url('delete_service_images')}}", 
     headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    },
    data: {filename: data},   
    cache:true,
    success: 
    function(data){
     // $("#rel_sub_real_estate").html(data).slideDown('slow');

 
   }
 });





        },
    onError: function(files,status,errMsg)
    {   
      $("#status").html(errMsg);
    }
  }
  $("#mulitplefileuploader").uploadFile(settings);
});
</script>


<?php

use App\ServiceSubCategory;

$title='title_'.App::getLocale();
?> 


<div class="form-group">
    <label>{{trans('common.service_category')}}</label>
{{Form::select('service_category_id',$service->toArray(),null,array('class'=>'form-control','onchange'=>'service_sub_category()','id'=>'service_category_id'))}}

</div>


 


<div id='service_sub_category'>
	 @if(old('service_sub_category_id')!='')

<div class="form-group">
    <label>{{trans('common.service_sub_category')}}</label>

  {{
        Form::select('service_sub_category_id', ServiceSubCategory::where('service_category_id', old('service_category_id'))->pluck($title,'id'),null,array('class'=>'form-control'))
  }}
</div>


 
@endif

</div>


<div class="form-group">
      <label class=" {{ $errors->has('service_name') ? ' has-error' : '' }}">{{trans('common.service_name')}}</label>

       <input type="text" value="{{ old('service_name') }}"  placeholder="{{trans('common.service_name_placeholder')}}" class="form-control {{ $errors->has('service_name') ? ' has-error' : '' }}" name="service_name"  maxlength="40" id="service_name"/> 
</div>


<div class="form-group">
<label>{{trans('common.city')}}</label>    
{{Form::select('service_city_id', $cities,null,array('class'=>'form-control'))}}    
</div>

<div class="form-group">
    <label> {{trans('common.service_rent_type')}}</label>
{{Form::select('service_ad_type',trans('common.service_type'),4,array('class'=>'form-control'))}}
</div>





<div class="form-group">
  <label>{{trans('common.image')}}</label>

<div id="mulitplefileuploader">

</div>

</div>

<div class="form-group">
    
    <label class=" {{ $errors->has('service_price') ? ' has-error' : '' }}">{{trans('common.price')}}</label>


<input type="text" value="{{ old('service_price') }}"  placeholder="{{trans('common.service_price_placeholder')}}" class="form-control {{ $errors->has('service_price') ? ' has-error' : '' }}" name="service_price" id="service_price"/>
</div>


<div class="form-group">
    <label class=" {{ $errors->has('description') ? ' has-error' : '' }}" > {{trans('common.services_description_label')}} </label>
        <textarea rows="5" name="description" id="comment" style="height:185px" placeholder="{{trans('common.services_description_placeholder')}}" class="form-control {{ $errors->has('description') ? ' has-error' : '' }}">{{ old('description') }}</textarea>
 @if ($errors->has('description'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span><br>
                                @endif
</div>
    
<div class="form-group">
    <label for="phone" class="control-label">{{trans('common.contact_phone')}}</label>

    <input type="text" id="phone" onkeypress="return isNumber(event)" value="{{(old('phone'))?old('phone'):Auth::user()->phone}}"   placeholder="{{trans('common.contact_phone_placeholder')}}" class="form-control {{ $errors->has('price') ? ' has-error' : '' }}" name="phone"/>


     @if ($errors->has('phone'))
<span class="has-error">
  <strong>{{ $errors->first('phone') }}</strong>
</span><br>
@endif
    <div class="help-block with-errors"></div>
  </div>

                                
<script>

                			function service_sub_category() {
                	 
                				$.ajax({

                					type: "GET",
                					url: "{{url('rel_service_sub_category')}}", 
                					data: {rel_service_sub_category: $('#service_category_id').val()},
                					cache:false,
                					success: 
                					function(data){
                					//	alert(data);
                						$("#service_sub_category").html(data);
                					}
                				});
                				return false;
                			}


                			 
                		</script>

                     <script src="{{asset('assets/js/input-mask.js')}}"></script>
                      <script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>