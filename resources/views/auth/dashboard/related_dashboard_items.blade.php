<style type="text/css">
	.load_item { display:none; }
</style>

<div class="col-md-12">


@if(!count($data_list)>0)
		<br><br>
		<div class="alert alert-danger">
			
{!! trans('common.no_post_own_in_this_category') !!}

		
			
		</div><br><br><br>

  
@else


 
							@foreach($data_list as $data)

							<div class="item-entry load_item">
								<span  style='background-color:#07253f'>{{trans('common.post_id')}}:  {{$data->id}}</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="{{asset('uploads/item_images/'.$data->item_images[0]->image)}}" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>{{$data->title}}  </h4>
											<p>{{trans('common.created_at')}}: {{$data->created_at->diffForHumans()}} </p>
											<p>{{trans('common.updated_at')}}:  {{$data->updated_at->diffForHumans()}}</p>
										</div>
										<div class="col-md-2 col-sm-2">

<?php


$active_status_class=($data->active==0 || $data->active==2)?'failed':'confirmed';
$active_status_icon=($data->active==0 || $data->active==2)?'fa fa-times':'fa fa-check';
?>

											<p class="{{$active_status_class}}"><i class="{{$active_status_icon}}"></i>{{trans("common.active")[$data->active]}}</p>
										</div>
										<div class="col-md-4 col-sm-4">
											<p><a href="{{url('item/'.$data->id)}}" target="_blank">{{trans('common.see')}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="" class="delete_press" data-href="{{url('delete_item/'.$data->id)}}" data-toggle="modal" data-target="#confirm-delete">{{trans('common.delete')}}</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>{{trans('common.real_estate_rent_type_label')}}: </strong> {{trans("common.rent_type")[$data->rent_type]}}<strong>{{trans('common.price')}}:</strong> {{$data->price}} AZN / {{trans("common.payment_period_type")[$data->payment_period_type]}}</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@endforeach		

<div class="text-center load-more">
<button type="button" id="load_click" class="search-button btn transition-effect">{{trans('common.load')}}</button>
  
 
								 
							</div>

							@endif
						</div>









	<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
							


$( document ).ready(function() {
 
 


   $(".load_item").slice(0, 3).show(); // select the first ten
    $("#load_click").click(function(e){ // click event for load more
        e.preventDefault();
        $("div.load_item:hidden").slice(0, 3).fadeIn(); // select next 10 hidden divs and show them
        if($("div.load_item:hidden").length == 0){ // check if any hidden divs still exist
            
            $(this).fadeOut();

          //  alert("No more divs"); // alert if there are none left
        }
    })
});




			 
						</script>


						  <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('common.read_carefully')}}</h4>
                </div>
            
                <div class="modal-body">
                {!!trans('common.delete_description')!!}
                     
                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.no')}}</button>
                    <a class="btn btn-danger btn-ok">{{trans('common.yes')}}</a>
                </div>
            </div>
        </div>
    </div>

     
    



    <script>

   $(document).on("click", ".delete_press", function () {
     var url = $(this).data('href');

      $('.btn-ok').attr('href',url);
   	//alert(myBookId);
});
    </script>