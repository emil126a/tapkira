@extends('layouts.app')

@section('content')

<?php
$title='title_'.App::getLocale();
?>

<div class="row user-profile">
		<div class="container">
			@include('auth.dashboard.dashboard_left_menu')
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">				
					<div id="profile">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>{{trans('common.personal_info')}}</h4>
								<div class="user-info-body">
@if(Session::has('message'))
<div class="alert alert-success">
   {{Session::get('message') }}
</div>

@endif


								{!! Form::open(array('url'=>App::getLocale().'/save_user_data','method'=>'post')) !!}

										<div class="col-md-12 {{ $errors->has('fullname') ? ' has-error' : '' }}">
											<label>{{trans('common.your_fullname')}}</label>
											<input type="text" style="margin-bottom: 0px" value="{{ Auth::user()->name }}" name="fullname" placeholder="{{trans('common.your_fullname_placeholder')}}" class="form-control {{ $errors->has('fullname') ? ' has-error' : '' }}">
											@if ($errors->has('fullname'))
<span class="has-error">
  <strong>{{ $errors->first('fullname') }}</strong>
</span><br>
@endif
										</div>
				<div class="clearfix"></div><br>
											<div class="col-md-12 {{ $errors->has('phone') ? ' has-error' : '' }}">
											<label>{{trans('common.your_phone')}}</label>
											<input  id="phone"  onkeypress="return isNumber(event)" type="text" style="margin-bottom: 0px" value="{{ Auth::user()->phone }}" name="phone" placeholder="(XXX) XXX-XX-XX" class="form-control {{ $errors->has('phone') ? ' has-error' : '' }}">
											@if ($errors->has('phone'))
<span class="has-error">
  <strong>{{ $errors->first('phone') }}</strong>
</span><br>
@endif
										</div>
				<div class="clearfix"></div><br>
										<div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
											<label>{{trans('common.your_email')}}</label>
											<input type="text" style="margin-bottom: 0px" value="{{ Auth::user()->email }}" name="email" placeholder="{{trans('common.your_email_placeholder')}}" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}">

												@if ($errors->has('email'))
<span class="has-error">
  <strong>{{ $errors->first('email') }}</strong>
</span><br>
@endif
										</div>
										 
								 
										<div class="clearfix"></div>
										<div class="col-md-12 col-sm-6 col-xs-6 text-center">
											 <button type="submit">{{trans('common.save')}}</button>
										</div>
										 
									{!! Form::close()!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>{{trans('common.change_password')}}</h4>
								<div class="change-password-body">
								@if(Session::has('error'))
<div class="alert alert-danger">
  {{Session::get('error') }}
</div>

@endif

@if(Session::has('password_message'))
<div class="alert alert-success">
 {{Session::get('password_message') }}
</div>

@endif

 

										{!! Form::open(array('url'=>App::getLocale().'/save_password_change','method'=>'post')) !!}
										<div class="col-md-12" style="margin-top: 15px">
											<label> {{trans('common.old_password')}}</label>
											<input type="password"  style="margin-bottom: 0px" placeholder="{{trans('common.old_password')}}" class="form-control" name="old_password">
										@if ($errors->has('old_password'))
<span class="has-error">
  <strong>{{ $errors->first('old_password') }}</strong>
</span><br>
@endif
										</div>
								 
										<div class="col-md-12" style="margin-top: 15px">
											<label>{{trans('common.new_password')}}</label>
											<input type="password" style="margin-bottom: 0px"  placeholder="{{trans('common.new_password')}}" class="form-control" name="new_password">
												@if ($errors->has('new_password'))
<span class="has-error">
  <strong>{{ $errors->first('new_password') }}</strong>
</span><br>
@endif
										</div>

						 
										<div class="col-md-12" style="margin-top: 15px" >
											<label>{{trans('common.new_password_repeat')}}</label>
											<input type="password"  style="margin-bottom: 0px"  placeholder="{{trans('common.new_password_repeat')}}" class="form-control" name="confirm-password">
																				@if ($errors->has('confirm-password'))
<span class="has-error">
  <strong>{{ $errors->first('confirm-password') }}</strong>
</span><br>
@endif
										</div>
										 
										<div class="col-md-12 text-center"  style="margin-top: 15px">
											 <button type="submit">{{trans('common.save')}}</button>
										</div>
										{!! Form::close()!!}
								</div>
							</div>
						 
						 
						</div>
					</div>
			 
			 
				</div>
			</div>
		</div>
	</div>


<script>

                            /*$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        $('.image_text').text('Şəkil №1');
        if(x < max_fields){ //max input box allowed
            //text box increment
        
            $(wrapper).append('<div><br><label>Şəkil №'+x+'</label><div class="input-group"> <input name="email" type="file" style="padding:0px 0px;border: 0 solid #ccc;" class="form-control" placeholder="Mənzilin sahəsi"> <span style="padding: 0px 0px;" class="input-group-addon"><button class="remove_field" style="margin-top: 0px;height: 40px !important;padding: 0px 10px 0px 10px !important;"><i class="fa fa-minus" style="font-size:30px;"></i></button></span> </div></div>'); //add input box
                 x++;
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ 

       // alert('asddas');

    //user click on remove text
        e.preventDefault(); $(this).parent().parent().parent('div').remove(); 
        x--;
        x=2;
    })
});*/

function related_sub_category() {
 
   $.ajax({

     type: "GET",
     url: "{{url('rel_dashboard')}}", 
     data: {id: $('#category').val()},   
     cache:true,
     success: 
     function(data){
     $('#related_category').html(data);


  }
});

                               return false;
                           }



                       </script>

                        <script src="{{asset('assets/js/input-mask.js')}}"></script>
 <script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>
	@endsection
