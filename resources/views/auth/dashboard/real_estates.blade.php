<script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCk4iM6XrgBpfDCiHX5u1I2vX9xH6T1k8o&libraries=places&language={{App::getLocale()}}'></script>
<script src="{{asset('assets/js/locationpicker.jquery.js')}}"></script>

<div class="image_files"></div>
<div id="status"></div>
<script>

  $(document).ready(function()
  {
   var count = 1;


   var settings = {
    url: "{{url('upload_real_estate_images')}}",
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    },
    method: "POST",
    allowedTypes:"jpg,png,gif,jpeg",
    fileName: "myfile",
    multiple: true,
    showPreview:true,
    maxFileCount:16,
    statusBarWidth:'100%',
/*    dragdropWidth:"76%",*/
    previewHeight: "100px",
    previewWidth: "100px",
    maxFileSize: 1000000,

    onSuccess:function(files,data,xhr)
    {

      
/* alert(data); */
 
      $(".image_files").append('<input value="'+data+'" type="hidden" name="image_files[]">');

   },
    deleteCallback: function(data, pd) // Delete function must be present when showDelete is set to true
        {
               /* alert(data); */
 
$(':input[value="'+data+'"]').remove();
          


$.ajax({

     type: "POST",
     url: "{{url('delete_real_estate_images')}}", 
     headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    },
    data: {filename: data},   
    cache:true,
    success: 
    function(data){
     // $("#rel_sub_real_estate").html(data).slideDown('slow');

 
   }
 });





        },
    onError: function(files,status,errMsg)
    {   
      $("#status").html(errMsg);
    }
  }
  $("#mulitplefileuploader").uploadFile(settings);

});
</script>


<div class="form-group">
  <label class=" {{ $errors->has('real_estate_sub_menu_id') ? ' has-error' : '' }}"> {{trans('common.real_estate_category')}}</label>
<?php

$error=$errors->has('real_estate_sub_menu_id') ? ' has-error' : '';
?>

{{Form::select('real_estate_sub_menu_id',['' => trans('common.select_picker_real_estate_category')]+$real_estate->toArray(),0,array('class'=>'form-control'.$error,'id'=>'real_estate_sub_menu_id','onchange'=>'related_sub_real_estate()'))}}

  <div class="help-block with-errors"></div>




@if ($errors->has('real_estate_sub_menu_id'))


<span class="has-error">
  <strong>{{ $errors->first('real_estate_sub_menu_id') }}</strong>
</span><br>
@endif

</div>




<div class="form-group">
  <label>{{trans('common.real_estate_rent_type')}}</label>

  {{Form::select('payment_period_type',trans('common.payment_period_type_real_estate'),null,array('class'=>'form-control'))}}
</div>



<div class="form-group">
  <label> {{trans('common.real_estate_rent_type_label')}}</label>

  <?php
$real_estate_ad_type= trans('common.rent_type');

?>

{{Form::select('real_estate_ad_type', $real_estate_ad_type,null,array('class'=>'form-control'))}}
</div>
 
  <div class="form-group">
    <label for="price" class="control-label">{{trans('common.price')}}</label>

    <input type="text" id="price"  value="{{ old('price') }}"   placeholder=" {{trans('common.real_estate_price_placeholder')}}" class="form-control {{ $errors->has('price') ? ' has-error' : '' }}" name="price"/>


     @if ($errors->has('price'))
<span class="has-error">
  <strong>{{ $errors->first('price') }}</strong>
</span><br>
@endif
    <div class="help-block with-errors"></div>
  </div>

<div id="rel_sub_real_estate">

 @if(old('real_estate_sub_menu_id')=='1' || old('real_estate_sub_menu_id')=='2' || old('real_estate_sub_menu_id')=='3')

 @include('auth.dashboard.real_estate_apartment')


 @else

 @include('auth.dashboard.real_estate_soil')

 @endif



</div>


<div class="form-group">
  <label>{{trans('common.image')}}</label>

<div id="mulitplefileuploader">

</div>

</div>

<div class="form-group">
  <label class=" {{ $errors->has('area') ? ' has-error' : '' }}" for="area">{{trans('common.area_label')}}</label>
  <div class="input-group {{ $errors->has('area') ? ' has-error' : '' }}">

    <input name="area" value="{{ old('area') }}" id='area' class="form-control" placeholder="{{trans('common.real_estate_area_placeholder')}}">

    <span class="input-group-addon">{{trans('common.m2')}} / {{trans('common.sot')}}</span>


  </div>
 <div class="help-block with-errors"></div>
  @if ($errors->has('area'))
  <span class="has-error">
    <strong>{{ $errors->first('area') }}</strong>
  </span><br>
  @endif




  
</div>


  <div class="form-group">
    
<label  for="description" class=" {{ $errors->has('description') ? ' has-error' : '' }}" >{{trans('common.real_estate_description_label')}}</label>
    <textarea rows="5" id="description" name="description" id="comment" style="height:185px" placeholder="{{trans('common.real_estate_description_placeholder')}}" class="form-control {{ $errors->has('description') ? ' has-error' : '' }}" >{{ old('description') }}</textarea>


@if ($errors->has('description'))
<span class="has-error">
  <strong>{{ $errors->first('description') }}</strong>
</span><br>
@endif
    <div class="help-block with-errors"></div>
  </div>









 <div class="form-group">
    
<label> {{trans('common.city')}}</label>
{{Form::select('real_estate_city_id', $cities,null,array('class'=>'form-control'))}}


 
 
  </div>


  <div class="form-group">
    <label for="phone" class="control-label"> {{trans('common.contact_phone')}}</label>

    <input type="text" id="phone"  onkeypress="return isNumber(event)" value="{{(old('phone'))?old('phone'):Auth::user()->phone}}"   placeholder="{{trans('common.contact_phone_placeholder')}}" class="form-control {{ $errors->has('price') ? ' has-error' : '' }}" name="phone"/>


     @if ($errors->has('phone'))
<span class="has-error">
  <strong>{{ $errors->first('phone') }}</strong>
</span><br>
@endif
    <div class="help-block with-errors"></div>
  </div>



 <div class="form-group">
    
<label class=" {{ $errors->has('address') ? ' has-error' : '' }}" for="us2-address"> {{trans('common.address')}}</label>
<input type="text" value="{{old('address')}}" placeholder="{{trans('common.address_placeholder')}}" class="form-control {{ $errors->has('address') ? ' has-error' : '' }}" name="address"  id="us2-address">


  <div class="help-block with-errors"></div>
 @if ($errors->has('address'))
<span class="has-error">
  <strong>{{ $errors->first('address') }}</strong>
</span><br>
@endif


  </div>



 
  <div class="form-group"><div id="us2" style="100%; height: 200px;"></div></div>





<?php
$long=(old('long'))?old('long'):'49.83675949999997';
$lat=(old('lat'))?old('lat'):'40.37084919999999';

?>
<input type="hidden" class="form-control" style="width: 110px" id="us2-lat" name="lat" />  
<input type="hidden" class="form-control" style="width: 110px" id="us2-lon" name="long" />
<br>





<script>
  $('#us2').locationpicker({
    location: {
      latitude:<?= $lat ?>,
      longitude: <?= $long ?>
    },
    radius: 50,
    inputBinding: {
      latitudeInput: $('#us2-lat'),
      longitudeInput: $('#us2-lon'),
      radiusInput: $('#us2-radius'),
      locationNameInput: $('#us2-address')
    },
    enableAutocomplete: true
  });
</script>







<script>
 function related_sub_real_estate() {



  $.ajax({

   type: "GET",
   url: "{{url('rel_sub_real_estate')}}", 
   data: {real_estate_sub_menu_id: $('#real_estate_sub_menu_id').val()},   
   cache:true,
   success: 
   function(data){
    $("#rel_sub_real_estate").html(data).slideDown('slow');


  }
});

                               // alert('asdsad');
                               return false;
                             }            
                           </script>


 

 <script src="{{asset('assets/js/input-mask.js')}}"></script>
 <script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>
 










