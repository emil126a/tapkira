@extends('layouts.app')

@section('content')

<?php
$title='title_'.App::getLocale();
?>

<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3>Welcome, Lenore</h3>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
				 
						<li class="active"><a data-toggle="tab" href="#booking" class="text-center"><i class="fa fa-history"></i> <span>Mənim elanlarım</span></a></li>	
						<li><a data-toggle="tab" href="#profile" class="text-center"><i class="fa fa-user"></i> <span>Profil</span></a></li>						 						 
					</ul>
				</div>
			</div>
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
 
					<div id="booking" class="tab-pane fade in active">
						<div class="col-md-3 col-sm-3 col-xs-6">
							<form>
								<select class="form-control" id="category" onchange="related_sub_category()">
									<option value="1">Daşınmaz əmlak</option>
									<option value="2">Əşyalar</option>
									<option value="3">Xidmətlər</option>
							 
								</select>
							</form>
						</div>
						<div class="clearfix"></div>
						<div id="related_category"> 
						<div class="col-md-12">

							@foreach($real_estates_list as $real_estate)

							<div class="item-entry">
								<span  style='background-color:#07253f'>Elan nömrəsi:  {{$real_estate->id}}</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="{{asset('uploads/real_estate_images/'.$real_estate->real_estate_images[0]->image)}}" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>{{$real_estate->city->$title}} şəhərində, {{$real_estate->real_estate_category->$title}} </h4>
											<p>Əlavə olunma müddəti: {{$real_estate->created_at->diffForHumans()}} </p>
											<p>Son dəyişiklik müddəti:  {{$real_estate->updated_at->diffForHumans()}}</p>
										</div>
										<div class="col-md-3 col-sm-3">

<?php


$active_status_class=($real_estate->active==0)?'failed':'confirmed';
$active_status_icon=($real_estate->active==0)?'fa fa-times':'fa fa-check';
?>

											<p class="{{$active_status_class}}"><i class="{{$active_status_icon}}"></i>{{trans("common.active")[$real_estate->active]}}</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Düzəliş et</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Sil</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Elanın növü: </strong> {{trans("common.rent_type")[$real_estate->rent_type]}}<strong>Qiyməti:</strong> {{$real_estate->price}} AZN / {{trans("common.payment_period_type")[$real_estate->payment_period_type]}}</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@endforeach						  						  
							<div class="text-center load-more">
								<a href="#">LOAD MORE</a>
							</div>
						</div></div>
					</div>
					<div id="profile" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Şəxsi məlumatlar</h4>
								<div class="user-info-body">
									<form >

										<div class="col-md-12">
											<label>Tam adınız</label>
											<input type="text" value="{{ Auth::user()->name }}" name="fullname" required placeholder="Adınız və Soyadınız" class="form-control">
										</div>
										<div class="col-md-12">
											<label>Email ünvanınız</label>
											<input type="email" value="{{ Auth::user()->email }}" name="email" placeholder="Email ünvanınızı daxil edin" required class="form-control">
										</div>
										 
								 
										<div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
											 <button type="submit">YADDA SAXLA</button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
											<a href="#">LƏĞV ET</a>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Şifrəni dəyiş</h4>
								<div class="change-password-body">
									<form >
										<div class="col-md-12">
											<label>Köhnə şifrə</label>
											<input type="password" placeholder="Köhnə şifrə" class="form-control" name="old-password">
										</div>
										<div class="col-md-12">
											<label>Yeni şifrə</label>
											<input type="password" placeholder="Yeni şifrə" class="form-control" name="new-password">
										</div>
										<div class="col-md-12">
											<label>Yeni şifrə təkrarı</label>
											<input type="password" placeholder="Yeni şifrə təkrarı" class="form-control" name="confirm-password">
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit">YADDA SAXLA</button>
										</div>
									</form>
								</div>
							</div>
						 
						 
						</div>
					</div>
			 
			 
				</div>
			</div>
		</div>
	</div>


<script>

                            /*$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        $('.image_text').text('Şəkil №1');
        if(x < max_fields){ //max input box allowed
            //text box increment
        
            $(wrapper).append('<div><br><label>Şəkil №'+x+'</label><div class="input-group"> <input name="email" type="file" style="padding:0px 0px;border: 0 solid #ccc;" class="form-control" placeholder="Mənzilin sahəsi"> <span style="padding: 0px 0px;" class="input-group-addon"><button class="remove_field" style="margin-top: 0px;height: 40px !important;padding: 0px 10px 0px 10px !important;"><i class="fa fa-minus" style="font-size:30px;"></i></button></span> </div></div>'); //add input box
                 x++;
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ 

       // alert('asddas');

    //user click on remove text
        e.preventDefault(); $(this).parent().parent().parent('div').remove(); 
        x--;
        x=2;
    })
});*/

function related_sub_category() {
 
   $.ajax({

     type: "GET",
     url: "{{url('rel_dashboard')}}", 
     data: {id: $('#category').val()},   
     cache:true,
     success: 
     function(data){
     $('#related_category').html(data);


  }
});

                               return false;
                           }



                       </script>
	@endsection
