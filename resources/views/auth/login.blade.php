@extends('layouts.app')

@section('content')


<!-- START: LOGIN/REGISTER -->
    <div class="row login-row">
        <div class="container clear-padding">
            <div class="col-sm-2 useful-links">
                
            </div>
            <div class="col-sm-5 login-form">
                <h4>{{trans('common.account_login')}}</h4>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
                  
                  <button type="button"  style="width:100%;background-color:#3b5998;" onclick="window.location.href='{{ url('facebook/login') }}'"><i class="fa fa-facebook"></i> {{trans('common.facebook_login')}}</button>
<br>
<input type="hidden" name="_token" value="{{ csrf_token() }}">  
                    <label class=" {{ $errors->has('email') ? ' has-error' : '' }}"> {{trans('common.email')}}</label>
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">

                        <input name="email" type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="  {{trans('common.email')}}" value="{{ old('email') }}">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>

                        
                    </div>
                    @if ($errors->has('email'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span><br>
                                @endif

<br>
                    <label class=" {{ $errors->has('email') ? ' has-error' : '' }}">{{trans('common.password')}}</label>
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input name="password" type="password" class="form-control" placeholder=" {{trans('common.password')}}" value="{{ old('password') }}">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>

                     
                    </div>   
   @if ($errors->has('password'))
                                    <span class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span><br><br>
                                @endif


<div class="input-group">
                      
                                        <label>
                                        <input type="checkbox" name="remember">  {{trans('common.remember_me')}}
                                    </label>
                                  <br><a class="btn btn-link" href="{{ url('password/reset') }}">{{trans('common.forget_password')}}</a>   
                        </div>
                  <button type="submit" style="width:100%">{{trans('common.login')}} <i class="fa fa-sign-in"></i></button> 
     

                </form>
                <br style="clear:both;">
            </div>
            <div class="col-sm-5 sign-up-form" style="text-align: justify;">

            {!!trans('common.login_rules')  !!}
            </div>
        </div>
    </div>
   
@endsection
