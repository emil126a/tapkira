@extends('layouts.app')

@section('content')
<!-- START: LOGIN/REGISTER -->
    <div class="row login-row">
        <div class="container clear-padding">
            <div class="col-sm-2 useful-links">
                
            </div>
            <div class="col-sm-5 login-form">
                <h4> {{trans('common.register')}}</h4>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('register') }}">

  <button type="button"  style="width:100%;background-color:#3b5998;" onclick="window.location.href='{{ url('facebook/login') }}'"><i class="fa fa-facebook"></i> {{trans('common.facebook_login')}}</button>
                  <br>
<input type="hidden" name="_token" value="{{ csrf_token() }}">  
                  
                    <label class=" {{ $errors->has('name') ? ' has-error' : '' }}"> {{trans('common.your_fullname')}}</label>
                    <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">

                        <input name="name" type="text" class="form-control {{ $errors->has('name') ? ' has-error' : '' }}" placeholder="  {{trans('common.your_fullname_placeholder')}}" value="{{ old('name') }}">
                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>

                        
                    </div>
                    @if ($errors->has('name'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span><br>
                                @endif

<br>




    <label class=" {{ $errors->has('email') ? ' has-error' : '' }}">  {{trans('common.email')}}</label>
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">

                        <input name="email" type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder=" {{trans('common.email')}}" value="{{ old('email') }}">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>

                        
                    </div>
                    @if ($errors->has('email'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span><br>
                                @endif

<br>

    <label class=" {{ $errors->has('password') ? ' has-error' : '' }}"> {{trans('common.password')}}</label>
                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">

                        <input name="password" type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="{{trans('common.password')}}" value="{{ old('password') }}">
                        <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>

                        
                    </div>
                    @if ($errors->has('password'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span><br>
                                @endif

<br>



                    <label class=" {{ $errors->has('password_confirmation') ? ' has-error' : '' }}"> {{trans('common.password_repeat')}}</label>
                    <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input name="password_confirmation" type="password" class="form-control" placeholder="{{trans('common.password_repeat')}}" value="{{ old('password_confirmation') }}">
                        <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>

                     
                    </div>   
   @if ($errors->has('password_confirmation'))
                                    <span class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span><br><br>
                                @endif

 
                  <button type="submit" style="width:100%"> {{trans('common.register')}} <i class="fa fa-sign-in"></i></button> 
                      


                </form>
                <br style='clear:both;'>
            </div>
            <div class="col-sm-5 sign-up-form" style="text-align: justify;">
              <div class="ad_limits_info" >

 {!!trans('common.register_rules')!!} 

</div>
            </div>
        </div>
    </div>
    <!-- END: LOGIN/REGISTER -->
@endsection
