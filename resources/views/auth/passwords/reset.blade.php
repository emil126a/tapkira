@extends('layouts.app')

@section('content')

<!-- START: LOGIN/REGISTER -->
    <div class="row login-row">
        <div class="container clear-padding">
            
            <div class="col-sm-5 col-md-5 col-md-offset-4 login-form">
                <h4> {{trans('common.enter_new_password')}}</h4>
       <form class="form-horizontal" role="form" method="POST" action="{{ url('password/reset') }}">

 
                    {{ csrf_field() }}
                      <input type="hidden" name="token" value="{{ $token }}">
                    <label class=" {{ $errors->has('email') ? ' has-error' : '' }}">{{trans('common.your_email')}}</label>
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">

                        <input id="email" name="email" type="text" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="{{trans('common.email')}}" value="{{ $email or old('email') }}">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>

                        
                    </div>
                    @if ($errors->has('email'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span><br>
                                @endif

<br>
                    <label class=" {{ $errors->has('password') ? ' has-error' : '' }}">{{trans('common.new_password')}}</label>
                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input name="password" type="password" class="form-control" placeholder="{{trans('common.new_password')}}">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>

                     
                    </div>   
   @if ($errors->has('password'))
                                    <span class="{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span><br><br>
                                @endif

         <br>
                    <label class=" {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">{{trans('common.new_password_repeat')}}</label>
                    <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input name="password_confirmation" type="password" class="form-control" placeholder="{{trans('common.new_password_repeat')}}">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>

                     
                    </div>   
   @if ($errors->has('password_confirmation'))
                                    <span class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span><br><br>
                                @endif                       

 
<button type="submit" style="float:left;"> {{trans('common.update_password')}} <i class="fa fa-btn fa-refresh"></i></button>
                    

                    


                </form>
            </div>
        
        </div>
    </div>
   
@endsection
