@extends('layouts.app')

<!-- Main Content -->
@section('content')



<!-- START: LOGIN/REGISTER -->
    <div class="row login-row">
        <div class="container clear-padding">
            <div class="col-sm-2 useful-links">
                
            </div>
            <div class="col-sm-5 login-form">
                <h4>{{ trans('common.restore_my_password') }}</h4>
                 @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

          <form class="form-horizontal" role="form" method="POST" action="{{ url('password/email') }}">
                        {{ csrf_field() }}


<input type="hidden" name="_token" value="{{ csrf_token() }}">  
                    <label class=" {{ $errors->has('email') ? ' has-error' : '' }}">{{ trans('common.registered_email') }} </label>
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">

                        <input name="email" type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="{{ trans('common.email') }}" value="{{ old('email') }}">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>

                        
                    </div>
                    @if ($errors->has('email'))
                                    <span class="has-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span><br>
                                @endif

 


 
<button type="submit" style="float:left; width: 100%;padding: 10px 27px;">   {{ trans('common.sendto_registered_email') }} <i class="fa fa-sign-in"></i></button> 
                     

                    


                </form>
            </div>
            <div class="col-sm-5 sign-up-form" style="text-align: justify;">

            {!! trans('common.password_restore_rules') !!}

         
            </div>
        </div>
    </div>
    <br><br><br><br>
@endsection
