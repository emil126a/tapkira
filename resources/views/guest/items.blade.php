

<div class="image_files"></div>
<div id="status"></div>
<script>

  $(document).ready(function()
  {
   var count = 1;


   var settings = {
    url: "{{url('upload_item_images')}}",
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    },
    method: "POST",
    allowedTypes:"jpg,png,gif,jpeg",
    fileName: "myfile",
    multiple: true,
    showPreview:true,
    maxFileCount:12,
    statusBarWidth:'100%',
   /* dragdropWidth:"100%",*/
    previewHeight: "100px",
    previewWidth: "100px",
    maxFileSize: 1000000,

    onSuccess:function(files,data,xhr)
    {

  /*    alert(data);*/

      $(".image_files").append('<input type="hidden" value="'+data+'" name="image_files[]">');

   },
    deleteCallback: function(data, pd) // Delete function must be present when showDelete is set to true
        {
                 
 
$(':input[value="'+data+'"]').remove();
          


$.ajax({

     type: "POST",
     url: "{{url('delete_item_images')}}", 
     headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    },
    data: {filename: data},   
    cache:true,
    success: 
    function(data){
     // $("#rel_sub_real_estate").html(data).slideDown('slow');

 
   }
 });





        },
    onError: function(files,status,errMsg)
    {   
      $("#status").html(errMsg);
    }
  }
  $("#mulitplefileuploader").uploadFile(settings);
});
</script>





<?php

use App\ItemSubCategory;

$title='title_'.App::getLocale();
?> 
 


 
<div class="form-group">
  <label for='item_category_id'>{{trans('common.item_category')}}</label>
{{Form::select('item_category_id',['' => trans('common.select_picker_item_category')]+$item->toArray(),null,array('class'=>'form-control','id'=>'item_category_id','onchange'=>'item_sub_category()'))}}
  <div class="help-block with-errors"></div>
  <span class="has-error">
    <strong>{{ $errors->first('item_category_id') }}</strong>
  </span> 
</div>

 





<div id='item_sub_category'>
	
@if(old('sub_menu_id')!='')


<div class="form-group">
  <label for='sub_menu_id1' class=" {{ $errors->has('sub_menu_id') ? ' has-error' : '' }}" >{{trans('common.item_sub_category')}}</label>

<?php

$error=$errors->has('sub_menu_id') ? ' has-error' : '';
?>




 {{Form::select('sub_menu_id', ItemSubCategory::where('item_category_id', old('item_category_id'))->pluck($title,'id'),null,array('class'=>'form-control','id'=>'sub_menu_id1'))}}

 <div class="help-block with-errors"></div>




@if ($errors->has('sub_menu_id'))


<span class="has-error">
  <strong>{{ $errors->first('sub_menu_id') }}</strong>
</span><br>
@endif



</div>
@endif
</div>

  <div class="form-group">
    <label class=" {{ $errors->has('item_name') ? ' has-error' : '' }}" for="item_name">{{trans('common.post_name')}}</label>

<input type="text" value="{{ old('item_name') }}"  placeholder="{{trans('common.post_name_placeholder')}}" class="form-control {{ $errors->has('item_name') ? ' has-error' : '' }}" name="item_name" id="item_name" maxlength="40" />
  </div>

<div class="form-group">
  
  <label for="city">{{trans('common.city')}}</label>
{{Form::select('item_city_id', $cities,null,array('class'=>'form-control','id'=>'city'))}}
</div>



<div class="form-group">
  <label for="item_ad_type">{{trans('common.item_rent_type_label')}}</label>
  {{Form::select('item_ad_type', trans('common.item_rent_type'),4,array('class'=>'form-control','id'=>'item_ad_type'))}}
</div>

 



<div class="form-group">
  
  <label for="payment_period_type">{{trans('common.item_rent_period_label')}}</label>

  {{Form::select('payment_period_type',trans('common.payment_period_type'),null,array('class'=>'form-control','id'=>'payment_period_type'))}}
</div>


   




<div class="form-group">
  <label>{{trans('common.image')}}</label>

<div id="mulitplefileuploader">

</div>

</div>

<div class="form-group">
  <label class=" {{ $errors->has('item_price') ? ' has-error' : '' }}" for="item_price">{{trans('common.price')}}</label>
<input type="text" value="{{ old('item_price') }}"  placeholder="{{trans('common.item_price_placeholder')}}" class="form-control {{ $errors->has('item_price') ? ' has-error' : '' }}" name="item_price"  />

@if ($errors->has('item_price'))
<span class="has-error">
  <strong>{{ $errors->first('item_price') }}</strong>
</span><br>
@endif


</div>



<div class="form-group">

  <label class=" {{ $errors->has('item_price') ? ' has-error' : '' }}" for="item_price">{{trans('common.item_condition_label')}}</label><br>
	<label>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="is_new"  value="1"> {{trans('common.is_new')}}
  </label>


</div>


<div class="form-group">
  <label class=" {{ $errors->has('description') ? ' has-error' : '' }}" for="comment">{{trans('common.item_description')}}</label>

<textarea rows="5" name="description" id="comment" style="height:185px" placeholder="{{trans('common.item_description_placeholder')}}" class="form-control {{ $errors->has('description') ? ' has-error' : '' }}">{{ old('description') }}</textarea>

  <div class="help-block with-errors"></div>

@if ($errors->has('description'))
<span class="has-error">
    <strong>{{ $errors->first('description') }}</strong>
</span><br>
@endif
</div>


<div class="form-group">
    <label for="phone" class="control-label">{{trans('common.contact_phone')}}</label>

    <input type="text" id="phone" onkeypress="return isNumber(event)" value="{{(old('phone'))?old('phone'):''}}"   placeholder="{{trans('common.contact_phone_placeholder')}}" class="form-control {{ $errors->has('price') ? ' has-error' : '' }}" name="phone"/>


     @if ($errors->has('phone'))
<span class="has-error">
  <strong>{{ $errors->first('phone') }}</strong>
</span><br>
@endif
    <div class="help-block with-errors"></div>
  </div>





    <script>

     function item_sub_category() {

        $.ajax({

           type: "GET",
           url: "{{url('rel_item_sub_category/guest')}}", 
           data: {rel_item_sub_category: $('#item_category_id').val()},       
           cache:false,
           success: 
           function(data){
                    						//alert(data);
                    						$("#item_sub_category").html(data);
                    					}
                    				});
        return false;
    }
    </script>

     <script src="{{asset('assets/js/input-mask.js')}}"></script>
      <script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>