<?php

$real_estate_sub_id=(isset($real_estate_sub_id))?$real_estate_sub_id:$default_real_sub_id;


$real_estate_sub_id=(isset($real_estate_sub_id))?$real_estate_sub_id:$default_real_sub_id;
$rent_type=isset($_GET['rent_type'])?$request->rent_type:'';
$min_price=isset($_GET['min_price'])?$request->min_price:'';
$max_price=isset($_GET['max_price'])?$request->max_price:'';

$city_id=isset($_GET['city_id'])?$request->city_id:'default';

$payment_period_type=isset($_GET['city_id'])?$request->payment_period_type:'default';
//$city_id=($request->has('city_id'))?$request->city_id:'null';

?>
 
<div class="col-md-4">
					<div class="form-gp">
						<label>{{trans('common.real_estate_category')}}</label>
						<div class="input-group margin-bottom-sm">
					 
					 

						 {{Form::select('cat_id', $categories,$real_estate_sub_id,array('class'=>'selectpicker','id'=>'real_estate_sub_menu_id','onchange'=>'related_sub_real_estate_search_form()'))}}
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-gp">
						<label>{{trans('common.real_estate_rent_type_label')}}</label>
						<div class="input-group margin-bottom-sm">
					 
						{{Form::select('rent_type',[''=>trans('common.any_real_estate_rent_type')]+trans('common.rent_type'),$rent_type,array('class'=>'selectpicker'))}}
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.min_price')}}</label>
  <input type="text" id="child_count" name="min_price" onkeypress="return isNumber(event)"  placeholder="0"  maxlength="11"  value="{{$min_price}}" class="form-control quantity-padding">

					</div>
				</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.max_price')}}</label>
  <input type="text" id="adult_count" name="max_price" onkeypress="return isNumber(event)" placeholder="1000"  value="{{$max_price}}" maxlength="11" class="form-control quantity-padding">

					</div>
				</div>
				 

				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.city')}} </label>
						{{Form::select('city_id',[''=>trans('common.any_city')]+$cities,$city_id,array('class'=>'selectpicker'))}}
					</div>
				</div>
				 
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.real_estate_rent_period')}}</label>
						{{Form::select('payment_period_type',['' => trans('common.any_real_estate_rent_period')]+trans('common.payment_period_type_real_estate'),$payment_period_type,array('class'=>'selectpicker'))}}
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="form-gp">
						<button type="submit" class="modify-search-button btn transition-effect">{{trans('common.search')}}</button>
					</div>
				</div>
		 
<!-- END: MODIFY SEARCH -->

 
<script src="{{asset('assets/js/js.js')}}"></script> 