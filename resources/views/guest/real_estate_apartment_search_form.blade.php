<?php

$real_estate_sub_id=(isset($real_estate_sub_id))?$real_estate_sub_id:$default_real_sub_id;
$rent_type=isset($_GET['rent_type'])?$request->rent_type:'';
$min_price=isset($_GET['min_price'])?$request->min_price:'';
$max_price=isset($_GET['max_price'])?$request->max_price:'';
$apartment_type=isset($_GET['apartment_type'])?$request->apartment_type:'';
$city_id=isset($_GET['city_id'])?$request->city_id:'';
$room_numbers=isset($_GET['room_numbers'])?$request->room_numbers:'';
$payment_period_type=isset($_GET['payment_period_type'])?$request->payment_period_type:'';
$priority_person=isset($_GET['priority_person'])?$request->priority_person:'';

 
?>

 
			<div class="col-md-3">
					<div class="form-gp">
						<label>{{trans('common.real_estate_category')}}</label>
						<div class="input-group margin-bottom-sm">
					 
						 {{Form::select('cat_id', $categories,$real_estate_sub_id,array('class'=>'selectpicker','id'=>'real_estate_sub_menu_id','onchange'=>'related_sub_real_estate_search_form()'))}}
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-gp">
						<label>{{trans('common.real_estate_rent_type_label')}}</label>
						<div class="input-group margin-bottom-sm">
					 

					   {{Form::select('rent_type',['' =>trans('common.any_real_estate_rent_type')]+trans('common.rent_type'),$rent_type,array('class'=>'selectpicker'))}}
 
						</div>
					</div>
				</div>


				<div class="col-md-2">
					<div class="form-gp">
						<label>{{trans('common.for_who_label')}}</label>
						<div class="input-group margin-bottom-sm">
					 

					   {{Form::select('priority_person',['' => trans('common.any_for_who')]+trans('common.priority_person'),$priority_person,array('class'=>'selectpicker'))}}
 
						</div>
					</div>
				</div>



				<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.min_price')}}</label>
  <input type="text" id="child_count" name="min_price" placeholder="0" onkeypress="return isNumber(event)"   maxlength="11"  value="{{$min_price}}" class="form-control quantity-padding">

					</div>
				</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.max_price')}}</label>
  <input type="text" id="adult_count" name="max_price" placeholder="1000" onkeypress="return isNumber(event)"   value="{{$max_price}}" maxlength="11" class="form-control quantity-padding">

					</div>
				</div>
				 <div class="clearfix"></div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.real_estate_condition_label')}}</label>

						 {{Form::select('apartment_type',['' => trans('common.any_real_estate_condition')]+trans('common.array_real'),$apartment_type,array('class'=>'selectpicker'))}}
  
					</div>
				</div>

				<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.city')}}</label>

						{{Form::select('city_id',['' => trans('common.any_city')]+$cities,$city_id,array('class'=>'selectpicker'))}}
					 
					</div>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.room_numbers_label')}}</label>
						{{Form::select('room_numbers',['' => trans('common.any_room_numbers')]+trans('common.real_room_numbers'),$room_numbers,array('class'=>'selectpicker'))}}
					 
					</div>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="form-gp">
						<label>{{trans('common.real_estate_rent_period')}}</label>


						{{Form::select('payment_period_type',['' => trans('common.any_real_estate_rent_period')]+trans('common.payment_period_type_real_estate'),$payment_period_type,array('class'=>'selectpicker'))}}


						 
					</div>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12">
					<div class="form-gp">
						<button type="submit" class="modify-search-button btn transition-effect">{{trans('common.search')}}</button>
					</div>
				</div>
 

<script src="{{asset('assets/js/js.js')}}"></script> 