 @extends('layouts.app')

@section('content')
<br>

 
	<!-- END: PAGE TITLE -->
 <!-- START: CONTACT-US -->
	<div class="row contact-address">
		<div class="container clear-padding">
			<div class="text-center">
			 	<div class="col-md-4 col-sm-4">
					<i class="fa fa-map-marker"></i>
					<p>Ünvan</p>
				</div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-envelope-o"></i>
					<p><a href="mailto:your@email.com">{{Html::mailto('info@tapkira.az')}}</a></p>
				</div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-phone"></i>
					<p>{{Config::get('custom.contact_phone')}}</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END: CONTACT-US -->
	<br>
	<!-- START: MAP & CONTACT FORM -->
	<div class="row">
		<div class="container">
		 
			<div class="col-md-12 col-sm-12 contact-form">

			@if(Session::has('message'))
			  <div class="alert alert-success" role="alert">
     
    {!!Session::get('message')!!}
  </div>

  @endif
				<div class="col-md-12">
					<h2>{{trans('common.contact_us')}}</h2>
					<h5>{{trans('common.send_us_message')}}</h5>
				</div>
			  {!! Form::open(array('url'=>url('contact'),'method'=>'POST')) !!}
					<div class="col-md-6 col-sm-6  {{ $errors->has('fullname') ? ' has-error' : '' }}">
						<input type="text" name="fullname" class="form-control" placeholder="{{trans('common.name_placeholder')}}">
						@if ($errors->has('fullname'))
<span class="has-error">
  <strong>{{ $errors->first('fullname') }}</strong>
</span><br>
@endif
					</div>
					<div class="col-md-6 col-sm-6 {{ $errors->has('email') ? ' has-error' : '' }}">
						<input type="text" name="email" class="form-control" placeholder="{{trans('common.email_placeholder')}}">
										@if ($errors->has('email'))
<span class="has-error">
  <strong>{{ $errors->first('email') }}</strong>
</span><br>
@endif
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
					<select class="form-control" name="subject">
					@foreach(trans('common.email_subject') as $subject)
						<option value="{{$subject}}">{{$subject}}</option>
					@endforeach						 
					</select>
						
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12 {{ $errors->has('message') ? ' has-error' : '' }}">
						<textarea class="form-control " rows="5" name="message" id="message" placeholder="{{trans('common.message_placeholder')}}"></textarea>
														@if ($errors->has('message'))
<span class="has-error">
  <strong>{{ $errors->first('message') }}</strong>
</span><br>
@endif

						<button type="submit" class="btn btn-default submit-review">{{trans('common.send')}}</button>
					</div>
					<div class="clearfix"></div>
					 
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!-- END: MAP & CONTACT FORM -->
	@endsection