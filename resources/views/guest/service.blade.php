<?php
$title='title_'.App::getLocale();

 function br2nl($text)
    {
        $input  =  array ("<br />","<br>", "<br  />");
        $output=array("","","");
                                     
        return   str_replace ($input,$output,$text);                                                          
    }


?>
	@extends('layouts.app')
	@section('head')

	<style>
		#map {
			height: 250px;
			width: 100%;
		}
	</style>
	@endsection


@section('ogimage'){{(count($service_result)>0 && $service_result[0]['service_images'][0]->image!='')?URL::asset('uploads/service_images/'.$service_result[0]['service_images'][0]->image):asset('assets/images/og_image.png')}}@stop
   
 

                          

@section('title')
{{(count($service_result)>0)?$service_result[0]->title:trans('common.non_active_post')}}
@stop



 @section('description')

{{(count($service_result)>0)?br2nl(substr($service_result[0]->description,0,200)):  trans('common.non_active_post')}}
@stop

	@section('body')
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/{{App::getLocale().'_'.strtoupper(App::getLocale())}}/sdk.js#xfbml=1&version=v2.8&appId=272183479847809";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>


	@endsection




	@section('content')

 





     
@if(count($service_result)>0)


	@foreach($service_result as $service) 











	<div class="row hotel-detail">
		<div class="container">
			<div class="product-brief-info">
				<div class="col-md-8 clear-padding">











				<div class="demo-slider-3">
    <div id="amazingslider-19" style="display:block;position:relative;margin:15px auto 30px;">
       
<span id="image_over_text" style="bottom: -6px; position: relative; z-index: 555; float: right; right: 8px; display: block; background-color:rgba(0,0,0,0.5);color:#FFF ;font-size: 12px;">{{trans('common.click_expand_image')}}</span>


        <ul class="amazingslider-slides" style="display:none;">
       
@foreach($service->service_images as $image)
							<li>
							   <a href="{{asset('uploads/service_images/'.$image->image)}}" class="html5lightbox"><img src="{{asset('uploads/service_images/'.$image->image)}}" /></a>         
								 
							</li>

						@endforeach
        </ul>




        <ul class="amazingslider-thumbnails" style="display:none;">



@foreach($service->service_images as $image)
							<li>
							  <a href="{{asset('uploads/service_images/'.$image->image)}}"><img src="{{asset('uploads/service_images/'.$image->image)}}" /></a>       
								 
							</li>

						@endforeach
        </ul>
        
 
    </div>
     
</div>
				</div>	
				<div class="col-md-4 detail clear-padding">
					<h4><i class="fa fa-home"></i>{{trans('common.about_service')}}</h4>
					<div class="detail-body">
						<ul>
							<li>{{trans('common.post_id')}}: {{$service->id}}</li>


							<li>{{trans('common.city')}}: {{$service->city->$title}}</li>

							<li>{{trans('common.service_category')}}: {{$service->service_category->$title}}</li>									 

							@if($service->service_sub_category!=null)
							<li>{{trans('common.service_sub_category')}}: {{$service->service_sub_category->$title}}  </li>

							@endif

							<li>{{trans('common.service_rent_type')}}: {{trans('common.service_type')[$service->service_ad_type]}} </li>
							
						</ul>
					</div>
					<div class="price-detail">
						<div class="col-md-7 col-sm-6 col-xs-6 clear-padding text-center">
							<h3>{{$service->price}} AZN<span> </h3>
						</div>

					</div>
				</div>
			</div>
						<div class="col-md-8 "> <span style="float: right;" class="label label-default">{{trans("common.updated_at")}}: {{$service->updated_at->diffForHumans()}}</span></div>
		</div>
	</div>




	<div class="row product-complete-info">
		<div class="container">
			<div class="main-content col-md-8 clear-padding">
				<div class="room-complete-detail">
					<ul class="nav nav-tabs">
						<li class="active col-md-6 col-sm-6 col-xs-6 text-center"><a data-toggle="tab" href="#overview"><i class="fa fa-bolt"></i> <span>{{trans("common.about_service")}}</span></a></li>
						<li class="col-md-6 col-sm-6 col-xs-6 text-center"><a data-toggle="tab" href="#review"><i class="fa fa-comments"></i> <span>{{trans('common.comments')}}</span></a></li>

					</ul> 
					<div class="tab-content">
						<div id="overview" class="tab-pane active in fade" style="min-height: 60vh;">

							{!! nl2br(e($service->description)) !!}
				 <br><br><br><br><br><br>
						</div>
						<div id="review" class="tab-pane fade">

							<div class="review-header">

								<div class="clearfix"></div>
								<div class="guest-review">

									<div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="5"></div>


								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
<div class="col-md-4 hotel-detail-sidebar clear-padding">
				<div class="col-md-12 sidebar-wrapper clear-padding">
					<div class="contact sidebar-item">
						<h4><i class="fa fa-phone"></i> {{trans('common.contact_details')}}</h4>
						<div class="sidebar-item-body">
							<h5><i class="fa fa-user"></i> {{$service->user->name}}</h5>	
							<h5><i class="fa fa-phone"></i> {{$service->user->phone}}</h5>		 
						</div>
					</div>
					 
				</div>
			</div>
		</div>
	</div>
	<!-- END: ROOM GALLERY -->




	@endforeach
 
@else

 <div class="row hotel-detail">
		<div class="container">
<br><br><br><br><br>
	
<div class="alert alert-danger">
{!!trans('common.non_active_post')!!}
</div>

<br><br><br><br><br><br><br><br><br><br><br>
</div></div>


 @endif

@endsection



	 	 @section('bottom')
  @include('guest.slider_files')
<script type="text/javascript">
    
    $(document).ready(function()
    {
        $('#image_over_text').show();
    })
</script>
@endsection