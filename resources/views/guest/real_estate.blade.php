<?php

$title='title_'.App::getLocale();

  function br2nl($text)
    {
        $input  =  array ("<br />","<br>", "<br  />");
        $output=array("","","");
                                     
        return   str_replace ($input,$output,$text);                                                          
    }
?>	
@extends('layouts.app')
@section('head')
   

<style>
       #map {
        height: 250px;
        width: 100%;
       }
    </style>
@endsection


@section('ogimage'){{(count($real_estate_result)>0 && $real_estate_result[0]['real_estate_images'][0]->image!='')?URL::asset('uploads/real_estate_images/'.$real_estate_result[0]['real_estate_images'][0]->image):asset('assets/images/og_image.png')}}@stop
   
 

                          

@section('title')
{{(count($real_estate_result)>0)?trans('common.in_city', ['city' => $real_estate_result[0]->city->$title]).', '. $real_estate_result[0]->real_estate_category->$title:trans('common.non_active_post')}}
@stop


 @section('description')

{{(count($real_estate_result)>0)?br2nl(substr($real_estate_result[0]->description,0,200)):  trans('common.non_active_post')}}
@stop


@section('body')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/{{App::getLocale().'_'.strtoupper(App::getLocale())}}/sdk.js#xfbml=1&version=v2.8&appId=272183479847809";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


@endsection




@section('content')


   
   
@if(count($real_estate_result)>0)
   
     
    

@foreach($real_estate_result as $real_estate) 

<?php

$area_measurement=($real_estate->real_estate_category->id==8)?trans('common.sot'):trans('common.m2');
 
?>

 

   

   


       
 <div class="row hotel-detail">
		<div class="container">


 

			<div class="product-brief-info">
				<div class="col-md-8 clear-padding">











<div class="demo-slider-3">
    <div id="amazingslider-19" style="display:block;position:relative;margin:15px auto 30px;">
     	
<span id="image_over_text" style="bottom: -6px; position: relative; z-index: 555; float: right; right: 8px; display: block; background-color:rgba(0,0,0,0.5);color:#FFF ;font-size: 12px;">{{trans('common.click_expand_image')}}</span>
     	   <ul class="amazingslider-slides" style="display:none;">
       
@foreach($real_estate->real_estate_images as $image)
							 
							   <li><a href="{{asset('uploads/real_estate_images/'.$image->image)}}" class="html5lightbox"><img src="{{asset('uploads/real_estate_images/'.$image->image)}}" /></a>            </li>
								 
							 

						@endforeach
        </ul>




        <ul class="amazingslider-thumbnails" style="display:none;">
@foreach($real_estate->real_estate_images as $image)
						 
							   <li><a href="{{asset('uploads/real_estate_images/'.$image->image)}}"><img src="{{asset('uploads/real_estate_images/'.$image->image)}}" /></a>            </li>
								 
 

						@endforeach
        </ul>
        
 
    </div>
    
</div>	




























			


				</div>	
				<div class="col-md-4 detail clear-padding">
					<h4><i class="fa fa-home"></i>{{trans('common.about_real_estate')}}</h4>
					<div class="detail-body">
						<ul>
							<li>{{trans('common.post_id')}}: {{$real_estate->id}}</li>
							<li>{{trans('common.real_estate_category')}}: {{$real_estate->real_estate_category->$title}}</li>
									@if($real_estate->room_number!="")
							<li>{{trans('common.room_numbers_label')}}: {{$real_estate->room_number}}</li>
							@endif
							<li>{{trans('common.city')}}: {{$real_estate->city->$title}}</li>
							<li>{{trans('common.area_label')}}: {{$real_estate->area}} {{$area_measurement}}</li>
								@if($real_estate->client_priority!="")
							<li>{{trans('common.for_who_label')}}: {{trans("common.priority_person")[$real_estate->client_priority]}}</li>
							@endif
							<li>{{trans('common.real_estate_rent_type')}}: {{trans("common.rent_type")[$real_estate->rent_type]}}</li>
							
							
						</ul>
					</div>
					<div class="price-detail">
						<div class="col-md-7 col-sm-6 col-xs-6 clear-padding text-center">
							<h3>{{$real_estate->price}} AZN<span>/{{trans("common.payment_period_type_real_estate")[$real_estate->payment_period_type]}}</span></h3>
						</div>
						 
					</div>
				</div>
			</div>

			<div class="col-md-8 "> <span style="float: right;" class="label label-default">{{trans("common.updated_at")}}: {{$real_estate->updated_at->diffForHumans()}}</span></div>
		</div>
	</div>




 <div class="row product-complete-info">
		<div class="container">
			<div class="main-content col-md-8 clear-padding">
				<div class="room-complete-detail">
					<ul class="nav nav-tabs">
						<li class="active col-md-6 col-sm-6 col-xs-6 text-center"><a data-toggle="tab" href="#overview"><i class="fa fa-bolt"></i> <span> {{trans('common.about_real_estate')}}</span></a></li>
						<li class="col-md-6 col-sm-6 col-xs-6 text-center"><a data-toggle="tab" href="#review"><i class="fa fa-comments"></i> <span>{{trans('common.comments')}}</span></a></li>
						 
					</ul> 
					<div class="tab-content">
						<div id="overview" class="tab-pane active in fade" style="min-height: 60vh;">
							 
							 	 {!! nl2br(e($real_estate->description)) !!}
					 
						</div>
						<div id="review" class="tab-pane fade">
							 
							<div class="review-header">
						 
								<div class="clearfix"></div>
								<div class="guest-review">
									 

<div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="5"></div>
								 
								 
								</div>
							</div>
						</div>
						 
					</div>
				</div>
			</div>
			<div class="col-md-4 hotel-detail-sidebar clear-padding">
				<div class="col-md-12 sidebar-wrapper clear-padding">
					<div class="contact sidebar-item">
						<h4><i class="fa fa-phone"></i> {{trans('common.contact_details')}}</h4>
						<div class="sidebar-item-body">
							<h5><i class="fa fa-user"></i> {{$real_estate->user->name}}</h5>	
							<h5><i class="fa fa-phone"></i> {{$real_estate->user->phone}}</h5>		 
						</div>
					</div>
					<div class="review sidebar-item">
						<h4><i class="fa fa-edit"></i> {{trans('common.look_map')}}</h4>
						<div class="sidebar-item-body text-center" style="padding: 0px 0px;">
					<br>
					{{trans('common.address')}}:   {{$real_estate->address}}
					<br><br>
							 			 <!-- Google Map HTML Codes -->
           <div id="map"></div>
          <!-- Google Map Javascript Codes -->
 

<?php

$lat=$real_estate->lat;
$lng=$real_estate->lng;
?>
 <script>
      function initMap() {
        var uluru = {lat: <?= $lat ?>, lng: <?= $lng ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByZGQs9bSHmyNsdge5xxBae7mSLDTFBzc&callback=initMap">
    </script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END: ROOM GALLERY -->
 



 @endforeach




	@endsection




	 @section('bottom')
  @include('guest.slider_files')
<script type="text/javascript">
    
    $(document).ready(function()
    {
        $('#image_over_text').show();
    })
</script>

@else
 <div class="row hotel-detail">
		<div class="container">
<br><br><br><br><br>
	
<div class="alert alert-danger">
{!!trans('common.non_active_post')!!}
</div>

<br><br><br><br><br><br><br><br><br><br><br>
</div></div>

@endif
@endsection