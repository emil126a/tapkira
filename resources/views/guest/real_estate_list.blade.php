<?php
$title="title_".App::getLocale();

?>

@extends('layouts.app')
@section('head')

<style type="text/css">
	
	label
	{
		/*text-shadow: 0 0 3px #FF0000, 0 0 5px #0000FF;*/

		background-color: #FF6617;
		padding: 0 8px 0 8px;
		font-size: 14px;
		font-weight: 500 !important;

	}


</style>
@endsection
@section('content')


<!-- START: MODIFY SEARCH -->
	<div class="row modify-search modify-car">
		<div class="container clear-padding">
			            {!! Form::open(array('url'=> url('real_estate_list/'.$cat_id),'method'=>'get')) !!}
<div id="related_sub_real_estate_search_form">
@if($cat_id==1 || $cat_id==2 || $cat_id==3)
@include('guest.real_estate_apartment_search_form',['default_real_sub_id' => $cat_id])

@else
@include('guest.real_estate_soil_search_form', ['default_real_sub_id' => $cat_id,'request'=>$request])
@endif
</div>
			  {!! Form::close()!!}
		</div>
	</div>
<!-- END: MODIFY SEARCH -->

<div class="row">
	<div class="container">
	@if(!count($real_estate_list)>0)
		<br><br><br><br><br><br>
		 <div class="alert alert-danger">
		{!!trans('common.not_found_in_category')!!}		
</div><br><br><br><br><br><br><br><br>
@else
		
		<!-- START: INDIVIDUAL LISTING AREA -->
		<!-- START: INDIVIDUAL LISTING AREA -->
		<div class="col-md-12 hotel-listing">
			
			 
			<div class="clearfix visible-xs-block"></div>
			<div class="col-sm-2 view-switcher"  style='float:right;'>
				<div class="pull-right">
					<a class="switchgrid" title="Grid View">
						<i class="fa fa-th-large"></i>
					</a>
					<a class="switchlist active" title="List View">
						<i class="fa fa-list"></i>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- START: HOTEL LIST VIEW -->
			<div class="switchable col-md-12 clear-padding">
				 
			 @foreach($real_estate_list as $real_estate)
<?php
	$area_measurement=($real_estate->real_estate_category->id==8)?trans('common.sot'):trans('common.m2');
?>
<div class="hotel-list-view" @if($real_estate->vip==1) style="background-color:#fff9bc;"   @endif>
					<div class="wrapper">
						<div class="col-md-4 col-sm-6 switch-img clear-padding" style="font-size:12px; font-weight:bold;">
					 
							<img src="{{asset('uploads/real_estate_images/'.$real_estate->real_estate_images[0]->image)}}" alt="Əmlakın şəkli">
						
						
						
						</div>
						<div class="col-md-6 col-sm-6 hotel-info">
							<div>
								<div class="hotel-header">
									<h5>{{trans('common.in_city', ['city' => $real_estate->city->$title])}},

                           {{$real_estate->real_estate_category->$title}} </h5>
									 
								</div>						 
								<div class="car-detail" style='font-size:12px;font-weight:bold;'>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-calendar"></i>{{trans('common.area_label')}}: {{$real_estate->area}} {{$area_measurement}}</p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-road"></i>{{trans('common.city')}}: {{$real_estate->city->$title}} </p>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-tint"></i>{{trans('common.real_estate_rent_type')}}: {{trans("common.rent_type")[$real_estate->rent_type]}}</p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-users"></i>{{trans('common.real_estate_category')}}: {{$real_estate->real_estate_category->$title}}</p>
									</div>
									<div class="clearfix"></div>
									<div class="clearfix"></div>
									@if($real_estate->client_priority!="")
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding"><p><i class="fa fa-dashboard"></i>{{trans('common.for_who_label')}}:  {{trans("common.priority_person")[$real_estate->client_priority]}}</p>
									</div>
									@endif
									@if($real_estate->real_estate_status!="")
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding"><p><i class="fa fa-dashboard"></i>{{trans('common.real_estate_condition_label')}}:  {{trans("common.array_real")[$real_estate->real_estate_status]}}</p>
									</div>
									@endif
									@if($real_estate->room_number!="")
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-cog"></i>{{trans('common.room_numbers_label')}}: {{trans("common.real_room_numbers")[$real_estate->room_number]}}</p>
									</div>
										@endif
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="clearfix visible-sm-block"></div>
						<div class="col-md-2 rating-price-box text-center clear-padding car-item">
						<div class="rating-box">

								<div class="user-rating">
									<i class="fa fa-calendar fa-fw" style="font-size: 25px"></i>
									<span>{{$real_estate->updated_at->diffForHumans()}}</span>

									<br>
									@if($real_estate->vip==1)  <i class="fa fa-diamond" aria-hidden="true" style="font-size: 25px"></i>   @endif
									
								</div>
							</div>
							<div class="room-book-box">
								<div class="price">
									<h5>{{$real_estate->price}} AZN/{{trans("common.payment_period_type_real_estate")[$real_estate->payment_period_type]}}</h5>
								</div>
								<div class="book">
									<a href="{{url('real_estate/'.$real_estate->id)}}">{{trans('common.more')}}</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			 @endforeach
			 
			  
			 
			  
			 
			 
				  
				<!-- END: HOTEL LIST VIEW -->
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<!-- START: PAGINATION -->
			<div class="bottom-pagination">
				<nav class="pull-right">
					 {{ $real_estate_list->links() }}
				</nav>
			</div>
			<!-- END: PAGINATION -->
		</div>

		@endif	
		<!-- END: INDIVIDUAL LISTING AREA -->
	</div>
</div>
<!-- END: LISTING AREA -->


<script>
 function related_sub_real_estate_search_form() {
 


    $.ajax({

     type: "GET",
     url: "{{url('related_sub_real_estate_search_form')}}", 
     data: {real_estate_sub_menu_id: $('#real_estate_sub_menu_id').val()},   
     cache:true,
     success: 
     function(data){
      $("#related_sub_real_estate_search_form").html(data).fadeIn();
			//alert(data);

  }
});

                               // alert('asdsad');
                               return false;
                           }            
</script>


<script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>

@endsection