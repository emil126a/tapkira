<div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.for_who_label')}}</label>

                                                    {{Form::select('priority_person',['' => trans('common.any_for_who')]+trans('common.priority_person'),null,array('class'=>'selectpicker'))}}
                                    
                                    </div>

<div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.real_estate_condition_label')}}</label>

                                                    {{Form::select('apartment_type',['' => trans('common.any_real_estate_condition')]+trans('common.array_real'),null,array('class'=>'selectpicker'))}}
                                    
                                    </div>

<div class="clearfix"></div>


<div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.room_numbers_label')}}</label>

                                                    {{Form::select('room_numbers',['' => trans('common.any_room_numbers')]+trans('common.real_room_numbers'),null,array('class'=>'selectpicker'))}}
                                    
                                    </div>
 
                                    <script src="{{asset('assets/js/js.js')}}"></script> 


