<?php
$title='title_'.App::getLocale();
?>
@extends('layouts.app')



@section('content')

<style type="text/css">
.area-label
{
    background: #0A3152 none repeat scroll 0 0;
    display: block;
    font-weight: bold;
    color: #ffffff;
    padding: 6px 4px;
    text-transform: uppercase;
    margin-top: -33px;
    position: absolute;
    right: 0;
    z-index: 100;
}

.price_label
{
       background: #F7941D none repeat scroll 0 0;
    display: block;
    font-weight: bold;
    color: #ffffff;
    padding: 6px 4px;
    text-transform: uppercase;
    margin-top: -170px;
    position: absolute;
    right: 0;
    z-index: 100; 
    font-size: 11px;
}


</style>
<!-- BEGIN: SEARCH SECTION -->
<section>
    <div class="row full-width-search">
        <div class="container clear-padding">
            <div class="col-md-8 search-section">
                <div role="tabpanel">
                    <!-- BEGIN: CATEGORY TAB -->
                    <ul class="nav nav-tabs search-top" role="tablist" id="searchTab">
                        <li role="presentation" class="active text-center">
                            <a href="#realestate" aria-controls="realestate" role="tab" data-toggle="tab">
                               <i class="fa fa-home" aria-hidden="true"></i>
                                <span>{{trans('common.real_estate_rent')}}</span>
                                </a>
                            </li>
                            <li role="presentation" class="text-center">
                                <a href="#item" aria-controls="item" role="tab" data-toggle="tab">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                    <span>{{trans('common.item_rent')}}</span>
                                </a>
                            </li>
                            <li role="presentation" class="text-center">
                                <a href="#service" aria-controls="service" role="tab" data-toggle="tab">
                                    <i class="fa fa-user-md" aria-hidden="true"></i>
                                    <span>{{trans('common.service_rent')}}</span>
                                </a>
                            </li>

                        </ul>
                        <!-- END: CATEGORY TAB -->

                        <!-- BEGIN: TAB PANELS -->
                        <div class="tab-content">
                            <!-- BEGIN: REAL ESTATE SEARCH -->
                            <div role="tabpanel" class="tab-pane active" id="realestate">
                                     {!! Form::open(array('url'=> url('real_estate_list/1'),'method'=>'get')) !!}
                                    <div class="col-md-12 product-search-title">{{trans('common.enter_real_estate_info')}}</div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                      <label>{{trans('common.category')}}</label><br>


                                 {{Form::select('cat_id', $real_estate_categories,null,array('class'=>'selectpicker','id'=>'real_estate_sub_menu_id','onchange'=>'related_sub_real_estate_search_form()'))}}                                         
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.post_type')}}</label>

                                                    {{Form::select('rent_type',[''=>trans('common.any_rent_type')]+trans('common.rent_type'),null,array('class'=>'selectpicker'))}}
                                    
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                          <label>{{trans('common.min_price')}}</label><br>
                                        <input id="hotel_adult_count" name="min_price" placeholder="0" onkeypress="return isNumber(event)"  class="form-control quantity-padding">
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.max_price')}}</label><br>
                                        <input type="text" id="hotel_child_count" placeholder="1000" name="max_price" onkeypress="return isNumber(event)" class="form-control quantity-padding">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="related_sub_real_estate_search_form">
                                        
                                        @include('guest.real_estate_homepage_related')
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.city')}}</label><br>

                                        {{Form::select('city_id',['' => trans('common.any_city')]+$cities,null,array('class'=>'selectpicker'))}}
                                         
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.rent_period')}}</label><br>
                                         {{Form::select('payment_period_type',['' => trans('common.any_rent_period')]+trans('common.payment_period_type_real_estate'),null,array('class'=>'selectpicker'))}}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 search-col-padding">
                                        <button type="submit" class="search-button btn transition-effect">{{trans('common.search_real_estate')}}</button>
                                    </div>
                                    <div class="clearfix"></div>

                                    
                                {!! Form::close()!!}
                            </div>
                            <!-- END: REAL ESTATE SEARCH -->
                            
                            <!-- START: ITEM SEARCH -->
                            <div role="tabpanel" class="tab-pane" id="item">
                               {!! Form::open(array('url'=> url('item_list/1'),'method'=>'get')) !!}
                                    <div class="col-md-12 product-search-title">{{trans('common.enter_item_info')}}</div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                      <label>{{trans('common.category')}}</label><br>


                                 {{Form::select('cat_id', $item_categories,null,array('class'=>'selectpicker','onchange'=>'related_sub_item_category_homepage(this)'))}}                                         
                                    </div>
                                  

                                    <div id='rel_sub'>
       
<div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.sub_category')}}</label>{{Form::select('sub_cat_id', ['' => trans('common.view_all')]+$sub_item_categories,null,array('class'=>'selectpicker'))}}</div><div class="clearfix"></div>

            </div>
                  <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.post_type')}}</label>

                                                    {{Form::select('rent_type',[''=>trans('common.any_rent_type')]+trans('common.item_rent_type'),null,array('class'=>'selectpicker'))}}
                                    
                                    </div>
                            <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label> {{trans('common.city')}}</label><br>

                                        {{Form::select('city_id',['' => trans('common.any_city')]+$cities,null,array('class'=>'selectpicker'))}}
                                         
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                          <label>{{trans('common.min_price')}}</label><br>
                                        <input name="min_price" onkeypress="return isNumber(event)" placeholder="0" class="count form-control quantity-padding">
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label> {{trans('common.max_price')}}</label><br>
                                        <input type="text" name="max_price" onkeypress="return isNumber(event)" placeholder="1000" class="count form-control quantity-padding">
                                    </div>
                             
                                    
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.rent_period')}}</label><br>
                                         {{Form::select('payment_period_type',['' => trans('common.any_rent_period')]+trans('common.payment_period_type'),null,array('class'=>'selectpicker'))}}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 search-col-padding">
                                        <button type="submit" name="item_search" value="item_search" class="search-button btn transition-effect">{{trans('common.search_item')}}</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    {!! Form::close()!!}
                            </div>
                            <!-- END: ITEM SEARCH -->

                            
                            
              
                            
                            
                            <!-- START: SERVICE SEARCH -->
                            <div role="tabpanel" class="tab-pane" id="service">
                                    {!! Form::open(array('url'=> url('service_list/1'),'method'=>'get')) !!}
                                    <div class="col-md-12 product-search-title">{{trans('common.enter_service_info')}}</div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                      <label>{{trans('common.category')}}</label><br>


                                 {{Form::select('cat_id', $service_categories,null,array('class'=>'selectpicker','id'=>'real_estate_sub_menu_id','onchange'=>'related_sub_service_category_homepage(this)'))}}                                         
                                    </div>

                                    <div id="rel_sub_service">
                                        
                                    </div>

                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.post_type')}}</label>

                                                    {{Form::select('service_ad_type',['' => trans('common.any_rent_type')]+trans('common.service_type'),null,array('class'=>'selectpicker'))}}

                                    
                                    </div>
                                     <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.city')}}</label><br>

                                        {{Form::select('city_id',['' => trans('common.any_city')]+$cities,null,array('class'=>'selectpicker'))}}
                                         
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                          <label>{{trans('common.min_price')}}</label><br>
                                        <input name="min_price" placeholder="0" onkeypress="return isNumber(event)" class="count form-control quantity-padding">
                                    </div>
                                    <div class="col-md-6 col-sm-6 search-col-padding">
                                        <label>{{trans('common.max_price')}}</label><br>
                                        <input type="text" name="max_price" placeholder="1000" onkeypress="return isNumber(event)" class="count form-control quantity-padding">
                                    </div>                                
                                   
                                   
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 search-col-padding">
                                        <button type="submit" class="search-button btn transition-effect" name="service_search"  value="service_search">{{trans('common.search_service')}}</button>
                                    </div>
                                    <div class="clearfix"></div>
                                   {!! Form::close()!!}
                            </div>
                            <!-- END: SERVICE SEARCH -->
                            
                        </div>
                        <!-- END: TAB PANE -->
                    </div>
                </div>
         
            </div>
        </div> 
    </section>  
    <!-- END: SEARCH SECTION -->
<!-- BEGIN: RECENT BLOG POST -->
<section id="recent-blog">
	<div class="row top-offer">
		<div class="container">
            @if(count($real_estates)>0)
			<div class="section-title text-center">
				<h2>{{trans('common.real_estate')}}</h2>
			</div>

			<div class="owl-carousel" id="post-list">

            @foreach($real_estates as $real_estate)
            <?php
$area_measurement=($real_estate->real_estate_category->id==8)?trans('common.sot'):trans('common.m2');
?>
				<div class="room-grid-view wow slideInUp" data-wow-delay="0.1s">
					<img src="{{asset('uploads/real_estate_images/'.$real_estate->real_estate_images[0]->image)}}" style="display: block; object-fit: contain; margin: 0 auto; height: 170px !important;" alt="cruise">
	
    <span class="price_label">{{$real_estate->price}} AZN / {{trans("common.payment_period_type")[$real_estate->payment_period_type]}}</span>			
    <span class="area-label">{{$real_estate->area}} {{$area_measurement}}</span>
                    <div class="room-info"    @if($real_estate->vip==1) style="background-color:#fff9bc;"   @endif>
						<div class="post-title" style="min-height:75px">
							<h5 style="min-height: 33px">{{trans('common.in_city', ['city' => $real_estate->city->$title])}},

                           {{$real_estate->real_estate_category->$title}}            </h5>
							<p><i class="fa fa-calendar"></i> {{$real_estate->updated_at->diffForHumans()}} 

                            @if($real_estate->vip==1)
                                 <span style="float:right;"><i class="fa fa-diamond" aria-hidden="true"></i></span>
                            @endif
                           

                            </p>
						</div>						
						<div class="room-book">							 
							<div class="col-md-12 col-sm-6 col-xs-6 clear-padding">
								<a href="{{url('real_estate/'.$real_estate->id)}}" class="text-center">{{trans('common.more')}}</a> 
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
            @endforeach
				 
			  
			</div>

            @endif

            
@if(count($items)>0)
<div class="section-title text-center">
                <h2>{{trans('common.items')}}</h2>
               
            </div>

            <div class="owl-carousel" id="post-list2">


            @foreach($items as $item)
<div class="room-grid-view wow slideInUp" @if($item->vip==1) style="background-color:#fff9bc;"   @endif data-wow-delay="0.1s" >
                    <img src="{{asset('uploads/item_images/'.$item->item_images[0]->image)}}" alt="item" style="display: block; object-fit: contain; margin: 0 auto; height: 170px !important;">


                        <span class="price_label">{{$item->price}} AZN / {{trans("common.payment_period_type")[$item->payment_period_type]}}</span>


                    <div class="room-info">
                        <div class="post-title">
                            <h5 style="min-height: 33px">{{str_limit($item->title,25)}}</h5>
                            <p><i class="fa fa-calendar"></i> {{$item->updated_at->diffForHumans()}} @if($item->vip==1)
                                 <span style="float:right;"><i class="fa fa-diamond" aria-hidden="true"></i></span>
                            @endif</p>

                            
                        </div>
                       
                        <div class="room-book">
                             
                            <div class="col-md-12 col-sm-6 col-xs-6 clear-padding">
                                <a href="{{url('item/'.$item->id)}}" class="text-center">{{trans('common.more')}}</a> 
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            @endforeach
               
            </div>
@endif


@if(count($services)>0)
<div class="section-title text-center">
                <h2>{{trans('common.services')}} </h2>
            </div>
            <div class="owl-carousel" id="post-list3">


            @foreach($services as $service)
<div class="room-grid-view wow slideInUp" data-wow-delay="0.1s" @if($service->vip==1) style="background-color:#fff9bc;"   @endif>
                    <img src="{{asset('uploads/service_images/'.$service->service_images[0]->image)}}" style="display: block; object-fit: contain; margin: 0 auto; height: 170px !important;" alt="services">

                            <span class="price_label">{{$service->price}} AZN</span>

                    <div class="room-info">
                        <div class="post-title">
                            <h5  style="min-height: 33px">{{str_limit($service->title,25)}}</h5>
                            <p><i class="fa fa-calendar"></i> {{$service->updated_at->diffForHumans()}} @if($service->vip==1)
                                 <span style="float:right;"><i class="fa fa-diamond" aria-hidden="true"></i></span>
                            @endif</p>
                        </div>
                        
                        <div class="room-book">
                             
                            <div class="col-md-12 col-sm-6 col-xs-6 clear-padding">
                                <a href="{{url('service/'.$service->id)}}" class="text-center">{{trans('common.more')}}</a> 
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            @endforeach                                                                          
            </div>
            @endif
		</div>
	</div>
</section>
<!-- END: RECENT BLOG POST -->


<!-- START: COUNTER -->
<section id="how-it-work">
        <div class="row work-row">
            <div class="container">   
                <div class="section-title text-center">
                    <h2>{{trans('common.daily_statistics')}}</h2>

                </div>           
                <div class="work-step">
                    <div class="col-md-4 col-sm-4 first-step text-center">
                       <i class="fa fa-home" aria-hidden="true"></i>
                        <h5>{{trans('common.real_estate')}}</h5>
                        <p>{{trans('common.today_published_real_estate_number')}}: <u><b>{{count($count_real_estates_per_day)}}</b></u></p>
                    </div>
                    <div class="col-md-4 col-sm-4 second-step text-center">
                      <i class="fa fa-camera" aria-hidden="true"></i>
                        <h5>{{trans('common.items')}}</h5>
                        <p>{{trans('common.today_published_item_number')}}: <u><b>{{count($count_items_per_day)}}</b></u></p>
                    </div>
                    <div class="col-md-4 col-sm-4 third-step text-center">
                      <i class="fa fa-user-md" aria-hidden="true"></i>
                        <h5>{{trans('common.services')}}</h5>
                        <p>{{trans('common.today_published_service_number')}}: <u><b>{{count($count_service_per_day)}}</b></u></p>
                    </div>
                </div>
            </div>
        </div>
</section>
<!-- END: COUNTER -->

    @section('bottom')

   <script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>

    <script>

    function related_sub_real_estate_search_form(value) {

    $.ajax({

     type: "GET",
     url: "{{url('related_sub_real_estate_search_form_homepage')}}", 
     data: {real_estate_sub_menu_id: $('#real_estate_sub_menu_id').val()},   
     cache:true,
     success: 
     function(data){
      $("#related_sub_real_estate_search_form").html(data).fadeIn();
/*           alert(data);*/

  }
});

                               // alert('asdsad');
                               return false;
 


 }


    function related_sub_item_category_homepage(value) {


        var main_item_cat_id = $(value).val();

       // alert(main_item_cat_id);
      ///  var sub_cat_id=$('#sub_cat_id').val();
     // get selected value
    // alert ("selected " + main_item_cat_id);
    $.ajax({


        type: "GET",
        url: "{{url('related_sub_item_category_homepage')}}", 
        data: {main_item_cat_id: main_item_cat_id},   
        cache:false,
        success: 
        function(data){

     //   alert(data);
        $("#rel_sub").html(data).fadeIn();
        }
     })


 }

function related_sub_service_category_homepage(value) {


        var main_service_cat_id = $(value).val();

       // alert(main_item_cat_id);
      ///  var sub_cat_id=$('#sub_cat_id').val();
     // get selected value
    // alert ("selected " + main_item_cat_id);
    $.ajax({


        type: "GET",
        url: "{{url('related_sub_service_category_homepage')}}", 
        data: {main_service_cat_id: main_service_cat_id},   
        cache:false,
        success: 
        function(data){

       // alert(data);
        $("#rel_sub_service").html(data).fadeIn();
        }
     })


 }


</script>
    <script type="text/javascript">  
        /* SLIDER SETTINGS */
        jQuery(function($){
            "use strict";
            $.supersized({

                        //Functionality
                        slideshow               :   1,      //Slideshow on/off
                        autoplay                :   1,      //Slideshow starts playing automatically
                        start_slide             :   1,      //Start slide (0 is random)
                        random                  :   0,      //Randomize slide order (Ignores start slide)
                        slide_interval          :   5000,  //Length between transitions
                        transition              :   1,      //0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                        transition_speed        :   500,    //Speed of transition
                        new_window              :   1,      //Image links open in new window/tab
                        pause_hover             :   0,      //Pause slideshow on hover
                        keyboard_nav            :   0,      //Keyboard navigation on/off
                        performance             :   1,      //0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                        image_protect           :   1,      //Disables image dragging and right click with Javascript

                        //Size & Position
                        min_width               :   0,      //Min width allowed (in pixels)
                        min_height              :   0,      //Min height allowed (in pixels)
                        vertical_center         :   1,      //Vertically center background
                        horizontal_center       :   1,      //Horizontally center background
                        fit_portrait            :   1,      //Portrait images will not exceed browser height
                        fit_landscape           :   0,      //Landscape images will not exceed browser width
                        
                        //Components
                        navigation              :   1,      //Slideshow controls on/off
                        thumbnail_navigation    :   1,      //Thumbnail navigation
                        slide_counter           :   1,      //Display slide numbers
                        slide_captions          :   1,      //Slide caption (Pull from "title" in slides array)
                        slides                  :   [       //Slideshow Images
                        {image : '<?= asset("assets/images/real_estates.jpg") ?>', title : 'Real estates'},  
                        {image : '<?= asset("assets/images/services.jpg") ?>', title : 'Services'},
                        {image : '<?= asset("assets/images/items.jpg") ?>', title : 'Items'},
                        ]

                    }); 
        });

    </script>

    @endsection
@endsection
