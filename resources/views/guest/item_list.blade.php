<?php
$title='title_'.App::getLocale();
?>

@extends('layouts.app')

@section('head')

<style type="text/css">
	
	label
	{
		/*text-shadow: 0 0 3px #FF0000, 0 0 5px #0000FF;*/

		background-color: #FF6617;
		padding: 0 8px 0 8px;
		font-size: 14px;
		font-weight: 500 !important;

	}



</style>
@endsection

@section('content')

<?php 

 
 //$real_estate_sub_id=(isset($real_estate_sub_id))?$real_estate_sub_id:$default_real_sub_id;

$cat_id_get=isset($_GET['cat_id'])?$_GET['cat_id']:$cat_id;
$sub_cat_id=isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:$sub_cat_id;
$rent_type=isset($_GET['rent_type'])?$_GET['rent_type']:'';
$min_price=isset($_GET['min_price'])?$_GET['min_price']:'';
$max_price=isset($_GET['max_price'])?$_GET['max_price']:'';
//$apartment_type=isset($_GET['apartment_type'])?$request->apartment_type:'';
$city_id=isset($_GET['city_id'])?$_GET['city_id']:'';
//$room_numbers=isset($_GET['room_numbers'])?$request->room_numbers:'';
$payment_period_type=isset($_GET['payment_period_type'])?$_GET['payment_period_type']:'';
 
?>

<!-- START: MODIFY SEARCH -->
<div class="row modify-search modify-holiday">
	<div class="container clear-padding">
		{!! Form::open(array('url'=> url('item_list/'.$cat_id_get),'method'=>'get')) !!}
			<div class="col-md-3">
				<div class="form-gp">
					<label>{{trans('common.item_category')}}</label>

					{{Form::select('cat_id', $categories,$cat_id_get,array('class'=>'selectpicker','onchange'=>'related_sub_item_category(this)'))}}
				</div>
			</div>

			<div id='rel_sub'>
				@if(count($sub_item_categories)>0)
<div class="col-md-3">
        <div class="form-gp">
          <label>{{trans('common.item_sub_category')}}</label>
           {{Form::select('sub_cat_id', ['all' => trans('common.view_all')]+$sub_item_categories,$sub_cat_id,array('class'=>'selectpicker','id'=>'sub_cat_id'))}}
        </div>
      </div>

				@endif
			</div>
		

			


			
			<div class="col-md-3">
				<div class="form-gp">
					<label>{{trans('common.min_price')}}</label>
					<input type="text" id="child_count" name="min_price" placeholder="0"  maxlength="11" onkeypress="return isNumber(event)" value="{{$min_price}}" class="form-control quantity-padding">

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-gp">
					<label>{{trans('common.max_price')}}</label>
					<input type="text" id="adult_count" name="max_price" placeholder="1000" onkeypress="return isNumber(event)"  value="{{$max_price}}" maxlength="11" class="form-control quantity-padding">

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-gp">
					<label>{{trans('common.city')}}</label>
					{{Form::select('city_id',[''=>trans('common.any_city')]+$cities,$city_id,array('class'=>'selectpicker','style'=>'z-index:1000 !important'))}}
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-gp">
					<label>{{trans('common.rent_period')}}</label>
					<div class="input-group margin-bottom-sm">

					{{Form::select('payment_period_type',['' =>trans('common.any_rent_period')]+trans('common.payment_period_type'),$payment_period_type,array('class'=>'selectpicker'))}}
					</div>


				</div>
			</div>
			<div class="col-md-3">
				<div class="form-gp">
					<label>{{trans('common.post_type')}}</label>
					<div class="input-group margin-bottom-sm">


						{{Form::select('rent_type',['' =>  trans('common.any_rent_type')]+trans('common.item_rent_type'),$rent_type,array('class'=>'selectpicker'))}}

					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="form-gp">
					<button type="submit" name="item_search" value="item_search" class="modify-search-button btn transition-effect">{{trans('common.search')}}</button>
				</div>
			</div>
		  {!! Form::close()!!}
	</div>
</div>
<!-- END: MODIFY SEARCH -->


<div class="row">
	<div class="container">
		@if(!count($item_list)>0)
		<br><br><br><br><br><br>
		<div class="alert alert-danger">
		{!!trans('common.not_found_in_category')!!}		 
		</div><br><br><br><br><br><br><br><br>
		@else
		
		<!-- START: INDIVIDUAL LISTING AREA -->
		<!-- START: INDIVIDUAL LISTING AREA -->
		<div class="col-md-12 hotel-listing">
			

			<div class="clearfix visible-xs-block"></div>
			<div class="col-sm-2 view-switcher"  style='float:right;'>
				<div class="pull-right">
					<a class="switchgrid" title="Grid View">
						<i class="fa fa-th-large"></i>
					</a>
					<a class="switchlist active" title="List View">
						<i class="fa fa-list"></i>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- START: ITEM LIST VIEW -->
			<div class="switchable col-md-12 clear-padding">



				@foreach($item_list as $item)


				<div class="hotel-list-view" @if($item->vip==1) style="background-color:#fff9bc;"   @endif>
					<div class="wrapper">
						<div class="col-md-4 col-sm-6 switch-img clear-padding" style="font-size:12px; font-weight:bold;">

							<img src="{{asset('uploads/item_images/'.$item->item_images[0]->image)}}" alt="item image">



						</div>
						<div class="col-md-6 col-sm-6 hotel-info">
							<div>
								<div class="hotel-header">
									<h5>{{$item->title}}</h5>

								</div>							
								<div class="car-detail" style='font-size:12px;font-weight:bold;'>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-calendar"></i>{{trans('common.city')}}: {{$item->city->$title}}</p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-road"></i> {{trans('common.item_rent_type_label')}}: {{trans("common.rent_type")[$item->rent_type]}}</p>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-tint"></i>{{trans('common.item_category')}}: {{$item->item_category->$title}}</p>
									</div>
									@if($item->item_sub_category!=null)
									<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
										<p><i class="fa fa-users"></i>{{trans('common.item_sub_category')}}: {{$item->item_sub_category->$title}}</p>
									</div>
									@endif

									
									<div class="clearfix"></div>

								</div>
							</div>
						</div>
						<div class="clearfix visible-sm-block"></div>
						<div class="col-md-2 rating-price-box text-center clear-padding car-item">
							<div class="rating-box">

								<div class="user-rating">
									<i class="fa fa-calendar fa-fw" style="font-size: 25px"></i>
									<span>{{$item->updated_at->diffForHumans()}}</span><br>
									@if($item->vip==1)  <i class="fa fa-diamond" aria-hidden="true" style="font-size: 25px"></i>   @endif
								</div>
							</div>
							<div class="room-book-box">
								<div class="price">

									<h5>{{$item->price}} AZN / {{trans("common.payment_period_type")[$item->payment_period_type]}}</h5>
								</div>
								<div class="book">
									<a href="{{url('item/'.$item->id)}}">{{trans('common.more')}}</a>
								</div>
							</div>

						</div>
					</div>
				</div>

				@endforeach







				<!-- END: ITEM LIST VIEW -->
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<!-- START: PAGINATION -->
			<div class="bottom-pagination">
				<nav class="pull-right">
					{{ $item_list->links() }}
				</nav>
			</div>
			<!-- END: PAGINATION -->
		</div>

		@endif	
		<!-- END: INDIVIDUAL LISTING AREA -->
	</div>
</div>
<!-- END: LISTING AREA -->

<script >
	function related_sub_item_category(value) {


		var main_item_cat_id = $(value).val();
		var sub_cat_id=$('#sub_cat_id').val();
     // get selected value
    // alert ("selected " + main_item_cat_id);
    $.ajax({


     	type: "GET",
     	url: "{{url('related_sub_item_category')}}", 
     	data: {main_item_cat_id: main_item_cat_id, sub_cat_id:sub_cat_id},   
     	cache:false,
     	success: 
     	function(data){

//     		alert(data);
     	$("#rel_sub").html(data).fadeIn();
     	}
     })


 }



</script>

<script type="text/javascript">
       function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

   </script>
@endsection