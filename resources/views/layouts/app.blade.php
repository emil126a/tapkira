<!DOCTYPE html>
<html class="load-full-screen">

<head>
   @yield('head')
   <meta name="_token" content="{{ csrf_token() }}">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="">
   <meta name="author" content="Emil Azizov">


<meta property="og:url"                content="{{URL::current()}}" />
<meta property="og:title"              content="@section('title'){{trans('common.personal_page')}}@show" />
<meta property="og:description"        content="@section('description'){{trans('common.site_title')}}@show" />
<meta property="og:image" content="@section('ogimage'){{asset('assets/images/og_image.png')}}@show">
<meta property="fb:app_id" content="163173560789444" />






   <title>{{trans('common.site_title')}}</title>
   <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/png">
   <!-- STYLES -->
   <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet">
   <link href="{{asset('assets/css/bootstrap-select.min.css')}}" rel="stylesheet">
   <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
   <link href="{{asset('assets/css/owl-carousel-theme.css')}}" rel="stylesheet">
   <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" media="screen">
   <link href="{{asset('assets/css/flexslider.css')}}" rel="stylesheet" media="screen">
   <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" media="screen">
   <!-- LIGHT -->
   <link rel="stylesheet" type="text/css" href="{{asset('assets/css/color/orange2.css')}}" id="select-style">
   <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
   <link href="{{asset('assets/css/light.css')}}" rel="stylesheet" media="screen">

   <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600' rel='stylesheet' type='text/css'>


   <link href="{{asset('assets/css/uploadfile.css')}}" rel="stylesheet"> 
   <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
   <script src="{{asset('assets/js/jquery.js')}}"></script>






   <!--  <link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/css/bootstrapvalidator.min.css" rel="stylesheet" media="screen"> -->
   <script src="{{asset('assets/js/validate.'.App::getLocale().'.js')}}"></script>








</head>
<body class="load-full-screen">


    @yield('body')


    <?php
    $title="title_".App::getLocale();

    ?>
    <!-- BEGIN: PRELOADER -->
    <div id="loader" class="load-full-screen">
        <div class="loading-animation">
            <span><i class="fa fa-home"></i></span>
            <span><i class="fa fa-camera-retro"></i></span>
            <span><i class="fa fa-users"></i></span>
        </div>
    </div>
    <!-- END: PRELOADER -->



    <!-- BEGIN: SITE-WRAPPER -->
    <div class="site-wrapper">
        <!-- BEGIN: NAV SECTION -->
        <section>
            <div class="row transparent-menu-top">
                <div class="container clear-padding">
                    <div class="navbar-contact">
                        <div class="col-md-7 col-sm-6 clear-padding">

                          @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                          <img src="{{asset('assets/images/'.$localeCode.'.png')}}" width="30">
                          <a href="{{LaravelLocalization::getLocalizedURL($localeCode) }}"  hreflang="{{$localeCode}}" class="lang transition-effect"> {{ $properties['native'] }} </a>


                          @endforeach


                      </div>
                      <div class="col-md-5 col-sm-6 clear-padding search-box">

                        <div class="col-md-12 col-xs-12 clear-padding user-logged">



                           @if (Auth::guest())
                           <a href="{{ url('login') }}" class="transition-effect" style='vertical-align:middle;'><b><i class="fa fa-sign-in"></i>{{trans('common.login')}}</b></a> 
                           <a href="{{ url('register') }}" class="transition-effect" style='vertical-align:middle;'> <b><i class="fa fa-user" aria-hidden="true"></i>
                            {{trans('common.register')}}</b></a> 
                            @else

                            <a href="{{ url('myads') }}" class="transition-effect">

                                @if((Auth::user()->facebook_user_id!='0') && (Auth::user()->is_default_facebook_avatar=='0') )

                                <img src="{{Auth::user()->profile_picture}}" alt="profilephoto">

                                @else
                                <img src="{{asset('assets/images/user.png')}}" alt="profilephoto">
                                @endif


                                





                                {{ Auth::user()->name }} -  {{trans('common.account')}}
                            </a>




                            <a href="{{ url('logout') }}" class="transition-effect">
                                <i class="fa fa-sign-out"></i>{{trans('common.sign_out')}}
                            </a>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row transparent-menu">
            <div class="container clear-padding">
                <!-- BEGIN: HEADER -->
                <div class="navbar-wrapper">
                    <div class="navbar navbar-default" role="navigation">
                        <!-- BEGIN: NAV-CONTAINER -->
                        <div class="nav-container">
                            <div class="navbar-header">
                                <!-- BEGIN: TOGGLE BUTTON (RESPONSIVE)-->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" style="background-color: #f7941d" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <!-- BEGIN: LOGO -->
                                <a class="logo"  href="{{ url('homepage') }}"><img src="{{asset('assets/images/logo.png')}}" ></a>
                            </div>

                            <!-- BEGIN: NAVIGATION -->       
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" href="{{ url('homepage') }}"><i class="fa fa-home"></i> {{trans('common.homepage')}} </a>

                                    </li>


                                    <li class="dropdown mega">
                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-building"></i> {{trans('common.real_estate')}} <i class="fa fa-caret-down"></i></a>
                                        <ul class="dropdown-menu mega-menu">
                                            <li class="col-md-3 col-sm-3 desc">
                                                <h4>{{trans('common.real_estate')}}</h4>
                                                <p>{{trans('common.real_estate_description')}}</p>
                                            </li>



                                            <?php $i=0; // counter ?>

                                            @foreach($data['real_estate_category'] as $real_estate_category)  


                                            <?php if ($i%3==0) { // if counter is multiple of 3 ?>
                                            <li class="col-md-3 col-sm-4 links">

                                                <?php } ?>    

                                                <ul>


                                                   <li><a href="{{url('real_estate_list/'.$real_estate_category->id)}}"> {{$real_estate_category->$title}}</a></li>   





                                               </ul>

                                               <?php if($i%3==0) { // if counter is multiple of 3 ?>
                                           </li>
                                           <?php } ?>


                                           @endforeach





                                       </ul>
                                       <div class="clearfix"></div>
                                   </li>
                                   <li class="dropdown mega">
                                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-camera-retro"></i> {{trans('common.items')}} <i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="col-md-3 col-sm-3 desc">
                                            <h4>{{trans('common.items')}}</h4>
                                            <p>{{trans('common.items_description')}}</p>
                                        </li>









                                        @foreach($data['item_cat_subcat'] as $item_main_cat)
                                        <?php if ($i%8==0) { // if counter is multiple of 3 ?>
                                        <li class="col-md-3 col-sm-4 links">

                                            <?php } ?>    

                                            <ul>

                                                <?php



                                                $sub_item_id=(!count( $item_main_cat->sub_category) == 0 )?"/".$item_main_cat->sub_category[0]->id:'';

                                                ?>


                                                <li><a href="{{url('item_list/'.$item_main_cat->id.$sub_item_id)}}">{{$item_main_cat->$title}}</a>



                                         <!--    @foreach($item_main_cat->sub_category as $sub_cat)

                                            <ul>
                                                <li style="font-size:11px;"><a href="{{url('item_list/'.$item_main_cat->id.'/'.$sub_cat->id)}}"> {{$sub_cat->$title}}</a></li>


                                            </ul>
                                            @endforeach -->
                                        </li>


                                    </ul>

                                    <?php if($i%8==0) { // if counter is multiple of 3 ?>
                                </li>
                                <?php } ?>

                                @endforeach






                            </ul>
                            <div class="clearfix"></div>
                        </li>

                        <li class="dropdown mega">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-users"></i> {{trans('common.services')}} <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="col-md-3 col-sm-3 desc">
                                    <h4>{{trans('common.services')}}</h4>
                                    <p>{{trans('common.services_description')}}</p>
                                </li>


                                @foreach($data['service_cat_subcat'] as $service_main_cat)

                                <?php if ($i%8==0) { // if counter is multiple of 3 ?>
                                <li class="col-md-3 col-sm-4 links">

                                    <?php } ?>    

                                    <ul>
                                        <?php



                                        $sub_service_id=(!count( $service_main_cat->sub_category) == 0 )?"/".$service_main_cat->sub_category[0]->id:'';

                                        ?>

                                        <li><a href="{{url('service_list/'.$service_main_cat->id.$sub_service_id)}}">{{$service_main_cat->$title}}</a>


                                            <!-- @foreach($service_main_cat->sub_category as $sub_cat)

                                            <ul>
                                                <li style="font-size:11px;"><a href="{{url('service_list/'.$service_main_cat->id.'/'.$sub_cat->id)}}"> {{$sub_cat->$title}}</a></li>


                                            </ul>
                                            @endforeach -->
                                        </li>


                                    </ul>

                                    <?php if($i%8==0) { // if counter is multiple of 3 ?>
                                </li>
                                <?php } ?>
                                @endforeach








                            </ul>
                            <div class="clearfix"></div>
                        </li>
                        <li class="dropdown mega">
                         <a class="dropdown-toggle" href="{{ url('contact') }}"><i class="fa fa-phone-square"></i> {{trans('common.contact')}}</a>
                         
                     </li>
                     <li>
                        <a class="dropdown-toggle" style="background-color:red; color: white !important;margin-top:6px;font-size: 12px;" href="{{url('add/new')}}"><i class="fa fa-plus"></i> {{trans('common.add_post')}} </a>
                    </li>
                    
                </ul>
            </div>
            <!-- END: NAVIGATION -->
        </div>
        <!--END: NAV-CONTAINER -->
    </div>
</div>
<!-- END: HEADER -->
</div>
</div>
</section>
<!-- END: NAV SECTION -->




@yield('content')

<!-- START: FOOTER -->
<section id="footer">
    <footer>
     <div class="row sm-footer-nav text-center">
        <div class="row" style="   margin-top: 7px">
            <div class="col-md-3">
                <p>
                    <a href="{{url('about')}}">{{trans('common.about_project')}}</a> {{Html::mailto("info@tapkira.az")}}
                </p>
            </div>
            <div class="col-md-6">
                <p>
                     <span style="color: white;">&copy;  2017 TAPKIRA.AZ </span> <span style="color: white; font-size:11px"><a href='http://www.azizov.org'>- By Azizov</a> </span>
                </p> 
            </div>
            <div class="col-md-3">
                <p style="margin: 0 0 0px;">
                    <a href="https://www.instagram.com/tapkira.az"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 30px"></i></a> 
                    <a href="https://www.facebook.com/tapkira.az"><i class="fa fa-facebook" aria-hidden="true" style="font-size: 30px"></i></a> 
                    <a href="https://twitter.com/tapkira_az"><i class="fa fa-twitter" aria-hidden="true" style="font-size: 30px"></i></a> 
                    <a href="https://www.linkedin.com/company-beta/13275090"><i class="fa fa-linkedin" aria-hidden="true" style="font-size: 30px"></i></a>               
                </p>
            </div>
        </div>



    </div>
</footer>
</section>
<!-- END: FOOTER -->
</div>
<!-- END: SITE-WRAPPER -->

<!-- Load Scripts -->
<script src="{{asset('assets/js/respond.js')}}"></script>


<script src="{{asset('assets/js/jquery.uploadfile-'.App::getLocale().'.min.js')}}"></script>

<script src="{{asset('assets/js/jquery.form.js')}}"></script>
<script src="{{asset('assets/plugins/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/plugins/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/supersized.3.1.3.min.js')}}"></script>
<script src="{{asset('assets/js/js.js')}}"></script>
@yield('bottom')
<!-- Start of StatCounter Code for Dreamweaver -->
<script type="text/javascript">
var sc_project=11292859; 
var sc_invisible=1; 
var sc_security="7475a874"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="web analytics"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="//c.statcounter.com/11292859/0/7475a874/1/" alt="web
analytics"></a></div></noscript>
<!-- End of StatCounter Code for Dreamweaver -->


 
 <script src='{{asset("assets/js/facebook-popup-box.js")}}' type='text/javascript'></script>

 
<style>
#fb-box-background {display: none;background: rgba(0, 0, 0, 0.8);width: 100%;height: 100%;position: fixed;top: 0;left: 0;z-index: 99999;}
#fb-box-close {width: 100%;height: 100%;}
#fb-box {background: #eaeaea;border: 2px solid #3A3A3A;-webkit-border-radius: 5px;-moz-border-radius: 5px;padding: 4px 10px 4px 10px;border-radius: 5px;position:absolute;top: 40%;left: 40%;margin-top: -50px;margin-left: -50px;}

#fclose-button {position: absolute;top: -10px;right: -10px;background: #fff;font: bold 16px Arial, Sans-Serif;text-decoration: none;line-height: 22px;width: 26px;
text-align: center;color: #000000;border: 2px solid #2F2F2F;-webkit-box-shadow: 0px 1px 2px rgba(0,0,0,0.4);-moz-box-shadow: 0px 1px 2px rgba(0,0,0,0.4);
box-shadow: 0px 1px 2px rgba(0,0,0,0.4);-webkit-border-radius: 22px;-moz-border-radius: 22px;border-radius: 25px;cursor: pointer;}
#fclose-button:before {content: "X";}
#Poweredby,#Poweredby a.visited,#Poweredby a,#Poweredby a:hover {color: #aaaaaa;font-size: 9px;text-decoration: none;text-align: center;padding: 5px;}
#follow-us{font-size: 1.7em;color: #010069;}
</style>
<script type='text/javascript'>
jQuery(document).ready(function($){
if($.cookie('popup_facebook_box') != 'yes'){
$('#fb-box-background').delay(2000).fadeIn('medium');
$('#fclose-button, #fb-box-close').click(function(){
$('#fb-box-background').stop().fadeOut('medium');
});
}
$.cookie('popup_facebook_box', 'yes', { path: '/', expires: 10 });
});
</script>
<div id='fb-box-background'>
<div id='fb-box-close'>
</div>
<div id='fb-box'>
<!-- <div id="follow-us">Tapkira.az - Facebook</div>-->
<div id='fclose-button'>
</div>
<iframe src="https://www.facebook.com/plugins/page.php?locale={{App::getLocale().'_'.strtoupper(App::getLocale())}}&href=https%3A%2F%2Fwww.facebook.com%2Ftapkira.az&tabs&width=250&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1576463719262830" width="250" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
<div id="Poweredby"><a style="padding-left: 0px;" href="http://www.tapkira.az" rel="nofollow">Tapkira.az</a></div>
</div>
</div>

</body>
</html>