<?php

return [


'rent_type' => array('0'=>'Kirayə verirəm','1'=>'Kirayə götürürəm'),
'item_rent_type'=> array(0=>'İcarə verirəm',1=>'İcarə götürürəm'),
'priority_person' => array('1' => 'Hamı üçün', '2' => 'Ailə üçün','3' => 'Tələbələr üçün'),
'payment_period_type' =>array('1' => 'Aylıq', '2' => 'Günlük', '3' => 'Həftəlik',  '4' => 'Saatlıq'),
'payment_period_type_real_estate' =>array('1' => 'Aylıq', '2' => 'Günlük'),

'array_real' => array('1' => 'Yeni Tikili', '2' => 'Köhnə tikili'),
'real_room_numbers' =>  array('1' => '1 otaq', '2' => '2 otaq', '3' => '3 otaq', '4' => '4 otaq','5' => '5 otaq', '6' => '6 otaq','7' => '7 otaq', '8' => '8 otaq','9' => '9 otaq', '10' => '10 otaq'),
'item_condition'=>array('0'=>'Köhnə','1'=>'Təzə'),
'service_type' => array('0'=>'Xidmət edirəm','1'=>'Xidmət istəyirəm'),
'active'=>array('0'=>'Təsdiq gözləyir','1'=>'Aktiv olunub','2'=>'Aktiv deyil'),
];
