<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Sizin şifrəniz yeniləndi!',
    'sent' => 'Şifrənizi yeniləmək üçün sizin emailə xüsusi link göndərdik!',
    'token' => 'Bu şifrənin yenilənməsi ilə bağlı verilmiş müddət artıq bitmişdir',
    'user' => "We can't find a user with that e-mail address.",
    'reset_email_subject'=>'Unudulmuş şifrənin bərpası',
    'click_here_reset'=>'Şifrənizi yeniləmək üçün bu linkin üzərinə tıklayın'

];
