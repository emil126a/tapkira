<?php

return [
	'site_title'=>'Tapkira.az - Hər şeyin kirayəsi',
	'rent_type' => array('0'=>'Kirayəyə verirəm','1'=>'Kirayəyə götürürəm'),
	'item_rent_type'=> array(0=>'İcarəyə verirəm',1=>'İcarəyə götürürəm'),
	'priority_person' => array('1' => 'Hamı üçün', '2' => 'Ailə üçün','3' => 'Tələbələr üçün'),
	'payment_period_type' =>array('1' => 'Aylıq', '2' => 'Günlük', '3' => 'Həftəlik',  '4' => 'Saatlıq'),
	'payment_period_type_real_estate' =>array('1' => 'Aylıq', '2' => 'Günlük', '3' => 'Həftəlik',  '4' => 'Saatlıq'),
	'array_real' => array('1' => 'Yeni Tikili', '2' => 'Köhnə tikili','3' => 'Fərdi tikili'),
	'real_room_numbers' =>  array('1' => '1 otaq', '2' => '2 otaq', '3' => '3 otaq', '4' => '4 otaq','5' => '5 otaq', '6' => '6 otaq','7' => '7 otaq', '8' => '8 otaq','9' => '9 otaq', '10' => '10 otaq'),
	'item_condition'=>array('0'=>'Köhnə','1'=>'Təzə'),
	'service_type' => array('0'=>'Xidməti göstərirəm','1'=>'Xidməti istəyirəm'),
	'active'=>array('0'=>'Təsdiq gözləyir','1'=>'Aktiv olunub','2'=>'Aktiv deyil'),
	'account'=>'Hesabım',
	'homepage'=>'ANA SƏHİFƏ',
	'real_estate'=>'DAŞINMAZ ƏMLAK',
	'real_estate_description'=>'Daşınmaz əmlak bölümündən siz müxtəlif daşınmaz əmlak növləri ilə tanış ola bilərsiniz',
	'items'=>'ƏŞYALAR',
	'items_description'=>'Əşyalar bölməsindən siz müxtəlif əşyaların kirayəsi ilə maraqlana bilərsiniz',
	'services'=>'XİDMƏTLƏR',
	'services_description'=>'Xidmətlər bölməsindən siz müxtəlif xidmətlərdən faydalana bilərsiniz',
	'contact'=>'ƏLAQƏ',
	'add_post'=>'ELAN YERLƏŞDİR',
	'sign_out'=>'Çıxış',
	'login'=>'GİRİŞ',
	'register'=>'QEYDİYYAT',
	'real_estate_rent'=>'DAŞINMAZ ƏMLAK İCARƏSİ',
	'item_rent'=>'ƏŞYA İCARƏSİ',
	'service_rent'=>'XİDMƏT',
	'enter_real_estate_info'=>'DAŞINMAZ ƏMLAK İLƏ BAĞLI MƏLUMATLARI DAXİL EDİN',
	'enter_item_info'=>'ƏŞYA İLƏ BAĞLI MƏLUMATLARI DAXİL EDİN',
	'enter_service_info'=>'XİDMƏT İLƏ BAĞLI MƏLUMATLARI DAXİL EDİN',
	'category'=>'KATEQORİYA',
	'sub_category'=>'ALT KATEQORİYA',
	'post_type'=>'Elanın növü',
	'min_price'=>'Minimum qiyməti',
	'max_price'=>'Maksimum qiyməti',
	'city'=>'Şəhər',
	'rent_period'=>'Kirayə müddəti',
	'search_real_estate'=>'DAŞINMAZ ƏMLAK AXTAR',
	'search_item'=>'ƏŞYA AXTAR',
	'search_service'=>'XİDMƏT AXTAR',
	'in_city'=>':city şəhərində',
	'more'=>'Ətraflı',
	'daily_statistics'=>'Gündəlik statistika',
	'today_published_real_estate_number'=>'Bu gün əlavə olunmuş əmlak sayı',
	'today_published_item_number'=>'Bu gün əlavə olunmuş əşya sayı',
	'today_published_service_number'=>'Bu gün əlavə olunmuş xidmət sayı',
	'any_rent_type'=>'İstənilən elan növü',
	'any_city'=>'İstənilən şəhər',
	'any_rent_period'=>'İstənilən kirayə müddəti',
	'post_id'=>'Elan №',
	'item_category'=>'Əşyanın kateqoriyası',
	'item_sub_category'=>'Alt kateqoriya',
	'item_condition_label'=>'Əşyanın vəziyyəti',
	'item_rent_type_label'=>'İcarə növü',
	'item_rent_period_label'=>'İcarə müddəti',
	'updated_at'=>'Yeniləndi',
	'created_at'=>'Əlavə olundu',
	'about_item'=>'Əşya haqqında',
	'comments'=>'Şərhlər',
	'contact_details'=>'Əlaqə məlumatları',
	'non_active_post'=>'<strong>Üzr istəyirik!</strong> Bu elan ya mövcud deyil, ya da artıq müddəti bitmişdir.',
	'click_expand_image'=>'Şəkli böyütmək üçün şəklə tıklayın',
	'view_all'=>'Hamısına bax',
	'search'=>'AXTAR',
	'not_found_in_category'=>'<strong>XƏTA!</strong> Bu kateqoriyada hər hansı elan tapılmadı',
	'sot'=>'SOT',
	'm2'=>'M²',
	'about_real_estate'=>'Əmlak haqqında',
	'real_estate_category'=>'Əmlakın kateqoriyası',
	'room_numbers_label'=>'Otaqların sayı',
	'any_room_numbers'=>'İstənilən sayda otaq',
	'area_label'=>'Əmlakın ölçüsü',
	'for_who_label'=>'Kim üçün',
	'any_for_who'=>'Hamısını seç',
	'real_estate_rent_type'=>'Kirayə növü',
	'any_real_estate_rent_type'=>'İstənilən elan növü',
	'real_estate_rent_period'=>'Kirayə müddəti',
	'any_real_estate_rent_period'=>'İstənilən kirayə müddəti',
	'look_map'=>'Xəritədə bax',
	'address'=>'Ünvan',
	'real_estate_rent_type_label'=>'Elanın növü',
	'real_estate_condition_label'=>'Əmlakın vəziyyəti',
	'any_real_estate_condition'=>'İstənilən əmlak tipi',
	'about_service'=>'Xidmət haqqında',
	'service_category'=>'Xidmətin kateqoriyası',
	'service_sub_category'=>'Alt-kateqoriya',
	'service_rent_type'=>'Xidmətin növü',
	'service_name'=>'Xidmətin adı',
	'service_name_placeholder'=>'Xidmətin adını daxil edin',
	'any_service_type'=>'İstənilən xidmət növü',
	'contact_us'=>'Bizimlə əlaqə',
	'send_us_message'=>'Mesajınızı bizə göndərin',
	'email_subject'=>['Təklif','İrad', 'Şikayət', 'Digər'],
	'send'=>'GÖNDƏR',
	'name_placeholder'=>'Adınız',
	'email_placeholder'=>'Email ünvanınız',
	'message_placeholder'=>'Mesajınız',
	'main_categories'=>[1=>'Daşınmaz əmlak',2=>'Əşyalar',3=>'Xidmətlər'],
	'save_post'=>'Əlavə et',
	'rules'=>'<h4>Saytda  elan yerləşdirilməsi qaydaları</h4>
<p>Hər  kəsin eyni  şərtlər altında saytın xidmətlərindən istifadə etməsi  məqsədilə, elan verməzdən əvvəl Tapkira.Az-ın  qaydaları ilə tanış olması zəruridir</p>
<p><strong>Ümumi qaydalar:</strong></p>
<ul type="disc">
  <li>Elan müvafiq kateqoriyada yerləşdirilməlidir;</li>
  <li>Təkrar xarakterli elanların verilməsi qadağandır.Elanın       müddəti bitənə qədər (30 gün) ,onu yenidən yerləşdirmək qadağandır, köhnə       elanın yenidən yerləşdirilməsi üçün adminə müraciət edilməlidir;</li>
  <li>Qadağan olunmuş məhsul, xidmət kirayəsi ilə bağlı elan       yerləşdirilməməlidir.</li>
  <li>Kirayəyə verilmiş əmlaka vurulan ziyana görə saytın       rəhbərliyi məsuliyyət daşımır.</li>
</ul>
<p><strong>Elanın başlığı və mətni barədə</strong></p>
<ul type="disc">
  <li>Kirayəyə veriləcək məhsul və xidmət haqqında mətnin       başlığı və onun məzmunu saytın Azərbaycan dilin bölməsində yalnız latın       hərfləri ilə yazılmalı, rus dili bölməsində isə müvafiq olaraq rus dilində       yazılmalıdır.</li>
  <li>Başlıqda ancaq  elanı       xarakterizə edən adı göstərilməlidir,artıq informasiya əlavə mətndə qeyd       edilməlidir;</li>
  <li>Elanın təsvirində əlaqə telefonunun, sayt ünvanlarının       (facebook, instagram linkləri daxil olmaqla) göstərilməsi qadağandır;</li>
  <li>Elanın şəkillərində sayt ünvanlarının (facebook,       instagram linkləri daxil olmaqla), loqotiplərin göstərilməsinə icazə       verilmir;</li>
  <li>Elanda Tapkiraz.Az-ın qaydalarına zidd (erotika,       ədəbsiz ifadələr vəs.) məzmun olmamalıdır;</li>
  <li>Elanların böyük hərflərlə doldurulmasına icazə       verilmir.(&quot;Caps Lock&quot; qoşulmuş vəziyyətdə)</li>
</ul>
<p><strong>Qiymət</strong></p>
<ul type="disc">
  <li>Yalnız elana aid olan  qiyməti göstərin;</li>
  <li>Rəqabətli qiymət təyin edin, bu zaman elanlara daha çox       diqqət edilir.</li>
</ul>
<p><strong>Əlaqə</strong></p>
<ul type="disc">
  <li>Elanda əks etdirilən şəxsin adı düzgün olmalıdır;</li>
  <li>Digər bir şəxsin telefon nömrəsi, elektron ünvanı və ya       əlaqə vasitələrini elanda yerləşdirilməsi qadağandır.</li>
</ul>
<p>Əgər sizin elanınız və ya profiliniz saytın admin  tərəfindən əngəllənibsə, bu o deməkdir ki, siz saytın qaydalarını pozmusunuz.  Əngəlləmənin ümumi səbəbləri ilə burada tanış ola bilərsiniz, həmçinin adminə də  müraciət edə bilərsiniz.</p>
Əgər siz hesab edirsinizsə  ki, əngəlləmə yalnışlıqla baş verib, o zaman <strong>'.Config::get('custom.contact_phone').'</strong> əlaqə telefonu (1-5-ci  günlər, Bakı vaxtı ilə saat 10:00-dan 20:00-dək) və ya  <b>'.Html::mailto("info@tapkira.az").'</b> e-mail  ünvanı vasitəsilə  müraciət edə  bilərsiniz',
	'image_empty'=>'Şəkil sahəsini boş buraxmayın',
	'welcome_user'=>':username, xoş gəlmisiniz!',
	'my_ads'=>'Mənim elanlarım',
	'profile'=>'Profil',
	'all_my_ads'=>'Bütün elanlarım',
	'personal_info'=>'Şəxsi məlumatlar',
	'success_message_user_info_save'=>'Dəyişikliklər yadda saxlanıldı',
	'your_fullname'=>'Tam adınız:',
	'your_fullname_placeholder'=>'Adınız və Soyadınız',
	'your_phone'=>'Telefon nömrəniz:',
	'your_email'=>'Sizin email',
	'your_email_placeholder'=>'Email ünvanınızı daxil edin',
	'save'=>'YADDA SAXLA',
	'change_password'=>'Şifrəni dəyiş',
	'error_password_no_match_db'=>'Daxil etdiyiniz köhnə şifrə düzgün deyil',
	'old_password'=>'Köhnə şifrə',
	'new_password'=>'Yeni şifrə',
	'new_password_repeat'=>'Yeni şifrə təkrarı',
	'select_picker_item_category'=>'Əşyanın kateqoriyasını seçin…',
	'select_picker_real_estate_category'=>'Əmlakın kateqoriyasını seçin…',
	'post_name'=>'Elan adı',
	'post_name_placeholder'=>'Elan adını daxil edin',
	'image'=>'Şəkil (lər)',
	'price'=>'Qiyməti',
	'item_price_placeholder'=>'Əşyanın qiymətini daxil edin',
	'is_new'=>'Təzə?',
	'item_description'=>'Məzmun',
	'item_description_placeholder'=>'Əşyanızı ətraflı şəkildə qeyd edin',
	'real_estate_description_placeholder'=>'Mənzilinizi ətraflı şəkildə qeyd edin',
	'real_estate_description_label'=>'Məzmun',
	'contact_phone'=>'Əlaqə telefonu',
	'contact_phone_placeholder'=>'Əlaqə telefonunuzu qeyd edin',
	'real_estate_price_placeholder'=>'Əmlakın qiymətini daxil edin',
	'real_estate_area_placeholder'=>'Əmlakın sahəsini daxil edin',
	'address_placeholder'=>'Yerləşdiyi ünvan',
	'no_post_own_at_all'=>'Sizin ümumiyyətlə heç bir elanınız yoxdur',
	'see'=>'Bax',
	'delete'=>'Sil',
	'load'=>'YÜKLƏ...',
	'no_post_own_in_this_category'=>'<strong>XƏTA!</strong> Bu kateqoriyada sizin elanınız yoxdur',
	'read_carefully'=>'Diqqətlə oxu',
	'delete_description'=>'<p>Siz elanınızı tamamilə silmək üzrəsiniz. Silinmiş elanlar geri qaytarıla bilməz.</p>
                    <p>Silmək istədiyinizə əminsinizmi?</p>',
    'yes'=>'Bəli',
    'no'=>'Xeyr',
    'service_price_placeholder'=>'Xidmətin qiymətini daxil edin',
    'services_description_placeholder'=>'Xidmətiniz haqqında məlumatı ətraflı şəkildə qeyd edin',
    'services_description_label'=>'Məzmun',
    'thank_you_message'=>'<h3>HÖRMƏTLİ, <u>:user</u> TƏŞƏKKÜR EDİRİK!</h3>',
    'your_post_accepted'=>'Sizin elanınız qəbul edildi!',
    'note_info'=>'Elanınızın kateqoriyasını (:category) və nömrəsini yadda saxlayın (:post_id). Elanınız ilə bağlı hər hansı müraciət etdikdə bu məlumatları təqdim etməyiniz vacibdir',
    'need_help'=>'Köməyə ehtiyacınız var?',
    'wanna_apply'=>'Elanla bağlı hər hansı müraciətiniz var?',
    'post_deleted'=>'Elanınız silindi!',
    'restore_my_password'=>'Şifrəmi bərpa et',
    'registered_email'=>'Qeydiyyatdan keçdiyiniz email ünvan',
    'sendto_registered_email'=>'"Şifrəmi bərpa et" təlimatını e-mailimə göndər',
    'password_restore_rules'=>'<h4>Hörmətli istifadəçi</h4>
                <p>Şifrənizi unutduğunuz halda bu formdan istifadə edərək yeni şifrə sifariş edə bilərsiniz.</p>
<p>Yeni şifrə sifarişi ilə bağlı qeydiyyatdan keçdiyiniz email ünvanınıza xüsusi link göndəriləcək. E-mail qutunuza daxil olmuş linkdən istifadə edərək şifrənizi bərpa edə bilərsiniz.</p>
<p>Şifrənizin bərpası ilə bağlı suallarınızı və müraciətlərinizi <b>'.Html::mailto("info@tapkira.az").'</b> ünvanına yazın yaxud <b>'.Config::get('custom.contact_phone').'</b> əlaqə telefonuna zəng edin.</p>',
	'email'=>'Email',
	'enter_new_password'=>'Yeni şifrənizi daxil edin',
	'update_password'=>'Şifrəmi yenilə!',
	'account_login'=>'Hesaba giriş',
	'password'=> 'Şifrə',
 	'remember_me'=>'Məni yadda saxla!',
 	'forget_password'=>'Şifrənizi unutmusunuz?',
 	'facebook_login'=>'FACEBOOK GİRİŞ',
 	'login_rules'=>'   <h4>Hörmətli istifadəçi</h4>
                <p>Xidmətlərimizdən istifadə etmək üçün sistemə daxil olmalısınız.</p>
<p>Xahiş edirik qeydiyyat zamanı aldığınız email və şifrənizi daxil edin.</p>
<p>Şəxsi kabinetinizə giriş və ya qeydiyyat ilə bağlı suallarınızı və müraciətlərinizi
<b>'.Html::mailto("info@tapkira.az").'</b> ünvanına yazın yaxud
<b>'.Config::get('custom.contact_phone').'</b> əlaqə telefonuna zəng edin.</p>
<p></p>',
'password_repeat'=>'Şifrə təkrarı',
'register_rules'=>'<p class="prompt"></p>  <h4>HÖRMƏTLİ İSTİFADƏÇİ</h4>
<p>Tapkira.Az saytında şirkətinizi qeydiyyatdan keçirmək üçün soldakı xanalarda
müvafiq məlumatları doldurmaq Sizdən xahiş olunur.</p> <p>Qeydiyyat prosesi bitdikdən sonra Siz email və şifrənizdən istifadə edərək sistemə daxil ola bilərsiniz.</p> <p>Şəxsi kabinetinizə giriş və ya qeydiyyat ilə bağlı suallarınızı və müraciətlərinizi <b>'.Html::mailto("info@tapkira.az").'</b> ünvanına yazın yaxud <b>'.Config::get('custom.contact_phone').'</b> əlaqə telefonuna zəng edin.</p>',
'new_post'=>'YENİ ELAN',
'about'=>'<h3>Layihə haqqında</h3>
				 Tapkira.Az layihəsi Azərbaycanda kirayə ilə bağlı elanların hər kəsin ixtiyarına verilməsi məqsədi ilə yaradılıb.
			<br><br>
Hər bir kəs saytdan istifadə etməklə geyim, mebeldən tutmuş, elektronika və avtomobilədək hər şeyi kirayəyə götürə və ya kirayə üçün sorğu verə bilər.<br><br>
Tapkira.Az-a əsasən ayrıca fərdlər elan yerləşdirir, həmçinin sayt fərdi sahibkarlar üçün də maraqlı ola bilər.Buna görə Tapkira.Az-da təkcə işlənmiş deyil, eləcə də yeni məhsulları da kirayəyə götürmək olar.',
'about_project'=>'Layihə haqqında',
    'send_success_message'=>'<strong>Diqqət!</strong> İsmarıcınız müvəffəqiyyətlə göndərildi. Tezliklə sizinlə əlaqə saxlanılacaq. Təşəkkür edirik.',
   




];
