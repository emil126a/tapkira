<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'İstifadəçi adınızı və şifrənizi düzgün daxil edin.',
    'throttle' => 'Hesabınıza giriş üçün həddindən artıq çox uğursuz cəhd etdiniz. Xahiş edirik :seconds saniyədən sonra yenidən cəhd edin.',

];
